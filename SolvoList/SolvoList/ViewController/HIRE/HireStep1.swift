//
//  HireStep1.swift
//  SolvoList
//
//  Created by Jaydeep on 10/9/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit

class HireStep1: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var productDetail = [String:Any]()
    
    @IBOutlet weak var txtCategory: TextField!
    @IBOutlet weak var txtCompanyName: TextField!
    @IBOutlet weak var txtJobTitle: TextField!
    var imgHire:UIImage!
    @IBOutlet var lblImgCount:UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var progressBar: ABSteppedProgressBar!
    var isUpdate:Bool = false
    
    let chooseCategory = DropDown()
    var arrCategory = [Any]()
    
    var strTextFieldSelected = ""
    var CategoryID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black];
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgClose"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        self.title = "Post Job"
        self.lblImgCount.isHidden = true
        DispatchQueue.main.async { // Dispatch in order to render the subtitle in the next runloop
            self.addSubtitles()
        }

        for dict in appDelegate.arrHireCategory {
            let dictCate = dict as! [String:Any]
            arrCategory.append(dictCate["hire_cat_name"] ?? "")
        }
        
        self.setupChooseCategoryDropDown()
        //self.customizeDropDown()
        
        if isUpdate {
            let dictDetail = getMyUserDefaults(key: MyUserDefaults.jobUpdateDetail) as! [String:Any]
            txtCompanyName.text = dictDetail["company_name"] as? String
            let namePredicate = NSPredicate(format: "hire_cat_id CONTAINS[C] '\(dictDetail["cat_id"] as! String)'")
            let arrCate = appDelegate.arrHireCategory.filter { namePredicate.evaluate(with: $0) }
            if arrCate.count > 0 {
                let dictCate = arrCate[0] as! [String:Any]
                txtCategory.text = dictCate["hire_cat_name"] as? String
                self.CategoryID = dictCate["hire_cat_id"] as! String
            }
            txtJobTitle.text = dictDetail["job_title"] as? String
            
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Global Methods
    private func addSubtitles() {
        let subtitleVerticalPosition: CGFloat = self.progressBar.frame.origin.y + self.progressBar.bounds.height + 2
        for (idx, point) in self.progressBar.centerPoints.enumerated() {
            let realPoint = self.progressBar.convert(point, to: self.containerView)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 60, height: 20))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.font = UIFont(name: FontName.OpenSansRegular, size: 14.0)
            subtitle.text = arrHireTab[idx]
            self.containerView.addSubview(subtitle)
        }
        progressBar.currentIndex = 0
    }
    
    func setupChooseCategoryDropDown() {
        chooseCategory.anchorView = txtCategory
        chooseCategory.dismissMode = .onTap
        chooseCategory.direction = .bottom

        chooseCategory.bottomOffset = CGPoint(x: 0, y: chooseCategory.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseCategory.dataSource = arrCategory as! [String]
        
        // Action triggered on selection
        chooseCategory.selectionAction = { [unowned self] (index, item) in
            self.txtCategory.text = item
            let dictCate = appDelegate.arrHireCategory[index] as! [String:Any]
            self.CategoryID = dictCate["hire_cat_id"] as! String
        }
    }
    
    func customizeDropDown() {
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        
        
        
        /*** FOR CUSTOM CELLS ***/
        chooseCategory.cellNib = UINib(nibName: "CategoryCell", bundle: nil)
        
        chooseCategory.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CategoryCell else { return }
            let dictCate = appDelegate.arrHireCategory[index] as! [String:Any]
            if dictCate["hire_cat_image"] as! String != "" {
                cell.imgCategory.sd_setImage(with: URL(string: dictCate["hire_cat_image"] as! String), completed: nil)
            }
            
            // Setup your custom UI components
            //cell.lblName.text = "Suffix \(index)"
        }
        /*** ---------------- ***/
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedOnNext(_ sender:UIControl) {
        if txtCategory.text == ""  {
            showAlert(msg: "Please select category", completion: nil)
        }else if txtCompanyName.text == ""  {
            showAlert(msg: "Please enter companyName", completion: nil)
        }else if txtJobTitle.text == ""  {
            showAlert(msg: "Please enter jobTitle", completion: nil)
        }else{
            let step1 = storyBoards.Hire.instantiateViewController(withIdentifier: "HireStep2") as! HireStep2
            step1.hireDetail = ["Category":txtCategory.text ?? "","CategoryID":self.CategoryID,"jobTitle":txtJobTitle.text ?? "","CompanyName":txtCompanyName.text ?? "","imgHire":(imgHire == nil) ? "" : imgHire];
            step1.isUpdate = self.isUpdate
            self.navigationController?.pushViewController(step1, animated: true)
        }
    }
    
    @IBAction func tappedOnSelectImage(_ sender:UIControl){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Open Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoCamera(target: self, canEdit: false)
        })
        let action2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoLibrary(target: self, canEdit: false)
        })
        let action3 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        present(alert, animated: true) { _ in }
    }
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        // use the image
        imgHire = chosenImage
        self.lblImgCount.isHidden = false
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCategory {
            self.customizeDropDown()
            chooseCategory.show()
            return false
        }else if textField == txtCompanyName {
            strTextFieldSelected = "txtCompanyName"
        }else if textField == txtJobTitle {
            strTextFieldSelected = "txtJobTitle"
        }
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }

    
    
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtCompanyName" {
                txtJobTitle.becomeFirstResponder()
            }else if strTextFieldSelected == "txtJobTitle" {
                txtJobTitle.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtCompanyName" {
                txtCompanyName.resignFirstResponder()
            }else if strTextFieldSelected == "txtJobTitle" {
                txtCompanyName.becomeFirstResponder()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
