//
//  HireViewController.swift
//  SolvoList
//
//  Created by ronak patel on 01/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit

class HireViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var isOpenHire = true
    
    override func awakeFromNib() {
        self.tabBarItem.title = "Hire"
        self.title = "Hire"
        self.tabBarItem.image = UIImage(named: "imgHireFooter")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isOpenHire {
            self.isOpenHire = false
            let hireStep1 = storyBoards.Hire.instantiateViewController(withIdentifier: "HireStep1") as! HireStep1
            let navController1 = NavigationController(rootViewController: hireStep1)
            present(navController1, animated: false, completion: nil)
        }else{
            appDelegate.tabBarController.selectedIndex = 2
            self.isOpenHire = true;
        }

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell") as! JobCell
        return cell
    }
    

    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
