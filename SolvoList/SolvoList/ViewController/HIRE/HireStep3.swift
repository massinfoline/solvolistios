//
//  HireStep3.swift
//  SolvoList
//
//  Created by Jaydeep on 10/9/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit

class HireStep3: UIViewController, UIPickerViewDelegate, UITextFieldDelegate{
   
    var hireDetail = [String:Any]()
    
    @IBOutlet weak var txtDate: TextField!
    /*@IBOutlet weak var txtSkill1: TextField!
    @IBOutlet weak var txtSkill2: TextField!
    @IBOutlet weak var txtSkill3: TextField!*/
    @IBOutlet weak var txtPayPerHour: TextField!
   
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var progressBar: ABSteppedProgressBar!
    
    var strTextFieldSelected = ""
   
    var datePicker: UIDatePicker = UIDatePicker()
    var isUpdate:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black];
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Post Job"
        
        DispatchQueue.main.async { // Dispatch in order to render the subtitle in the next runloop
            self.addSubtitles()
        }
        
        //Date
        
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        txtDate.inputView = datePicker
        datePicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        
        txtPayPerHour.addTarget(self, action: #selector(priceTextFieldDidChange), for: .editingChanged)
        
        
        if isUpdate {
            let dictDetail = getMyUserDefaults(key: MyUserDefaults.jobUpdateDetail) as! [String:Any]
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let sDate = dateFormatter.date(from: dictDetail["start_date"] as! String)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            txtDate.text = dateFormatter.string(from: sDate!)
            txtPayPerHour.text = dictDetail["pay_per_hour"] as? String
            
        }
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Date picker
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        if sender == datePicker {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            txtDate.text = dateFormatter.string(from: sender.date)
            //dateFormatter.dateFormat = "yyyy-MM-dd"
        }
    }
    
    //MARK: - Global Methods
    
    private func addSubtitles() {
        let subtitleVerticalPosition: CGFloat = self.progressBar.frame.origin.y + self.progressBar.bounds.height + 2
        for (idx, point) in self.progressBar.centerPoints.enumerated() {
            let realPoint = self.progressBar.convert(point, to: self.containerView)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 60, height: 20))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.font = UIFont(name: FontName.OpenSansRegular, size: 14.0)
            subtitle.text = arrHireTab[idx]
            self.containerView.addSubview(subtitle)
        }
        progressBar.currentIndex = 2
    }

    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }

    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnNext(_ sender:UIControl) {
        let payPerHour:String = (txtPayPerHour.text?.replacingOccurrences(of: "$", with: ""))!//txtPrice.text!
        if txtDate.text == ""  {
            showAlert(msg: "Please select date", completion: nil)
        }else if payPerHour == ""  {
            showAlert(msg: "Please enter pay perHour", completion: nil)
        }else {
            
//            self.hireDetail["Skill1"] = txtSkill1.text
//            self.hireDetail["Skill2"] = txtSkill2.text
//            self.hireDetail["Skill3"] = txtSkill3.text
            self.hireDetail["PayPer"] = payPerHour
            self.hireDetail["StartDate"] = txtDate.text
            let step4 = storyBoards.Hire.instantiateViewController(withIdentifier: "HireStep4") as! HireStep4
            step4.hireDetail = self.hireDetail
            step4.isUpdate = self.isUpdate
            self.navigationController?.pushViewController(step4, animated: true)
        }
    }
    
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func priceTextFieldDidChange(_ textField: UITextField) {
        
        if let amountString = textField.text?.currencyInputFormatting() {
            textField.text = amountString
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDate {
            strTextFieldSelected = "txtDate"
        }else if textField == txtPayPerHour {
            strTextFieldSelected = "txtPayPerHour"
        }
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtDate" {
                txtPayPerHour.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPayPerHour" {
                txtPayPerHour.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtDate" {
                txtDate.resignFirstResponder()
            }else if strTextFieldSelected == "txtPayPerHour" {
                txtDate.becomeFirstResponder()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
