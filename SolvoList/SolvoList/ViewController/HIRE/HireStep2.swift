//
//  HireStep2.swift
//  SolvoList
//
//  Created by Jaydeep on 10/9/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit

class HireStep2: UIViewController,UITextViewDelegate, UITextFieldDelegate {
    
    
    
    var hireDetail = [String:Any]()
    
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtRequiredSkill: UITextView!
    @IBOutlet weak var txtAdditionalSkill: UITextView!
    var textViewActive:UITextView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var progressBar: ABSteppedProgressBar!
    var requiredSkillValue = 0
    var additionalSkillValue = 0
    var strTextFieldSelected = ""
    var isUpdate:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black];
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Post Job"
        
        DispatchQueue.main.async { // Dispatch in order to render the subtitle in the next runloop
            self.addSubtitles()
        }
        
        if isUpdate {
            let dictDetail = getMyUserDefaults(key: MyUserDefaults.jobUpdateDetail) as! [String:Any]
            txtRequiredSkill.text = dictDetail["skill1"] as? String
            txtAdditionalSkill.text = dictDetail["skill2"] as? String
            txtView.text = dictDetail["skill3"] as? String
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Global Methods
    private func addSubtitles() {
        let subtitleVerticalPosition: CGFloat = self.progressBar.frame.origin.y + self.progressBar.bounds.height + 2
        for (idx, point) in self.progressBar.centerPoints.enumerated() {
            let realPoint = self.progressBar.convert(point, to: self.containerView)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 60, height: 20))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.font = UIFont(name: FontName.OpenSansRegular, size: 14.0)
            subtitle.text = arrHireTab[idx]
            self.containerView.addSubview(subtitle)
        }
        progressBar.currentIndex = 1
    }
    
    //MARK: - Button Action Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnNext(_ sender:UIControl) {
        if txtRequiredSkill.text?.length == 0 {
            showAlert(msg: "Please enter required skill value", completion: nil)
        }else if txtAdditionalSkill.text?.length == 0 {
            showAlert(msg: "Please enter additional skill value", completion: nil)
        }else if txtView.text == ""  {
            showAlert(msg: "Please enter text", completion: nil)
        }else {
            let step2 = storyBoards.Hire.instantiateViewController(withIdentifier: "HireStep3") as! HireStep3
            self.hireDetail["requiredSkillValue"] = txtRequiredSkill.text
            self.hireDetail["additionalSkillValue"] = txtAdditionalSkill.text
            self.hireDetail["ExtraSkill"] = txtView.text
            step2.hireDetail = self.hireDetail;
            step2.isUpdate = self.isUpdate
            self.navigationController?.pushViewController(step2, animated: true)
        }
    }
    
    @IBAction func tappedOnSkillReq(_ sender:UIButton) {
        showAlert(msg: "Put in the skills that the job requires 60% of the time. These are skills critical to performing the job", completion: nil)
    }
    
    @IBAction func tappedOnSkillAdd(_ sender:UIButton) {
        showAlert(msg: "Put in the skills required to performing 30%  of the job.", completion: nil)
    }
    
    @IBAction func btnRequiredSlider(_ sender: ASValueTrackingSlider) {
        requiredSkillValue = Int(sender.value*100)
        //print(requiredSkillValue)
    }
    
    @IBAction func btnAdditionalSlider(_ sender: ASValueTrackingSlider) {
        additionalSkillValue = Int(sender.value*100)
        //print(additionalSkillValue)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    // MARK: - UITextViewDelegate
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textViewActive = textView
        if textView == txtRequiredSkill {
            strTextFieldSelected = "txtRequiredSkill"
        }else if textView == txtAdditionalSkill {
            strTextFieldSelected = "txtAdditionalSkill"
        }else{
            strTextFieldSelected = "txtView"
        }
        textView.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtRequiredSkill" {
                txtAdditionalSkill.becomeFirstResponder()
            }else if strTextFieldSelected == "txtAdditionalSkill" {
                txtView.becomeFirstResponder()
            }else if strTextFieldSelected == "txtView" {
                txtView.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtRequiredSkill" {
                txtRequiredSkill.resignFirstResponder()
            }else if strTextFieldSelected == "txtAdditionalSkill" {
                txtRequiredSkill.becomeFirstResponder()
            }else if strTextFieldSelected == "txtView" {
                txtAdditionalSkill.becomeFirstResponder()
            }
        }
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWasShown), name:.UIKeyboardDidShow, object: nil)
    }
    
    func unregisterNotifications() {
        //NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardDidShow, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 200
    }
    
    func keyboardWasShown(notification: NSNotification)
    {
        let userInfo = notification.userInfo
        let keyboardSize = (userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        let contentInset = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize?.height)!+10,  0.0)
        self.scrollView.contentInset = contentInset
        self.scrollView.scrollIndicatorInsets = contentInset
        
        // Step 3: Scroll the target text field into view.
        var aRect: CGRect = self.viewContent.frame;
        aRect.size.height -= (keyboardSize?.height)!+10
        if (!aRect.contains(textViewActive!.frame.origin))
        {
            let scrollPoint: CGPoint = CGPoint(x: 0.0, y: textViewActive!.frame.origin.y - (keyboardSize!.height-80))
            ;            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
        
        return
        
        /*if textFieldActive == nil {
            let scrollPoint: CGPoint = CGPoint(x: 0.0, y: 390 - (keyboardSize!.height-60))
            ;            self.scrollViewGroupMakeTakers.setContentOffset(scrollPoint, animated: true)
            return
        }
        if (!aRect.contains(textFieldActive!.frame.origin))
        {
            
            let scrollPoint: CGPoint = CGPoint(x: 0.0, y: textFieldActive!.frame.origin.y - (keyboardSize!.height-60))
            ;            self.scrollViewGroupMakeTakers.setContentOffset(scrollPoint, animated: true)
        }*/
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
