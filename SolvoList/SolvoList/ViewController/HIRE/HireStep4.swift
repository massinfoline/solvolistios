//
//  HireStep4.swift
//  SolvoList
//
//  Created by ronak patel on 14/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire

class HireStep4: UIViewController {
    
    
    var hireDetail = [String:Any]()
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var progressBar: ABSteppedProgressBar!
    
    @IBOutlet var imgHire: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblPayHours: UILabel!
    @IBOutlet var lblCategory: UILabel!
    @IBOutlet var lblCompName: UILabel!
    @IBOutlet var lblStartDate: UILabel!
    //@IBOutlet var lblExtraSkill: UILabel!
    @IBOutlet var lblSkill1: UILabel!
    @IBOutlet var lblSkill2: UILabel!
    @IBOutlet var lblSkill3: UILabel!
    //@IBOutlet var lblSkillRequired: UILabel!
    //@IBOutlet var lblAdditionalSkill: UILabel!
    
    //@IBOutlet var additionalSkill: ASValueTrackingSlider!
    //@IBOutlet var skillRequired: ASValueTrackingSlider!
    
    var isUpdate:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black];
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Post Job"
        
        DispatchQueue.main.async { // Dispatch in order to render the subtitle in the next runloop
            self.addSubtitles()
        }
        
        showHireDetail()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Global Methods
    private func addSubtitles() {
        let subtitleVerticalPosition: CGFloat = self.progressBar.frame.origin.y + self.progressBar.bounds.height + 2
        for (idx, point) in self.progressBar.centerPoints.enumerated() {
            let realPoint = self.progressBar.convert(point, to: self.containerView)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 60, height: 20))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.font = UIFont(name: FontName.OpenSansRegular, size: 14.0)
            subtitle.text = arrHireTab[idx]
            self.containerView.addSubview(subtitle)
        }
        progressBar.currentIndex = 3
    }
    
    func showHireDetail() {
        print("HIRE Detail:\(hireDetail)")
        lblTitle.text = self.hireDetail["jobTitle"] as? String
        lblCategory.text = self.hireDetail["Category"] as? String
        lblPayHours.text = "Pay per hour : $\(self.hireDetail["PayPer"] as! String)"
        lblCompName.text = self.hireDetail["CompanyName"]  as? String
        lblStartDate.text = self.hireDetail["StartDate"]  as? String
        lblSkill1.text = self.hireDetail["requiredSkillValue"] as? String
        lblSkill2.text = self.hireDetail["additionalSkillValue"] as? String
        lblSkill3.text = self.hireDetail["ExtraSkill"] as? String
        if let image = self.hireDetail["imgHire"] as? UIImage {
            imgHire.image = image
        }else{
            imgHire.image = #imageLiteral(resourceName: "jobs")
        }
        
        //lblExtraSkill.text = self.hireDetail["ExtraSkill"] as? String
        //lblSkillRequired.text = self.hireDetail["requiredSkillValue"] as? String
        //lblAdditionalSkill.text = self.hireDetail["additionalSkillValue"] as? String
        
        /*//requiredSkillSliderBar
         let formatter = NumberFormatter()
         formatter.numberStyle = .percent
         
         self.skillRequired.popUpViewAnimatedColors = [Color.COLOR_GREEN]
         self.skillRequired.minimumValue = 0.0
         self.skillRequired.maximumValue = 1.0
         self.skillRequired.numberFormatter = formatter
         self.skillRequired.popUpViewArrowLength = 10.0
         self.skillRequired.value = Float(self.hireDetail["requiredSkillValue"] as! String)! / 100.0
         self.skillRequired.showPopUpView(animated: true)
         self.skillRequired.font = UIFont(name: FontName.OpenSansRegular, size: 15.0)
         
         //additionalSkillSliderBar
         self.additionalSkill.popUpViewAnimatedColors = [Color.COLOR_GREEN]
         self.additionalSkill.minimumValue = 0.0
         self.additionalSkill.maximumValue = 1.0
         self.additionalSkill.value = Float(self.hireDetail["additionalSkillValue"] as! String)! / 100.0
         self.additionalSkill.numberFormatter = formatter
         self.additionalSkill.popUpViewArrowLength = 10.0
         self.additionalSkill.showPopUpView(animated: true)
         self.additionalSkill.font = UIFont(name: FontName.OpenSansRegular, size: 15.0)*/
    }
    
    func updateJobDetail() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        let jobDetail = getMyUserDefaults(key: MyUserDefaults.jobUpdateDetail) as! [String:Any]
        var parameters = [String : Any]()
        parameters["i_key"] = WebURL.appKey
        parameters["user_id"] = userDetail["user_id"]
        parameters["cat_id"] = hireDetail["CategoryID"]
        parameters["company_name"] = hireDetail["CompanyName"]
        parameters["job_title"] = hireDetail["jobTitle"]
        parameters["skill_required"] = "60"
        parameters["additional_skill"] = "30"
        parameters["extra_skill"] = hireDetail["ExtraSkill"]
        parameters["start_date"] = hireDetail["StartDate"]
        parameters["skill_1"] = hireDetail["requiredSkillValue"]
        parameters["skill_2"] = hireDetail["additionalSkillValue"]
        parameters["skill_3"] = hireDetail["ExtraSkill"]
        parameters["pay_per_hour"] = hireDetail["PayPer"]
        parameters["job_id"] = jobDetail["hire_id"]
        appDelegate.showHud()
        
        upload(multipartFormData: { (multipartFormData) in
            if let image = self.hireDetail["imgHire"] as? UIImage {
                let data1 = UIImageJPEGRepresentation(image, 0.7)
                multipartFormData.append(data1!, withName: "image", fileName: "Hire\(arc4random_uniform(3) + 1).jpg", mimeType: "image/jpg")
            }
            
            for (key, value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            
        }, to: WebURL.UpdateJobDetail, method: .post, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.validate()
                upload.responseJSON { response in
                    appDelegate.hideHud()
                    if response.result.error != nil {
                        print("failure")
                        UIAlertView(title: "Fail", message: "Please retry again.", delegate: nil, cancelButtonTitle: "Ok").show()
                    } else {
                        print("success")
                        print("UpdateJobDetail Result:\(response)")
                        if response.result.isSuccess {
                            if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                                if (dictResult["o_returnCode"] as! Bool){
                                    let successMsg = dictResult["o_message"] as! String
                                    showAlert(msg: successMsg, completion: { (true) in
                                        self.presentingViewController?.dismiss(animated: true, completion: nil)
                                    })
                                }else{
                                    let errorMsg = dictResult["o_message"] as! String
                                    appDelegate.showAlertMessage(strMessage: errorMsg)
                                }
                                
                            }
                        }
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                //failure
            }
        })
    }
    
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnSubmit(_ sender:UIBarButtonItem) {
        
        if isUpdate {
            self.updateJobDetail()
        }else{
            let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
            
            var parameters = [String : Any]()
            parameters["i_key"] = WebURL.appKey
            parameters["user_id"] = userDetail["user_id"]
            parameters["cat_id"] = hireDetail["CategoryID"]
            parameters["company_name"] = hireDetail["CompanyName"]
            parameters["job_title"] = hireDetail["jobTitle"]
            parameters["skill_required"] = "60"
            parameters["additional_skill"] = "30"
            parameters["extra_skill"] = hireDetail["ExtraSkill"]
            parameters["start_date"] = hireDetail["StartDate"]
            parameters["skill_1"] = hireDetail["requiredSkillValue"]
            parameters["skill_2"] = hireDetail["additionalSkillValue"]
            parameters["skill_3"] = hireDetail["ExtraSkill"]
            parameters["pay_per_hour"] = hireDetail["PayPer"]
            
            appDelegate.showHud()
            
            upload(multipartFormData: { (multipartFormData) in
                if let image = self.hireDetail["imgHire"] as? UIImage {
                    let data1 = UIImageJPEGRepresentation(image, 0.7)
                    multipartFormData.append(data1!, withName: "image", fileName: "Hire\(arc4random_uniform(3) + 1).jpg", mimeType: "image/jpg")
                }
                
                for (key, value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                
            }, to: WebURL.addHire, method: .post, encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.validate()
                    upload.responseJSON { response in
                        appDelegate.hideHud()
                        if response.result.error != nil {
                            print("failure")
                            UIAlertView(title: "Fail", message: "Please retry again.", delegate: nil, cancelButtonTitle: "Ok").show()
                        } else {
                            print("success")
                            print("addHire Result:\(response)")
                            if response.result.isSuccess {
                                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                                    if (dictResult["o_returnCode"] as! Bool){
                                        let successMsg = dictResult["o_message"] as! String
                                        showAlert(msg: successMsg, completion: { (true) in
                                            self.presentingViewController?.dismiss(animated: true, completion: nil)
                                        })
                                    }else{
                                        let errorMsg = dictResult["o_message"] as! String
                                        appDelegate.showAlertMessage(strMessage: errorMsg)
                                    }
                                    
                                }
                            }
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    showAlert(msg: "This application requires internet connection", completion: nil)
                    
                    //failure
                }
            })
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
