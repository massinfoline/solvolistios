//
//  SellProductStep1.swift
//  SolvoList
//
//  Created by ronak patel on 05/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import MobileCoreServices

class SellProductStep1: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    let imagePickerView = UIImagePickerController()
    var imageProCover:UIImage!
    @IBOutlet var txtTitle:UITextField!
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var containerView:UIView!
    var isUpdate:Bool = false
    @IBOutlet var progressBar:ABSteppedProgressBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black];
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgClose"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Sell Item"
        
        DispatchQueue.main.async { // Dispatch in order to render the subtitle in the next runloop
            self.addSubtitles()
        }
        
        if isUpdate {
            let dictDetail = getMyUserDefaults(key: MyUserDefaults.prodUpdateDetail) as! [String:Any]
            txtTitle.text = dictDetail["product_name"] as? String
            let imgView = UIImageView()
            imgView.sd_setImage(with: URL(string: dictDetail["product_cover_image"] as! String), completed: { (image, error, nil, url) in
                self.imageProCover = image
            })
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Global Methods
    
    private func addSubtitles() {
        let subtitleVerticalPosition: CGFloat = self.progressBar.frame.origin.y + self.progressBar.bounds.height + 2
        for (idx, point) in self.progressBar.centerPoints.enumerated() {
            let realPoint = self.progressBar.convert(point, to: self.containerView)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 60, height: 20))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.font = UIFont(name: FontName.OpenSansRegular, size: 14.0)
            subtitle.text = arrSellTab[idx]
            self.containerView.addSubview(subtitle)
        }
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedOnTakePhoto(_ sender:UIControl) {
        _ = PresentPhotoCamera(target: self, canEdit: false)
    }
    
    @IBAction func tappedOnSelectPhoto(_ sender:UIControl) {
        _ = PresentPhotoLibrary(target: self, canEdit: false)
    }
    
    @IBAction func tappedOnNext(_ sender:UIControl) {
        if txtTitle.text == ""  {
            showAlert(msg: "Please enter title", completion: nil)
        }else if imageProCover == nil {
            showAlert(msg: "Please select product image.", completion: nil)
        }else{
            let step2 = storyBoards.Sell.instantiateViewController(withIdentifier: "SellProductStep2") as! SellProductStep2
            step2.productDetail = ["CoverImage":imageProCover,"Title":txtTitle.text ?? ""];
            step2.isUpdate = self.isUpdate
            self.navigationController?.pushViewController(step2, animated: true)
        }
    }
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imageProCover = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true) { 
            let step2 = storyBoards.Sell.instantiateViewController(withIdentifier: "SellProductStep2") as! SellProductStep2
            step2.productDetail = ["CoverImage":self.imageProCover,"Title":self.txtTitle.text ?? ""];
            step2.isUpdate = self.isUpdate
            self.navigationController?.pushViewController(step2, animated: false)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
