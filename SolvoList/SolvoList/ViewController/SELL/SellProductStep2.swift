//
//  SellProductStep2.swift
//  SolvoList
//
//  Created by ronak patel on 06/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import AVKit


class SellProductStep2: UIViewController,ELCImagePickerControllerDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate {

    var productDetail = [String:Any]()
    var isUpdate:Bool = false
    @IBOutlet var imageProCover:UIImageView!
    @IBOutlet var txtTitle:UITextField!
    @IBOutlet var lblImgCount:UILabel!
    @IBOutlet var lblVideoCount:UILabel!
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var containerView:UIView!
    var urlProdVideo:NSURL!
    
    var arrUploadImages = [AnyObject]()
    @IBOutlet var progressBar:ABSteppedProgressBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black];
        self.title = "Sell Item"
        
        if isUpdate {
            let dictDetail = getMyUserDefaults(key: MyUserDefaults.prodUpdateDetail) as! [String:Any]
            txtTitle.text = dictDetail["product_name"] as? String
            imageProCover.sd_setImage(with: URL(string: dictDetail["product_cover_image"] as! String), completed: nil)
        }
        
        imageProCover.image = productDetail["CoverImage"] as? UIImage
        txtTitle.text = productDetail["Title"] as? String
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        self.lblImgCount.isHidden = true
        self.lblVideoCount.isHidden = true
        
        
        DispatchQueue.main.async {
            self.addSubtitles()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Global Methods
    private func addSubtitles() {
        let subtitleVerticalPosition: CGFloat = self.progressBar.frame.origin.y + self.progressBar.bounds.height + 2
        for (idx, point) in self.progressBar.centerPoints.enumerated() {
            let realPoint = self.progressBar.convert(point, to: self.containerView)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 60, height: 20))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.font = UIFont(name: FontName.OpenSansRegular, size: 14.0)
            subtitle.text = arrSellTab[idx]
            self.containerView.addSubview(subtitle)
        }
        progressBar.currentIndex = 0
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    
    
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedToUploadImage(_ sender:UIControl) {
        let elcPicker:ELCImagePickerController = ELCImagePickerController(imagePicker: ())
        elcPicker.maximumImagesCount = 4; //Set the maximum number of images to select to 100
        elcPicker.returnsOriginalImage = true; //Only return the fullScreenImage, not the fullResolutionImage
        elcPicker.returnsImage = true; //Return UIimage if YES. If NO, only return asset location information
        elcPicker.onOrder = true; //For multiple image selection, display and return order of selected images
        elcPicker.mediaTypes = [kUTTypeImage] //[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
        
        elcPicker.imagePickerDelegate = self;
        self.present(elcPicker, animated: true, completion: nil)
    }
    
    @IBAction func tappedToUploadVideo(_ sender:UIControl) {
        _ = PresentVideoLibrary(target: self, canEdit: true, length: 10)
    }
    
    @IBAction func tappedToNext(_ sender:UIControl) {
        
        if self.urlProdVideo != nil {
            productDetail["VideoURL"] = urlProdVideo.absoluteString
            let imgVideo = self.getThumbnailImage(forUrl: urlProdVideo as URL)
            productDetail["imgVideo"] = imgVideo
        }else{
            productDetail["VideoURL"] = ""
        }
        
        if self.arrUploadImages.count > 0 {
            productDetail["ProductGallery"] = arrUploadImages
        }else{
            productDetail["ProductGallery"] = [AnyObject]()
        }
        
        if txtTitle.text == ""  {
            showAlert(msg: "Please enter title", completion: nil)
        }else{
            self.productDetail["Title"] = txtTitle.text ?? ""
            let step3 = storyBoards.Sell.instantiateViewController(withIdentifier: "SellProductStep3") as! SellProductStep3
            step3.isUpdate = self.isUpdate
            step3.productDetail = self.productDetail;
            self.navigationController?.pushViewController(step3, animated: true)
        }
    }
    
    
    //MARK: - ELCImagePickerControllerDelegate
    func elcImagePickerController(_ picker: ELCImagePickerController!, didFinishPickingMediaWithInfo info: [Any]!) {
        
        print("selected image \(info)")
        let arrImageAssest = info
        for imageAssest in arrImageAssest! {
            let image = imageAssest as! [String:AnyObject]
            arrUploadImages.append(image[UIImagePickerControllerOriginalImage]!)
        }
        self.lblImgCount.isHidden = false
        lblImgCount.text = "\(arrUploadImages.count)"
        self.dismiss(animated: true, completion: nil)
    }
    
    func elcImagePickerControllerDidCancel(_ picker: ELCImagePickerController!) {
        self.dismiss(animated: true, completion: nil)
    }

    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        urlProdVideo = info[UIImagePickerControllerMediaURL] as! NSURL;
        self.lblVideoCount.isHidden = false
        self.lblVideoCount.text = "\(1)"
        print("Video URL:\(urlProdVideo)")
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(0, 1) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
