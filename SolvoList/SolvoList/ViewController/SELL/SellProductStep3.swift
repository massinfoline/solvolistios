//
//  SellProductStep3.swift
//  SolvoList
//
//  Created by ronak patel on 07/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit

class SellProductStep3: UIViewController,UITextFieldDelegate,UITextViewDelegate {

    var productDetail = [String:Any]()
    var isUpdate:Bool = false
    @IBOutlet var txtCategory:UITextField!
    @IBOutlet var txtStatus:UITextField!
    @IBOutlet var txtDescription:UITextView!
    
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var containerView:UIView!
    @IBOutlet var progressBar:ABSteppedProgressBar!
    
    let chooseCategory = DropDown()
    let chooseStatus = DropDown()
    var arrCategory = [Any]()
    var strProdCate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black];
        self.title = "Sell Item"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        DispatchQueue.main.async {
            self.addSubtitles()
        }
        for dict in appDelegate.arrCategory {
            let dictCate = dict as! [String:Any]
            arrCategory.append(dictCate["cat_name"] ?? "")
        }
        
        self.setupChooseCategoryDropDown()
        self.setupChooseStatusDropDown()
        
        if isUpdate {
            let dictDetail = getMyUserDefaults(key: MyUserDefaults.prodUpdateDetail) as! [String:Any]
            txtStatus.text = dictDetail["product_status"] as? String
            let namePredicate = NSPredicate(format: "id CONTAINS[C] '\(dictDetail["cat_id"] as! String)'")
            let arrCate = appDelegate.arrCategory.filter { namePredicate.evaluate(with: $0) }
            if arrCate.count > 0 {
                let dictCate = arrCate[0] as! [String:Any]
                txtCategory.text = dictCate["cat_name"] as? String
            }
            
            txtDescription.text = dictDetail["product_description"] as! String
        }
        
        //self.customizeDropDown()
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    //MARK: - Global Methods
    
    private func addSubtitles() {
        let subtitleVerticalPosition: CGFloat = self.progressBar.frame.origin.y + self.progressBar.bounds.height + 2
        for (idx, point) in self.progressBar.centerPoints.enumerated() {
            let realPoint = self.progressBar.convert(point, to: self.containerView)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 60, height: 20))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.font = UIFont(name: FontName.OpenSansRegular, size: 14.0)
            subtitle.text = arrSellTab[idx]
            self.containerView.addSubview(subtitle)
        }
        progressBar.currentIndex = 1
    }
    
    func setupChooseCategoryDropDown() {
        chooseCategory.anchorView = txtCategory
        chooseCategory.dismissMode = .onTap
        chooseCategory.direction = .bottom
        chooseCategory.bottomOffset = CGPoint(x: 0, y: txtCategory.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseCategory.dataSource = arrCategory as! [String]
        
        // Action triggered on selection
        chooseCategory.selectionAction = { [unowned self] (index, item) in
            self.txtCategory.text = item
            let dictCate = appDelegate.arrCategory[index] as! [String:Any]
            self.strProdCate = dictCate["id"] as! String
        }
    }
    
    func setupChooseStatusDropDown() {
        chooseStatus.anchorView = txtCategory
        chooseStatus.dismissMode = .onTap
        chooseStatus.direction = .bottom
        chooseStatus.bottomOffset = CGPoint(x: 0, y: txtStatus.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseStatus.dataSource = ["New","Reconditioned/Certified","Open Box","Used","For Parts","Other description"]
        
        // Action triggered on selection
        chooseStatus.selectionAction = { [unowned self] (index, item) in
            self.txtStatus.text = item
        }
    }
    
    func customizeDropDown() {
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray

        
        
            /*** FOR CUSTOM CELLS ***/
            chooseCategory.cellNib = UINib(nibName: "CategoryCell", bundle: nil)
            
            chooseCategory.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                guard let cell = cell as? CategoryCell else { return }
                let dictCate = appDelegate.arrCategory[index] as! [String:Any]
                cell.imgCategory.sd_setImage(with: URL(string: dictCate["cat_image"] as! String), completed: nil)
                // Setup your custom UI components
                //cell.lblName.text = "Suffix \(index)"
            }
            /*** ---------------- ***/
    }
    
    // MARK: - Keyboard Navigation
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        //let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        //previousButton.customView = self.prevNextSegment()
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 100
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedToNext(_ sender:UIControl) {
        if txtCategory.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please select category")
        }else if txtStatus.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please select status")
        }else if txtDescription.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter description status")
        }else if txtDescription.text.length > 300 {
            appDelegate.showAlertMessage(strMessage: "Description must be less then 100 characters.")
        }else{
            self.productDetail["ProductCategoryID"] = self.strProdCate
            self.productDetail["ProductCategory"] = txtCategory.text
            self.productDetail["ProductStatus"] = txtStatus.text
            self.productDetail["ProductDescription"] = txtDescription.text
            let step4 = storyBoards.Sell.instantiateViewController(withIdentifier: "SellProductStep4") as! SellProductStep4
            step4.isUpdate = self.isUpdate
            step4.productDetail = self.productDetail;
            self.navigationController?.pushViewController(step4, animated: true)
        }
    }
    
    
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCategory {
            self.customizeDropDown()
            chooseCategory.show()
            return false
        }else if textField == txtStatus {
            chooseStatus.show()
            return false
        }
        return true
    }
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
