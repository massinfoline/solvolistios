//
//  CategoryCell.swift
//  SolvoList
//
//  Created by ronak patel on 07/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit

class CategoryCell: DropDownCell {

    @IBOutlet var lblName:UILabel!
    @IBOutlet var imgCategory:UIImageView!
    
}
