//
//  SellProductStep5.swift
//  SolvoList
//
//  Created by ronak patel on 12/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire
class ProductImage: UICollectionViewCell {
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var imgPlayIcon: UIImageView!
}

class SellProductStep5: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var productDetail = [String:Any]()
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var containerView:UIView!
    @IBOutlet var progressBar:ABSteppedProgressBar!
    
    @IBOutlet var imgProductCover: UIImageView!
    @IBOutlet var collectionImgProd: UICollectionView!
    @IBOutlet var imgNegoPrice: UIImageView!
    @IBOutlet var imgFixedPrice: UIImageView!
    @IBOutlet var lblProduct: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblProdDescription: UILabel!
    
    @IBOutlet var lblPrice: UILabel!
    var arrProductImage = [Any]()
    
    var isUpdate:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black];
        self.title = "Sell Item"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        DispatchQueue.main.async {
            self.addSubtitles()
        }
        self.showProdcutDetail()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Global Methods
    
    private func addSubtitles() {
        let subtitleVerticalPosition: CGFloat = self.progressBar.frame.origin.y + self.progressBar.bounds.height + 2
        for (idx, point) in self.progressBar.centerPoints.enumerated() {
            let realPoint = self.progressBar.convert(point, to: self.containerView)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 60, height: 20))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.font = UIFont(name: FontName.OpenSansRegular, size: 14.0)
            subtitle.text = arrSellTab[idx]
            self.containerView.addSubview(subtitle)
        }
        progressBar.currentIndex = 3
    }
    
    
    func showProdcutDetail() {
        print("Product Detail:\(productDetail)")
        imgProductCover.image = productDetail["CoverImage"] as? UIImage
        lblProduct.text = productDetail["Title"] as? String
        lblProdDescription.text = productDetail["ProductDescription"] as? String
        lblPrice.text = "$\(productDetail["ProductPrice"] as! String)"
        lblLocation.text = ""
        if productDetail["ProductPriceType"] as! String == "Fixed" {
            imgFixedPrice.isHighlighted = true
            imgNegoPrice.isHighlighted = false
        }else{
            imgFixedPrice.isHighlighted = false
            imgNegoPrice.isHighlighted = true
        }
        if (productDetail["ProductGallery"] != nil) {
            self.arrProductImage = productDetail["ProductGallery"] as! [Any]
            self.collectionImgProd.reloadData()
        }
    }
    
    func updateProductDetail() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if userDetail == nil {
            appDelegate.showAlertMessage(strMessage: "Please login first.")
            return
        }
        let prodDetail = getMyUserDefaults(key: MyUserDefaults.prodUpdateDetail) as! [String:Any]
        let parameters = ["i_key":WebURL.appKey,
                          "product_name":productDetail["Title"] ?? "",
                          "product_description":productDetail["ProductDescription"] ?? "",
                          "product_category":productDetail["ProductCategoryID"] ?? "",
                          "product_status":productDetail["ProductStatus"] ?? "",
                          "product_price":productDetail["ProductPrice"] ?? "",
                          "product_isFixed":productDetail["ProductPriceType"] ?? "",
                          "user_id":userDetail["user_id"] ?? "",
                          "product_id":prodDetail["product_id"] ?? ""] as [String : Any]
        
        appDelegate.showHud()
        
        
        upload(multipartFormData: { (multipartFormData) in
            if self.productDetail["CoverImage"] != nil {
                
                let data1 = UIImageJPEGRepresentation(self.productDetail["CoverImage"] as! UIImage, 0.7)
                multipartFormData.append(data1!, withName: "product_cover_picture", fileName: "Product\(arc4random_uniform(3) + 1).jpg", mimeType: "image/jpg")
            }
            for img in self.arrProductImage {
                let image = img as! UIImage
                let data1 = UIImageJPEGRepresentation(image, 0.7)
                multipartFormData.append(data1!, withName: "product_gallery[]", fileName: "Product.jpg", mimeType: "image/jpeg")
            }
            let videoURL = self.productDetail["VideoURL"] as! String
            if videoURL != "" {
                multipartFormData.append(URL(string:videoURL)!, withName: "product_video")
            }
            
            for (key, value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            
        }, to: WebURL.updateProductDetail, method: .post, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                
                
                upload.validate()
                upload.responseJSON { response in
                    
                    appDelegate.hideHud()
                    if response.result.error != nil {
                        print("failure")
                        UIAlertView(title: "Fail", message: "Please retry again.", delegate: nil, cancelButtonTitle: "Ok").show()
                    } else {
                        print("success")
                        print("addProduct Result:\(response)")
                        if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                            if (dictResult["o_returnCode"] as! Bool){
                                let successMsg = dictResult["o_message"] as! String
                                showAlert(msg: successMsg, completion: { (true) in
                                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                                })
                            }else{
                                setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
                                let errorMsg = dictResult["o_message"] as! String
                                appDelegate.showAlertMessage(strMessage: errorMsg)
                            }
                        }
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                //failure
            }
        })
    }
    
    func uploadProduct()  {
        
        
        if isUpdate {
            self.updateProductDetail()
        }else {
            
            let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
            if userDetail == nil {
                appDelegate.showAlertMessage(strMessage: "Please login first.")
                return
            }
            let parameters = ["i_key":WebURL.appKey,
                              "product_name":productDetail["Title"] ?? "",
                              "product_description":productDetail["ProductDescription"] ?? "",
                              "product_category":productDetail["ProductCategoryID"] ?? "",
                              "product_status":productDetail["ProductStatus"] ?? "",
                              "product_price":productDetail["ProductPrice"] ?? "",
                              "product_isFixed":productDetail["ProductPriceType"] ?? "",
                              "user_id":userDetail["user_id"] ?? ""] as [String : Any]
            
            appDelegate.showHud()
            
            
            upload(multipartFormData: { (multipartFormData) in
                if self.productDetail["CoverImage"] != nil {
                    
                    let data1 = UIImageJPEGRepresentation(self.productDetail["CoverImage"] as! UIImage, 0.7)
                    multipartFormData.append(data1!, withName: "product_cover_picture", fileName: "Product\(arc4random_uniform(3) + 1).jpg", mimeType: "image/jpg")
                }
                for img in self.arrProductImage {
                    let image = img as! UIImage
                    let data1 = UIImageJPEGRepresentation(image, 0.7)
                    multipartFormData.append(data1!, withName: "product_gallery[]", fileName: "Product.jpg", mimeType: "image/jpeg")
                }
                let videoURL = self.productDetail["VideoURL"] as! String
                if videoURL != "" {
                    multipartFormData.append(URL(string:videoURL)!, withName: "product_video")
                }
                
                for (key, value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                
            }, to: WebURL.addProduct, method: .post, encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    
                    upload.validate()
                    upload.responseJSON { response in
                        
                        appDelegate.hideHud()
                        if response.result.error != nil {
                            print("failure")
                            UIAlertView(title: "Fail", message: "Please retry again.", delegate: nil, cancelButtonTitle: "Ok").show()
                        } else {
                            print("success")
                            print("addProduct Result:\(response)")
                            if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                                if (dictResult["o_returnCode"] as! Bool){
                                    let successMsg = dictResult["o_message"] as! String
                                    showAlert(msg: successMsg, completion: { (true) in
                                        self.presentingViewController?.dismiss(animated: true, completion: nil)
                                    })
                                }else{
                                    setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
                                    let errorMsg = dictResult["o_message"] as! String
                                    appDelegate.showAlertMessage(strMessage: errorMsg)
                                }
                            }
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    showAlert(msg: "This application requires internet connection", completion: nil)
                    //failure
                }
            })
        }
    }
    
    
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnSubmit(_ sender:UIBarButtonItem) {
        uploadProduct()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if productDetail["VideoURL"] as! String == "" {
            return arrProductImage.count
        }else {
            return arrProductImage.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductImage", for: indexPath) as! ProductImage
        if self.arrProductImage.count > indexPath.row {
            cell.imgProduct.image = self.arrProductImage[indexPath.row] as? UIImage
            cell.imgPlayIcon.isHidden = true
        }else{
            cell.imgProduct.image = productDetail["imgVideo"] as? UIImage
            cell.imgPlayIcon.isHidden = false
        }
        
        return cell
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
