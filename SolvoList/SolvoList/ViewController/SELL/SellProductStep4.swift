//
//  SellProductStep4.swift
//  SolvoList
//
//  Created by ronak patel on 09/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit

class SellProductStep4: UIViewController,UITextFieldDelegate {

    var productDetail = [String:Any]()
    var isUpdate:Bool = false
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var containerView:UIView!
    @IBOutlet var progressBar:ABSteppedProgressBar!
    
    
    @IBOutlet var txtPrice:UITextField!
    @IBOutlet var txtPriceType:UITextField!
    
    let choosePriceType = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black];
        self.title = "Sell Item"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        DispatchQueue.main.async {
            self.addSubtitles()
        }
        
        txtPrice.addTarget(self, action: #selector(myTextFieldDidChange), for: .editingChanged)
        
        self.setupChoosePriceTypeDropDown()
        
        if isUpdate {
            let dictDetail = getMyUserDefaults(key: MyUserDefaults.prodUpdateDetail) as! [String:Any]
            txtPrice.text = dictDetail["product_price"] as? String
            txtPriceType.text = dictDetail["product_isfixed"] as? String
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Global Methods
    
    private func addSubtitles() {
        let subtitleVerticalPosition: CGFloat = self.progressBar.frame.origin.y + self.progressBar.bounds.height + 2
        for (idx, point) in self.progressBar.centerPoints.enumerated() {
            let realPoint = self.progressBar.convert(point, to: self.containerView)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 60, height: 20))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.font = UIFont(name: FontName.OpenSansRegular, size: 14.0)
            subtitle.text = arrSellTab[idx]
            self.containerView.addSubview(subtitle)
        }
        progressBar.currentIndex = 2
    }
    
    func setupChoosePriceTypeDropDown() {
        choosePriceType.anchorView = txtPriceType
        choosePriceType.dismissMode = .onTap
        choosePriceType.direction = .top
        choosePriceType.bottomOffset = CGPoint(x: 0, y: txtPriceType.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        choosePriceType.dataSource = ["Fixed","Negotiable"]
        
        // Action triggered on selection
        choosePriceType.selectionAction = { [unowned self] (index, item) in
            self.txtPriceType.text = item
        }
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedToNext(_ sender:UIControl) {
        var price:String = (txtPrice.text?.replacingOccurrences(of: "$", with: ""))!//txtPrice.text!
        price = price.replacingOccurrences(of: ",", with: "")
        if txtPrice.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter price.")
        }else if !price.isNumber() {
            appDelegate.showAlertMessage(strMessage: "Please enter valid price.")
        }else if txtPriceType.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please select price type.")
        }else {
            self.productDetail["ProductPrice"] = price
            self.productDetail["ProductPriceType"] = txtPriceType.text
            let step5 = storyBoards.Sell.instantiateViewController(withIdentifier: "SellProductStep5") as! SellProductStep5
            step5.productDetail = self.productDetail;
            step5.isUpdate = self.isUpdate
            self.navigationController?.pushViewController(step5, animated: true)
        }
    }
    
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtPriceType {
            choosePriceType.show()
            return false
        }
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    func myTextFieldDidChange(_ textField: UITextField) {
        
        if let amountString = textField.text?.currencyInputFormatting() {
            textField.text = amountString
        }
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    
    // formatting text for currency textField
    func currencyInputFormatting() -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        if #available(iOS 9.0, *) {
            formatter.numberStyle = .currencyAccounting
        } else {
            // Fallback on earlier versions
        }
        formatter.currencySymbol = "$"
        formatter.maximumFractionDigits = 0
        formatter.minimumFractionDigits = 0
        
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: double)
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }
        
        return formatter.string(from: number)!
    }
}
