//
//  SellViewController.swift
//  SolvoList
//
//  Created by ronak patel on 15/09/17.
//  Copyright © 2017 depixed.com. All rights reserved.
//

import UIKit

class SellViewController: UIViewController {
    var isOpenSell = true
    override func awakeFromNib() {
        self.tabBarItem.title = "Sell"
        self.title = "Sell"
        self.tabBarItem.image = UIImage(named: "imgSell")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isOpenSell {
            self.isOpenSell = false
            let sellStep1 = storyBoards.Sell.instantiateViewController(withIdentifier: "SellProductStep1") as! SellProductStep1
            let navController1 = NavigationController(rootViewController: sellStep1)
            present(navController1, animated: false, completion: nil)
        }else{
            self.isOpenSell = true;
            appDelegate.tabBarController.selectedIndex = 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
