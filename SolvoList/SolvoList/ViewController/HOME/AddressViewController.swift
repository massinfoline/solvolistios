//
//  AddressViewController.swift
//  SolvoList
//
//  Created by ronak patel on 09/01/18.
//  Copyright © 2018 MassInfo. All rights reserved.
//

import UIKit

class AddressViewController: UIViewController {

    
    @IBOutlet var txtPhoneNumber:UITextField!
    @IBOutlet var txtEmail:UITextField!
    @IBOutlet var txtAddress:UITextField!
    @IBOutlet var txtCity:UITextField!
    @IBOutlet var txtState:UITextField!
    @IBOutlet var txtZipCode:UITextField!
    var productDetail:[String:Any] = [:]
    var strTextFieldSelected:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Address"
        // Do any additional setup after loading the view.
    }

    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedOnNext(_ sender:UIButton) {
        
        if (txtPhoneNumber.text?.length)! < 7 {
            appDelegate.showAlertMessage(strMessage: "Please enter phone number")
        }else if !((txtEmail.text?.isValidEmail())!)  {
            appDelegate.showAlertMessage(strMessage: "Please enter valid email id")
        }else if (txtAddress.text?.length)! < 5 {
            appDelegate.showAlertMessage(strMessage: "Please enter address")
        }else if (txtCity.text?.length)! < 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter city")
        }else if (txtState.text?.length)! < 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter state")
        }else if txtZipCode.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter zip code")
        }else {
        let paymentView = storyBoards.Main.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        paymentView.productDetail = self.productDetail
        paymentView.addressDetail = ["address":txtAddress.text!,
                                     "city":txtCity.text!,
                                     "state":txtState.text!,
                                     "email":txtEmail.text!,
                                     "phone":txtPhoneNumber.text!,
                                     "pincode":txtZipCode.text!]
        paymentView.isSellProd = true
        self.navigationController?.pushViewController(paymentView, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtPhoneNumber {
            strTextFieldSelected = "txtPhoneNumber"
        }else if textField == txtEmail  {
            strTextFieldSelected = "txtEmail"
        }else if textField == txtAddress  {
            strTextFieldSelected = "txtAddress"
        }else if textField == txtZipCode  {
            strTextFieldSelected = "txtZipCode"
        }else if textField == txtCity  {
            strTextFieldSelected = "txtCity"
        }else if textField == txtState  {
            strTextFieldSelected = "txtState"
        }
        
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtPhoneNumber" {
                txtEmail.becomeFirstResponder()
            }else if strTextFieldSelected == "txtEmail" {
                txtAddress.becomeFirstResponder()
            }else if strTextFieldSelected == "txtAddress" {
                txtCity.becomeFirstResponder()
            }else if strTextFieldSelected == "txtCity" {
                txtState.becomeFirstResponder()
            }else if strTextFieldSelected == "txtState" {
                txtZipCode.becomeFirstResponder()
            }else if strTextFieldSelected == "txtZipCode" {
                txtZipCode.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtPhoneNumber" {
                txtPhoneNumber.resignFirstResponder()
            }else if strTextFieldSelected == "txtEmail" {
                txtPhoneNumber.becomeFirstResponder()
            }else if strTextFieldSelected == "txtAddress" {
                txtEmail.becomeFirstResponder()
            }else if strTextFieldSelected == "txtCity" {
                txtAddress.becomeFirstResponder()
            }else if strTextFieldSelected == "txtState" {
                txtCity.becomeFirstResponder()
            }else if strTextFieldSelected == "txtZipCode" {
                txtState.becomeFirstResponder()
            }
        }
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
