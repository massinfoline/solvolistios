//
//  MakeOfferViewController.swift
//  SolvoList
//
//  Created by ronak patel on 18/11/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire
class MakeOfferViewController: UIViewController {

    @IBOutlet var lblProName:UILabel!
    @IBOutlet var txtPrice:UITextField!
    var dictProd = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Make Offer"
        self.lblProName.text = "Enter your offer for \(String(describing: dictProd["product_name"]!))"
        self.txtPrice.text = "$\(String(describing: dictProd["product_price"]!))"
        txtPrice.addTarget(self, action: #selector(priceTextFieldDidChange), for: .editingChanged)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnMakeOffer(_ sender:UIBarButtonItem) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if userDetail == nil {
            appDelegate.showAlertMessage(strMessage: "Please login first.")
            return
        }
        var price:String = txtPrice.text!.replacingOccurrences(of: "$", with: "")
        price = price.replacingOccurrences(of: ",", with: "")
        if price == ""{
            appDelegate.showAlertMessage(strMessage: "Please enter valid price")
            return
        }
        
        if !price.isNumber() {
            appDelegate.showAlertMessage(strMessage: "Please enter valid price")
            return
        }
        
        if Int(price)! < 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter valid price")
            return
        }
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? "",
                          "product_id":dictProd["product_id"] ?? "0",
                          "price":price] as [String : Any]
        appDelegate.showHud()
        request(WebURL.makeOffer, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("makeOffer:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.sendMakeOfferMessage()
                        showAlert(msg: dictResult["o_message"] as! String, completion: { (true) in
                            self.navigationController?.popToRootViewController(animated: true)
                        })
                    }else{
                        showAlert(msg: dictResult["o_message"] as! String, completion: { (true) in
                        })
                    }
                    
                }
            }
        })
    }
    
    func sendMakeOfferMessage() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if let userEmail = userDetail["email"] as? String {
            FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
                if let err:Error = error {
                    print(err.localizedDescription)
                    return
                }
                //let strUID = FIRAuth.auth()?.currentUser?.uid
                let dictUser = self.dictProd["userprofile"] as! [String:Any]
                print("Email:\(userEmail)")
                let strProdEmail = dictUser["user_email"] as! String
                print("MD5: \(self.chatId([userEmail,strProdEmail]))")
                let groupID = self.chatId([userEmail,strProdEmail])
                
                //let strRecentID1 = self.chatId([groupID,userEmail])
                
                let object = FObject(path: FRECENT_PATH)
                object[FRECENT_OBJECTID] = groupID
                object[FRECENT_USERID] = dictUser["user_id"] as! String
                object[FRECENT_CURRENTUSERID] = userDetail["user_id"] as! String
                
                object[FRECENT_NAME] = dictUser["user_name"] as! String
                object[FRECENT_CURRENTNAME] = userDetail["name"] as! String
                
                object[FRECENT_EMAIL] = dictUser["user_email"] as! String
                object[FRECENT_CURRENTEMAIL] = userDetail["email"] as! String
                
                object[FRECENT_PICTURE] = dictUser["profile_picture"] as! String
                object[FRECENT_CURRENTPICTURE] = userDetail["profile_picture"] as! String
                object[FRECENT_MSGID] = groupID
                object.saveInBackground()
                
                
                let msgObject = FObject(path: FMESSAGE_PATH, subpath:groupID)
                let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
                
                msgObject[MESSAGE_TEXT] = "I would like to buy \(self.txtPrice.text!)"
                msgObject[FMESSAGE_SENDERID] = FIRAuth.auth()?.currentUser?.uid ?? ""
                msgObject[FMESSAGE_SENDERNAME] = dictUser["user_name"] ?? ""
                msgObject[FMESSAGE_DEVICETYPE] = "ios"
                msgObject[FMESSAGE_DEVICETOKEN] = getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken)
                msgObject[FMESSAGE_CURRENTDEVICETYPE] = dictUser["device_type"] as! String
                msgObject[FMESSAGE_CURRENTDEVICETOKEN] = dictUser["device_id"] as! String
                msgObject[FMESSAGE_CURRENTUSERID] = userDetail["user_id"] ?? ""
                msgObject[FMESSAGE_USERID] = dictUser["user_id"] ?? ""
                msgObject.saveInBackground()
                
                let dictBody:[String:String] = ["email":userEmail,
                                                "text":msgObject[MESSAGE_TEXT] as! String,
                                                "username":userDetail["name"] as! String,
                                                "photo":"",
                                                "fcm_token": getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String,
                                                "currentEmail":userDetail["email"] as! String,
                                                "current_userid":userDetail["user_id"] as! String,
                                                "sound":"default"]
                print(dictBody)
                appDelegate.sendPushMessage(strToken: dictUser["device_id"] as! String, deviceType: dictUser["device_type"] as! String, dictBody: dictBody)
                
            })
        }
    }
    
    func chatId(_ members: [String]) -> String {
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        let sorted = members.sorted { $0.compare($1 , options: .numeric) == .orderedAscending }
        let strMDSHASH = Checksum.md5Hash(of: sorted.joined(separator: ""))
        return strMDSHASH!
        //return Checksum.md5Hash(sorted.joined(separator: ""))
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        //let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        //previousButton.customView = self.prevNextSegment()
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    func priceTextFieldDidChange(_ textField: UITextField) {
        
        if let amountString = textField.text?.currencyInputFormatting() {
            textField.text = amountString
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
