//
//  ProductDetailViewController.swift
//  SolvoList
//
//  Created by Jaydeep on 10/12/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation
import AVKit
import Firebase

class ImageCell: UICollectionViewCell {
    @IBOutlet var imgProduct: UIImageView!
}

class VideoCell: UICollectionViewCell {
    @IBOutlet var imgProduct: UIImageView!
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
}

class ProductDetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate,MKMapViewDelegate {

    var dictProduct = [String:Any]()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
   
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var viewProductPrice: UIView!
   
    @IBOutlet weak var lblProductTitle: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
   
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserProductDescription: UILabel!
    @IBOutlet var collectionGallery:UICollectionView!

    @IBOutlet var btnAksOffer: UIButton!
    @IBOutlet var btnMakeOffer: UIButton!
    @IBOutlet var btnBuy: UIButton!
    
    var arrProductGallery = [String]()
    
    @IBOutlet var btnProductPromote: UIButton!
    
    //PromoteView
    @IBOutlet var viewPromoteContainer: UIView!
    
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnPromote: UIButton!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var imgPromote: UIImageView!
    @IBOutlet var btnTagSold: UIButton!
    @IBOutlet var btnDeleteJob: UIButton!
    @IBOutlet var lblPromotePrice: UILabel!
   
    @IBOutlet weak var lblKm: UILabel!
    @IBOutlet var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true;
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewPromoteContainer.isHidden = true
        self.navigationController?.navigationBar.isHidden = true;
        imgProduct.sd_setImage(with: URL(string: dictProduct["product_cover_image"] as! String)) { (image, error, nil, url) in
            
        }
        lblProductPrice.text = "$\(dictProduct["product_price"] as! String)"
        lblProductTitle.text = dictProduct["product_name"] as? String
        lblDescription.text = dictProduct["product_description"] as? String
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormate.YYYYMMDD24
        let uploadDate = dateFormatter.date(from: dictProduct["created_datetime"] as! String)
        let strDateSince:String = (uploadDate?.timeAgoSinceDate(numericDates: true))!
        lblMonth.text = "Post \(String(describing: strDateSince))"
        
        if let arrImage = dictProduct["product_gallery"] as? [String] {
            self.arrProductGallery = arrImage
        }
        
        //---------------UserDetails---------------
        let dictUser = self.dictProduct["userprofile"] as! [String:Any]
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        if userDetail["user_id"] as! String == dictUser["user_id"] as! String {
            btnAksOffer.isHidden = true
            btnMakeOffer.isHidden = true
            btnProductPromote.isHidden = false
        }else{
            btnAksOffer.isHidden = false
            btnMakeOffer.isHidden = false
            btnProductPromote.isHidden = true
        }
        
        if dictProduct["solvolist"] as! String == "1" {
            btnAksOffer.isHidden = true
            btnMakeOffer.isHidden = true
            btnBuy.isHidden = false
        }else{
            if userDetail["user_id"] as! String == dictUser["user_id"] as! String {
                btnAksOffer.isHidden = true
                btnMakeOffer.isHidden = true
                btnProductPromote.isHidden = false
            }else{
                btnAksOffer.isHidden = false
                btnMakeOffer.isHidden = false
                btnProductPromote.isHidden = true
            }
            btnBuy.isHidden = true
        }
        
        lblLocation.text = dictUser["user_address"] as? String
        self.imgUser.sd_setImage(with: URL(string: dictUser["profile_picture"] as! String)) { (image, error, nil, url) in
            if image == nil {
                self.imgUser.image = UIImage(named: "imgUser")
            }
        }
        lblUserName.text = dictUser["user_name"] as? String
        lblUserProductDescription.text = ""
        let latitude = Double(dictUser["user_latitude"] as! String)
        let longitude = Double(dictUser["user_longitude"] as! String)
        let coord: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude!, longitude!)
        let span: MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(coord, span)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coord
        mapView.region = region
        //mapView.addAnnotation(annotation)
        
        let coordinateSource = CLLocation(latitude: latitude!, longitude: longitude!)
        var currentLat:Double = 0.0
        var currentLong:Double = 0.0
        if let Lat:Double = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? Double {
            currentLat = Lat
            currentLong = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! Double
        }
        let coordinateDestination = CLLocation(latitude: currentLat, longitude: currentLong)
        self.addRadiusCircle(location: coordinateDestination)
        let distanceInMeters = coordinateSource.distance(from: coordinateDestination)
        lblKm.text = "\(Float((distanceInMeters/1000)*0.621371192)) mi away"
        DispatchQueue.main.async {
            self.viewProductPrice.layer.cornerRadius = self.viewProductPrice.frame.size.height / 2
        }
    }
    
    func addRadiusCircle(location: CLLocation){
        self.mapView.delegate = self
        let circle = MKCircle(center: location.coordinate, radius: 1000)
        self.mapView.add(circle)
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.red
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.5)
            circle.lineWidth = 1
            return circle
        } else {
            return MKPolylineRenderer()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Button Action Event
    @IBAction func tappedOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnAskToFriend(_ sender: UIButton) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if let userEmail = userDetail["email"] as? String {
            FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
                if let err:Error = error {
                    print(err.localizedDescription)
                    return
                }
                //let strUID = FIRAuth.auth()?.currentUser?.uid
                let dictUser = self.dictProduct["userprofile"] as! [String:Any]
                print("Email:\(userEmail)")
                let strProdEmail = dictUser["user_email"] as! String
                print("MD5: \(self.chatId([userEmail,strProdEmail]))")
                let groupID = self.chatId([userEmail,strProdEmail])
                
                //let strRecentID1 = self.chatId([groupID,userEmail])
                
                let object = FObject(path: FRECENT_PATH)
                object[FRECENT_OBJECTID] = groupID
                object[FRECENT_USERID] = dictUser["user_id"] as! String
                object[FRECENT_CURRENTUSERID] = userDetail["user_id"] as! String
                
                object[FRECENT_NAME] = dictUser["user_name"] as! String
                object[FRECENT_CURRENTNAME] = userDetail["name"] as! String
                
                object[FRECENT_EMAIL] = dictUser["user_email"] as! String
                object[FRECENT_CURRENTEMAIL] = userDetail["email"] as! String
                
                object[FRECENT_PICTURE] = dictUser["profile_picture"] as! String
                object[FRECENT_CURRENTPICTURE] = userDetail["profile_picture"] as! String
                object[FRECENT_MSGID] = groupID
                object.saveInBackground()
                
                let chatView = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatView.hidesBottomBarWhenPushed = true
                chatView.messageObjectID = groupID
                chatView.senderDisplayName = dictUser["user_name"] as! String
                chatView.userID = dictUser["user_id"] as! String
                chatView.isSendCustomMessage = false
        
                //chatView.receiverFCMToken = dictUser["device_id"] as! String
                //chatView.deviceType = dictUser["device_type"] as! String
                //chatView.customMessage = "I am interested in buying your product."
                self.navigationController?.pushViewController(chatView, animated: true)
            })
        }
    }
    
    @IBAction func tappedOnMakeOffer(_ sender: UIButton) {
        let makeOffer = self.storyboard?.instantiateViewController(withIdentifier: "MakeOfferViewController") as! MakeOfferViewController
        makeOffer.dictProd = self.dictProduct
        self.navigationController?.pushViewController(makeOffer, animated: true)
    }
    
    @IBAction func tappedOnBuy(_ sender: UIButton) {
        let addressView = self.storyboard?.instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
        addressView.productDetail = self.dictProduct
        self.navigationController?.pushViewController(addressView, animated: true)
    }
    
    @IBAction func tappedOnPromoteProd(_ sender: UIButton) {
        
        let dictDetail = dictProduct
        lblPromotePrice.text = "$ \(getMyUserDefaults(key: MyUserDefaults.ProductPromoteAmount) as! String)"
        let strURL = dictDetail["product_cover_image"] as! String
        imgPromote.sd_setImage(with: URL(string: strURL), completed: { (image, error, nil, url) in
            
        })
        self.viewPromoteContainer.tag = sender.tag
        self.viewPromoteContainer.isHidden = false
        self.viewPromoteContainer.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.viewPromoteContainer.transform = .identity
        }, completion: {(finished: Bool) -> Void in
            // do something once the animation finishes, put it here
        })
    }
    
    @IBAction func tappedOnCancel(_ sender: UIButton) {
        self.viewPromoteContainer.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.viewPromoteContainer.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            self.viewPromoteContainer.isHidden = true
        })
    }
    @IBAction func tappedOnPromote(_ sender: UIButton) {
        self.viewPromoteContainer.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.viewPromoteContainer.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            self.viewPromoteContainer.isHidden = true
            let dictDetail = self.dictProduct
            let paymentView = storyBoards.Main.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            paymentView.productDetail = dictDetail
            self.navigationController?.pushViewController(paymentView, animated: true)
        })
    }
    @IBAction func tappedOnShare(_ sender: UIButton) {
        
    }
    
    func chatId(_ members: [String]) -> String {
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        let sorted = members.sorted { $0.compare($1 , options: .numeric) == .orderedAscending }
        let strMDSHASH = Checksum.md5Hash(of: sorted.joined(separator: ""))
        return strMDSHASH!
        //return Checksum.md5Hash(sorted.joined(separator: ""))
    }

    
    //MARK: - UICollectionViewDelegate & DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if dictProduct["product_video"] as! String == "" {
            return 1 + self.arrProductGallery.count
        }else{
            return 2 + self.arrProductGallery.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        if indexPath.row == 0 {
            cell.imgProduct.sd_setImage(with: URL(string: dictProduct["product_cover_image"] as! String)) { (image, error, nil, url) in
                
            }
        }else{
            if (self.arrProductGallery.count+1) > indexPath.row {
                cell.imgProduct.sd_setImage(with: URL(string: arrProductGallery[indexPath.row-1])) { (image, error, nil, url) in
                    
                }
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
                let url = URL(string: dictProduct["product_video"] as! String)
                cell.player = AVPlayer(url: url!)
                cell.avpController = AVPlayerViewController()
                cell.avpController.player = cell.player
                let height = (ScreenSize.SCREEN_WIDTH * 230) / 375
                cell.avpController.view.frame = CGRect(x: 0.0, y: 0.0, width: ScreenSize.SCREEN_WIDTH, height: height)
                
                cell.addSubview(cell.avpController.view)
                
                return cell
            }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = (ScreenSize.SCREEN_WIDTH * 230) / 375
        return CGSize(width: ScreenSize.SCREEN_WIDTH, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cellVideo = cell as? VideoCell {
            cellVideo.player.pause()
            //let indexPath = self.collectionGallery.indexPath(for: cellVideo);
            //print(indexPath)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in self.collectionGallery.visibleCells {
            if cell is VideoCell {
                //let indexPath = self.collectionGallery.indexPath(for: cellVideo);
                //cellVideo.player.play()
                self.viewProductPrice.isHidden = true
            }else{
                self.viewProductPrice.isHidden = false
            }
            
            
        }
    }
    
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(0, 1) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}




