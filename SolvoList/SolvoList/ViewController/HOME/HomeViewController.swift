//
//  HomeViewController.swift
//  SolvoList
//
//  Created by ronak patel on 15/09/17.
//  Copyright © 2017 depixed.com. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import CoreLocation

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    var image: UIImage? {
        didSet {
            if let image = image {
                imageView.image = image
            } else {
                imageView.backgroundColor = .lightGray
            }
        }
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? PinterestLayoutAttributes {
            //change image view height by changing its constraint
            imageViewHeightLayoutConstraint.constant = attributes.imageHeight
        }
    }
}

class ProductCategoryCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var btnClose: UIButton!
}




class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate,FilterProtocol {
    
    var searchBar =  UISearchBar(frame: CGRect(x:0, y:0, width:200, height:20))
    
    @IBOutlet var prodCollectionView:UICollectionView!
        
    private var _items:[PinterestItem] = []
    
    var arrProductList = [Any]()
    var arrProductSearchList = [Any]()
    var arrProductCateList = [Any]()
    @IBOutlet var collectionCategory:UICollectionView!
    
    @IBOutlet var controlSell: UIControl!
    @IBOutlet var controlHire: UIControl!
    
    var isOpenFilter:Bool = false
    
    override func awakeFromNib() {
        self.tabBarItem.title = "Home"
        //self.title = "Home"
        self.tabBarItem.image = UIImage(named: "imgHome")
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //zappay
        //unocoin
        searchBar.delegate = self
        searchBar.placeholder = "SolvoList"
        self.navigationItem.titleView = searchBar
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        //setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
        
        //-------- NotificationCenter Register ------
        NotificationCenter.default.addObserver(self, selector: #selector(productCategoryList), name: .refreshProductCategory, object: nil)
        
        //------Navigation Button------
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgAccount"), style: .plain, target: self, action: #selector(tappedOnAccount(_:)))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgFilter"), style: .plain, target: self, action: #selector(tappedOnFilter(_:)))
        
        
        setupCollectionViewInsets()
        setupLayout()
        //let storyboard = UIStoryboard(name:"Sell", bundle:nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false;
        
        self.navigationController?.navigationBar.barTintColor = Color.COLOR_NAVIGATION_BACKGROUND
        
        DispatchQueue.main.async {
            self.controlHire.layer.cornerRadius = self.controlHire.frame.size.height / 2
            self.controlSell.layer.cornerRadius = self.controlSell.frame.size.height / 2
        }
        
        
        if !(UserDefaults.standard.bool(forKey: "isLogin")) {
            let welcomeView = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
            let navController1 = NavigationController(rootViewController: welcomeView)
            navController1.isNavigationBarHidden = true
            present(navController1, animated: true, completion: nil)
        }else{
            if isOpenFilter {
                //isOpenFilter = false
            }else{
                getProductList()
            }
        }
        
        //print(chatId([".@aabcghijklmmmortu", "fe0452b0eac5472237c28bed7248c3b5"]))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom Methods
    
    func filterData(dictData: [String : Any]) {
        print("Filterdata:\(dictData)")
        getProductListWithFilter(dictData: dictData)
    }
    
    func resetData() {
        print("reset data")
        isOpenFilter = false
        getProductList()
    }
    
    func productCategoryList() {
        self.arrProductCateList = appDelegate.arrCategory
        self.collectionCategory.reloadData()
    }
    
    func getProductList() {
        
        if getMyUserDefaults(key: MyUserDefaults.currentCityName) as! String == "" {
            let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.getProductList()
            }
            
            return;
        }
        
        var strLat = ""
        var strLong = ""
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            strLat = "\(latitude)"
            strLong = "\(longitude)"
        }
        
        let parameters = ["i_key":WebURL.appKey,
                          "address":appDelegate.strCityName,
                          "lat":strLat,
                          "long":strLong] as [String : Any]
        //appDelegate.showHud()
        request(WebURL.productList, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("Product List:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        if let arrTemp = dictResult["o_data"] as? [Any] {
                            self.arrProductList = arrTemp
                        }
                        self.arrProductSearchList = self.arrProductList
                        self.prodCollectionView.reloadData()
                    }else{
                        
                    }
                    
                }
            }else{
                showAlert(msg: "This application requires internet connection", completion: nil)
            }
        })
    }
    
    
    func getProductListWithFilter(dictData:[String : Any]) {
        
        
        var parameters = [String : Any]()
        parameters["i_key"] = WebURL.appKey
        parameters["latitude"] = dictData["latitude"]
        parameters["longitude"] = dictData["longitude"]
        parameters["longitude"] = dictData["longitude"]
        if dictData["posted_duration"] as! String != "all" {
            parameters["posted_duration"] = dictData["posted_duration"]
        }
        parameters["price"] = dictData["price"]
        parameters["distance"] = dictData["distance"]
        if dictData["sort_by"] as! String != "" {
            parameters["sort_by"] = dictData["sort_by"]
        }
        
        /*let parameters = ["i_key":WebURL.appKey,
                          "address":appDelegate.strCityName,
                          "latitude":strLat,
                          "longitude":strLong,
                          "distance":"",
                          "category_id":""] as [String : Any]*/
        //appDelegate.showHud()'
        let strCate = dictData["category_id"] as! String
        if strCate.length > 0 {
            parameters["category_id"] = dictData["category_id"]
            self.arrProductCateList = [Any]()
        }
        for dict in appDelegate.arrCategory {
            let dictTemp = dict as! [String:Any]
            let catid = dictTemp["id"] as! String
            if (strCate.range(of: catid) != nil) {
                self.arrProductCateList.append(dictTemp)
                self.collectionCategory.reloadData()
            }
        }
        
        print("Filter Parameter: \(parameters)")
        request(WebURL.filter, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("Product List:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.arrProductList = dictResult["o_data"] as! [Any]
                        self.arrProductSearchList = self.arrProductList
                        self.prodCollectionView.reloadData()
                    }else{
                        self.arrProductList = [Any]()
                        self.arrProductSearchList = self.arrProductList
                        self.prodCollectionView.reloadData()
                        appDelegate.showAlertMessage(strMessage: dictResult["o_message"] as! String)
                    }
                    
                }
            }else{
                showAlert(msg: "This application requires internet connection", completion: nil)
            }
        })
    }
    
    
    //MARK: - UIScrollViewDelegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    
    //MARK: - Button Tapped Event
    
    @IBAction func tappedOnAccount(_ sender:UIBarButtonItem) {
        
        let AccountView = self.storyboard?.instantiateViewController(withIdentifier: "AccountViewController")
        let navController1 = NavigationController(rootViewController: AccountView!)
        //navController1.isNavigationBarHidden = true
        present(navController1, animated: true, completion: nil)
    }
    
    @IBAction func tappedOnFilter(_ sender:UIBarButtonItem) {
        let filterView = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        let navController1 = NavigationController(rootViewController: filterView)
        //navController1.isNavigationBarHidden = true
        filterView.delegate = self
        isOpenFilter = true
        present(navController1, animated: true, completion: nil)
    }
    
    @IBAction func tappedOnSell(_ sender:UIControl) {
        let sellStep1 = storyBoards.Sell.instantiateViewController(withIdentifier: "SellProductStep1") as! SellProductStep1
        let navController1 = NavigationController(rootViewController: sellStep1)
        present(navController1, animated: true, completion: nil)
        //sellStep1.hidesBottomBarWhenPushed = true;
        //self.navigationController?.pushViewController(sellStep1, animated: true)
    }
    
    @IBAction func tappedOnHire(_ sender:UIControl) {
        let hireStep1 = storyBoards.Hire.instantiateViewController(withIdentifier: "HireStep1") as! HireStep1
        let navController1 = NavigationController(rootViewController: hireStep1)
        present(navController1, animated: true, completion: nil)
        //sellStep1.hidesBottomBarWhenPushed = true;
        //self.navigationController?.pushViewController(sellStep1, animated: true)
    }
    
    @IBAction func tappedOnClose(_ sender:UIButton) {
        self.arrProductCateList.remove(at: sender.tag)
        self.collectionCategory.reloadData()
        var arrCateID = [Any]()
        for dict in arrProductCateList {
            let dictProdCate = dict as! [String:Any]
            arrCateID.append(dictProdCate["id"] ?? 0)
        }
        let filterCate = NSPredicate(format:"cat_id IN %@", arrCateID)
        let tempArr = self.arrProductSearchList as Array
        self.arrProductSearchList = tempArr.filter(filterCate.evaluate(with:))
        self.prodCollectionView.reloadData()
    }
    
    //MARK: - Setup CollectionView Layout
    
    private func setupCollectionViewInsets() {
        prodCollectionView!.backgroundColor = .clear
        prodCollectionView!.contentInset = UIEdgeInsets(
            top: 15,
            left: 5,
            bottom: 5,
            right: 5
        )
    }
    
    private func setupLayout() {
        let layout: PinterestLayout = {
            if let layout = prodCollectionView.collectionViewLayout as? PinterestLayout {
                return layout
            }
            let layout = PinterestLayout()
            
            prodCollectionView?.collectionViewLayout = layout
            
            return layout
        }()
        layout.delegate = self
        layout.cellPadding = 5
        layout.numberOfColumns = 2
    }
    
    
    
    //MARK: - UICollectionViewDataSource
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionCategory {
            return self.arrProductCateList.count
        }else{
            return self.arrProductSearchList.count
        }
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionCategory {
            let dictProdCate = self.arrProductCateList[indexPath.row] as! [String:Any]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCategoryCell",for: indexPath) as! ProductCategoryCell
            cell.imageView.sd_setImage(with: URL(string: dictProdCate["cat_image"] as! String), completed: nil)
            cell.btnClose.tag = indexPath.row
            cell.btnClose.addTarget(self, action: #selector(tappedOnClose(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell",for: indexPath) as! CollectionViewCell
            
            let dictProduct = self.arrProductSearchList[indexPath.row] as! [String:Any]
            cell.imageView.sd_setImage(with: URL(string: dictProduct["product_cover_image"] as! String), completed: { (image, error, nil, url) in
                if image == nil {
                    cell.imageView.image = #imageLiteral(resourceName: "solvolist")
                }
            })
            cell.descriptionLabel.text = "$\(dictProduct["product_price"] as! String)"
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == prodCollectionView {
            let productDetail = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
            productDetail.hidesBottomBarWhenPushed = true
            let dictProduct = self.arrProductSearchList[indexPath.row] as! [String:Any]
            productDetail.dictProduct = dictProduct;
            self.navigationController?.pushViewController(productDetail, animated: true)
        }
    }
    
    
    //MARK: - UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //----------------------------------------------------------------------
        searchBar.frame = CGRect(x:0, y:0, width:ScreenSize.SCREEN_WIDTH - 100, height:20)
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //-----------------------------------------------------------------------
        searchBar.frame = CGRect(x:0, y:0, width:200, height:20)
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
            arrProductSearchList = arrProductList;
            self.prodCollectionView.reloadData()
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("search text: \(searchText)")
        let namePredicate = NSPredicate(format: "product_name CONTAINS[C] '\(searchText)'")
        arrProductSearchList = self.arrProductList.filter { namePredicate.evaluate(with: $0) }
        if searchText == "" {
            arrProductSearchList = arrProductList;
        }
        self.prodCollectionView.reloadData()
        print("search result: \(arrProductSearchList)")
    }
    
    public func height(forWidth width: CGFloat, Size size:CGSize) -> CGFloat {
        let boundingRect = CGRect(
            x: 0,
            y: 0,
            width: ScreenSize.SCREEN_WIDTH / 2.0 ,
            height: CGFloat(MAXFLOAT)
        )
        let rect = AVMakeRect(aspectRatio: size, insideRect: boundingRect)
        return rect.size.height
    }
}


extension HomeViewController: PinterestLayoutDelegate {
    
    func collectionView(collectionView: UICollectionView,
                        heightForImageAtIndexPath indexPath: IndexPath,
                        withWidth: CGFloat) -> CGFloat {
        
        let dictProduct = self.arrProductSearchList[indexPath.row] as! [String:Any]
        if let width = dictProduct["product_cover_image_width"] as? CGFloat {
            let size = CGSize(width: width, height: dictProduct["product_cover_image_height"] as! CGFloat)
            return self.height(forWidth: dictProduct["product_cover_image_width"] as! CGFloat, Size: size)
        }else {
            return 200
        }
        
    }
    
    func collectionView(collectionView: UICollectionView,
                        heightForAnnotationAtIndexPath indexPath: IndexPath,
                        withWidth: CGFloat) -> CGFloat {
        return 0
    }
}


