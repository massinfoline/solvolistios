//
//  FilterViewController.swift
//  SolvoList
//
//  Created by ronak patel on 15/11/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import CoreLocation

@objc protocol FilterProtocol {
    
    @objc optional func filterData(dictData:[String:Any])
    @objc optional func resetData()
    
}

class FilterViewController: UIViewController, kDropDownListViewDelegate {
    
    var delegate:FilterProtocol!


    @IBOutlet var controlLocation: UIControl!
    @IBOutlet var lblLocation: UILabel!
    
    @IBOutlet var controlCategory: UIControl!
    @IBOutlet var lblCategory: UILabel!
    
    @IBOutlet var control24Hrs: UIControl!
    @IBOutlet var control7days: UIControl!
    @IBOutlet var control30days: UIControl!
    @IBOutlet var controlAllProduct: UIControl!
    
    @IBOutlet var img24Hrs: UIImageView!
    @IBOutlet var img7days: UIImageView!
    @IBOutlet var img30days: UIImageView!
    @IBOutlet var imgAllProd: UIImageView!
    
    @IBOutlet var txtStart: UITextField!
    @IBOutlet var txtEnd: UITextField!
    @IBOutlet var sliderPrice: ASValueTrackingSlider!
    @IBOutlet var lblPrice: UILabel!
    
    @IBOutlet var controlSortBy: UIControl!
    @IBOutlet var lblSortBy: UILabel!
    
    @IBOutlet var btnReset: UIButton!
    @IBOutlet var btnApply: UIButton!
    
    @IBOutlet var scrollView: UIScrollView!
    
    let chooseCategory = DropDown()
    var arrCategory = [Any]()
    
    var Dropobj:DropDownListView!
    @IBOutlet var DropobjBG: UIControl!
    
    var sortingFlag = ""
    var categoryIDs = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        controlAllProduct.isSelected = true
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgClose"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Filter"
        
        for dict in appDelegate.arrCategory {
            let dictCate = dict as! [String:Any]
            arrCategory.append(dictCate["cat_name"] ?? "")
        }
        
        //requiredSkillSliderBar
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        self.sliderPrice.popUpViewAnimatedColors = [Color.COLOR_GREEN]
        self.sliderPrice.minimumValue = 1.0
        self.sliderPrice.maximumValue = 10000.0
        //self.sliderPrice.numberFormatter = formatter
        self.sliderPrice.popUpViewArrowLength = 10.0
        setupChooseSortby()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DropobjBG.isHidden = true
        registerNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.setupChooseCategoryDropDown()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedOnApply(_ sender: UIButton) {
        
        var strLat = ""
        var strLong = ""
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            strLat = "\(latitude)"
            strLong = "\(longitude)"
        }
        var start = Int(txtStart.text!)
        var end = Int(txtEnd.text!)
        if start == nil {
            start = 0
        }
        if end == nil {
            end = 1
        }
        
        if end! < 1 {
            end = 1
        }
        if start! > end! {
            appDelegate.showAlertMessage(strMessage: "Please enter valid distace")
            return
        }
        
        var strDuration = ""
        if imgAllProd.image != nil {
            strDuration = "all"
        }else if img30days.image != nil {
            strDuration = "30"
        }else if img7days.image != nil {
            strDuration = "7"
        }else if img24Hrs.image != nil {
            strDuration = "24"
        }
        let maxPrice:String = (lblPrice.text?.replacingOccurrences(of: "$", with: ""))!
        self.dismiss(animated: true) { 
            self.delegate.filterData!(dictData: ["latitude":strLat,"longitude":strLong,"distance":"\(String(describing: end!))","posted_duration":strDuration,"price":maxPrice ,"category_id":self.categoryIDs,"sort_by":self.sortingFlag])
        }
        
        
    }
    
    @IBAction func tappedOnReset(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            self.delegate.resetData!()
        }
    }
    
    @IBAction func sliderValueChanged(_ sender: ASValueTrackingSlider) {
        //print("value: \(sender.value)")
        lblPrice.text = "$\(sender.value)"
    }
    
    @IBAction func tappedOnLocation(_ sender:UIControl) {
        
        
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            //let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let location = CLLocation(latitude: latitude, longitude: longitude)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    
                    print(pm.postalCode ?? "") //prints zip code
                    print(pm.locality ?? "") //prints zip code
                    print(pm.administrativeArea ?? "") //prints zip code
                    print(pm.country ?? "") //prints zip code
                    //self.txtZipCode.text = pm.postalCode
                    let locality = (pm.locality! != "") ? pm.locality! : ""
                    let administrativeArea = (pm.administrativeArea! != "") ? ", \(pm.administrativeArea!)" : ""
                    let country = (pm.country! != "") ? ", \(pm.country!)" : ""
                    let strAddress =  locality + administrativeArea + country
                    self.lblLocation.text = strAddress
                }
                else {
                    print("Problem with the data received from geocoder")
                }
            })
        }
        
        
    }
    
    @IBAction func tappedOnCategory(_ sender: UIControl) {
        //self.customizeDropDown()
        //chooseCategory.show()
        DropobjBG.isHidden = false
        Dropobj.show(in: self.view, animated: true)
        Dropobj.setBackGroundDropDown_R(242, g: 242, b: 242, alpha: 1.0)
        
    }
    
    @IBAction func tappedOnCategoryBG(_ sender: UIControl) {
        //self.customizeDropDown()
        //chooseCategory.show()
        DropobjBG.isHidden = true
        Dropobj.fadeOut()
        
    }
    
    @IBAction func tappedOnSortby(_ sender: UIControl) {
        //self.customizeDropDown()
        chooseCategory.show()
    }
    
    @IBAction func tappedOnWithIn(_ sender: UIControl) {
        if sender.tag == 111 {
            if sender.isSelected {
                sender.isSelected = false
                img24Hrs.image = nil
            }else{
                sender.isSelected = true
                img24Hrs.image = #imageLiteral(resourceName: "checked-status")
            }
        }else if sender.tag == 222 {
            if sender.isSelected {
                sender.isSelected = false
                img7days.image = nil
            }else{
                sender.isSelected = true
                img7days.image = #imageLiteral(resourceName: "checked-status")
            }
        }else if sender.tag == 333 {
            if sender.isSelected {
                sender.isSelected = false
                img30days.image = nil
            }else{
                sender.isSelected = true
                img30days.image = #imageLiteral(resourceName: "checked-status")
            }
        }else if sender.tag == 444 {
            if sender.isSelected {
                sender.isSelected = false
                imgAllProd.image = nil
            }else{
                sender.isSelected = true
                imgAllProd.image = #imageLiteral(resourceName: "checked-status")
            }
        }
        
    }
    
    // MARK: - Keyboard Navigation
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        //let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        //previousButton.customView = self.prevNextSegment()
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    
    
    func setupChooseCategoryDropDown() {
        var arrCategoryID = [String]()
        for dict in appDelegate.arrCategory {
            let dictCate = dict as! [String:Any]
            arrCategoryID.append(dictCate["id"] as? String ?? "")
        }
        
        Dropobj = DropDownListView(title: "Select Category", options: appDelegate.arrCategory, optionsID: arrCategoryID, xy: CGPoint(x: 0, y: controlCategory.frame.origin.y), size: CGSize(width: ScreenSize.SCREEN_WIDTH, height: 300), isMultiple: true)
        Dropobj.delegate = self
        
        /*chooseCategory.anchorView = controlCategory
        chooseCategory.dismissMode = .onTap
        chooseCategory.direction = .bottom
        chooseCategory.bottomOffset = CGPoint(x: 0, y: controlCategory.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseCategory.dataSource = arrCategory as! [String]
        
        // Action triggered on selection
        chooseCategory.selectionAction = { [unowned self] (index, item) in
            //self.txtCategory.text = item
            let dictCate = appDelegate.arrCategory[index] as! [String:Any]
            //self.strProdCate = dictCate["id"] as! String
        }*/
    }
    
    
    func setupChooseSortby() {
        chooseCategory.anchorView = controlSortBy
        chooseCategory.dismissMode = .onTap
        chooseCategory.direction = .top
        chooseCategory.bottomOffset = CGPoint(x: 0, y: controlSortBy.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseCategory.dataSource = ["Ascending","Descending"] 
        
        // Action triggered on selection
        chooseCategory.selectionAction = { [unowned self] (index, item) in
            //self.txtCategory.text = item
            //let dictCate = appDelegate.arrCategory[index] as! [String:Any]
            //self.strProdCate = dictCate["id"] as! String
            self.lblSortBy.text = item
            if item == "Ascending" {
                self.sortingFlag = "ASC"
            }else if item == "Descending"{
                self.sortingFlag = "DESC"
            }
        }
    }
    
    func customizeDropDown() {
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        
        /*** FOR CUSTOM CELLS ***/
        chooseCategory.cellNib = UINib(nibName: "CategoryCell", bundle: nil)
        
        chooseCategory.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CategoryCell else { return }
            let dictCate = appDelegate.arrCategory[index] as! [String:Any]
            cell.imgCategory.sd_setImage(with: URL(string: dictCate["cat_image"] as! String), completed: nil)
            // Setup your custom UI components
            //cell.lblName.text = "Suffix \(index)"
        }
        /*** ---------------- ***/
    }
    
    //MARK: - kDropDownListViewDelegate
    func dropDownListViewDidCancel() {
         DropobjBG.isHidden = true
    }
    
    func dropDownListView(_ dropdownListView: DropDownListView!, datalist ArryData: NSMutableArray!) {
         DropobjBG.isHidden = true
        var strName = ""
        for category in ArryData {
            let dictCate = category as! [String:Any]
            let cateID = dictCate["id"] as! String
            let cateName = dictCate["cat_name"] as! String
            print("category id : \(cateID)")
            if (categoryIDs.range(of: cateID) == nil){
                categoryIDs = "\(categoryIDs),\(cateID)"
            }
            
            if (strName.range(of: cateID) == nil){
                strName = "\(strName),\(cateName)"
            }
        }
        
        if self.categoryIDs.length > 1 {
            self.categoryIDs.remove(at: self.categoryIDs.startIndex)
            strName.remove(at: strName.startIndex)
        }
        
        self.lblCategory.text = strName
    }
    
    func dropDownListView(_ dropdownListView: DropDownListView!, didSelectedIndex anIndex: Int) {
        print("anIndex : \(anIndex)")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
