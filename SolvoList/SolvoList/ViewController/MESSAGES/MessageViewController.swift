//
//  MessageViewController.swift
//  SolvoList
//
//  Created by ronak patel on 15/09/17.
//  Copyright © 2017 depixed.com. All rights reserved.
//

import UIKit
import Firebase

class MessageCell: UITableViewCell {
    @IBOutlet var imgViewUser:UIImageView!
    @IBOutlet var lblUserName:UILabel!
    @IBOutlet var lblProdName:UILabel!
    @IBOutlet var lblTime:UILabel!
}

class MessageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblMessage:UITableView!
    
    private var newMessageRefHandle: FIRDatabaseHandle?
    var messageRef: FIRDatabaseReference!
    
    var arrRecentMessage = [Any]()
    override func awakeFromNib() {
        self.tabBarItem.title = "Messages"
        self.title = "Messages"
        self.tabBarItem.image = UIImage(named: "imgMessage")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeRecents()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setMyUserDefaults(value: "0", key: MyUserDefaults.NotificationCount)
        appDelegate.setBadgeCount()
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    private func observeRecents() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        //appDelegate.showHud()
        messageRef = FIRDatabase.database().reference().child(FRECENT_PATH)
        
        
        //let messageQuery = messageRef.queryOrdered(byChild: FRECENT_USERID)//.queryEqual(toValue: userDetail["user_id"])
        newMessageRefHandle = messageRef.observe(.childAdded, with: { (snapshot) in
            let messageData = snapshot.value as! [String:Any]
            //appDelegate.hideHud()
            print(messageData)
            let strLoginID = userDetail["user_id"] as! String
            let userID = messageData["userId"] as! String
            let currentUserID = messageData["currentUserId"] as! String
            
            if currentUserID == strLoginID {
                var dictTemp = messageData
                dictTemp["name"] = messageData["name"]
                self.arrRecentMessage.append(dictTemp)
            }
            
            if userID == strLoginID {
                var dictTemp = messageData
                dictTemp["name"] = messageData["currentName"]
                self.arrRecentMessage.append(dictTemp)
            }
            
            
            
            self.tblMessage.reloadData()
        })
        
        /*newMessageRefHandle = messageRef.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData = snapshot.value as! [String:Any]
            print(messageData)
            self.arrRecentMessage.append(messageData)
            self.tblMessage.reloadData()
        })*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrRecentMessage.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
        let dictRecent = self.arrRecentMessage[indexPath.section] as! [String:Any]
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        if dictRecent["userId"] as! String == userDetail["user_id"] as! String {
            cell.lblUserName.text = dictRecent["currentName"] as? String
            cell.imgViewUser.sd_setImage(with: URL(string: dictRecent["currentPhoto"] as! String)) { (image, error, nil, url) in
                if image == nil {
                    cell.imgViewUser.image = #imageLiteral(resourceName: "imgUser")
                }
            }
        }
        if dictRecent["currentUserId"] as! String == userDetail["user_id"] as! String {
            cell.lblUserName.text = dictRecent["name"] as? String
            cell.imgViewUser.sd_setImage(with: URL(string: dictRecent["photo"] as! String)) { (image, error, nil, url) in
                if image == nil {
                    cell.imgViewUser.image = #imageLiteral(resourceName: "imgUser")
                }
            }
        }
        
        //cell.imgViewUser.sd_setImage(with: URL(string: dictRecent["photo"] as! String), completed:)
        cell.lblProdName.text = ""
        cell.lblTime.text = TimeElapsed(dictRecent[FRECENT_UPDATEDAT] as! Int64)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
            if let err:Error = error {
                print(err.localizedDescription)
                return
            }
            let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
            let chatView = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            chatView.hidesBottomBarWhenPushed = true
            let dictRecent = self.arrRecentMessage[indexPath.section] as! [String:Any]
            chatView.messageObjectID = dictRecent[FRECENT_MSGID] as! String
            if dictRecent["userId"] as! String == userDetail["user_id"] as! String {
                chatView.senderDisplayName = dictRecent["name"] as! String
                chatView.userID = dictRecent["userId"] as! String
                chatView.userEmail = dictRecent["currentEmail"] as! String
            }
            if dictRecent["currentUserId"] as! String == userDetail["user_id"] as! String {
                chatView.senderDisplayName = dictRecent["currentName"] as! String
                chatView.userID = dictRecent["userId"] as! String
                chatView.userEmail = dictRecent["currentEmail"] as! String
            }
            self.navigationController?.pushViewController(chatView, animated: true)
        })
    }
}
