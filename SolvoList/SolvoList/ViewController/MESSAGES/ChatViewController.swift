/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import Photos
import Firebase
import JSQMessagesViewController

final class ChatViewController: JSQMessagesViewController {
    
    // MARK: Properties
    private let imageURLNotSetKey = "NOTSET"
    
    var channelRef: FIRDatabaseReference?
    
    var messageObjectID:String = ""
    var userID:String = ""
    var userEmail:String = ""
    var receiverFCMToken = ""
    var deviceType = ""
    var isSendCustomMessage:Bool = false
    var customMessage:String = ""
    
    var messageRef: FIRDatabaseReference!
    //fileprivate lazy var storageRef: FIRStorageReference = FIRStorage.storage().reference(forURL: "gs://afconnect-ed8b5.appspot.com/")
    private lazy var userIsTypingRef: FIRDatabaseReference = self.channelRef!.child("typingIndicator").child(self.senderId)
    private lazy var usersTypingQuery: FIRDatabaseQuery = self.channelRef!.child("typingIndicator").queryOrderedByValue().queryEqual(toValue: true)
    
    private var newMessageRefHandle: FIRDatabaseHandle?
    private var updatedMessageRefHandle: FIRDatabaseHandle?
    
    private var messages: [JSQMessage] = []
    private var photoMessageMap = [String: JSQPhotoMediaItem]()
    
    private var localTyping = false
    
    var isOpenPage = false
    
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
            userIsTypingRef.setValue(newValue)
        }
    }
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.senderId = FIRAuth.auth()?.currentUser?.uid
        self.title  = self.senderDisplayName
        channelRef = FIRDatabase.database().reference()
        observeMessages()
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        // No avatars
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isOpenPage = true
        if isSendCustomMessage {
            isSendCustomMessage = false
            let object = FObject(path: FMESSAGE_PATH, subpath:messageObjectID)
            let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
            
            object[MESSAGE_TEXT] = customMessage
            object[FMESSAGE_SENDERID] = self.senderId
            object[FMESSAGE_SENDERNAME] = senderDisplayName
            object[FMESSAGE_DEVICETYPE] = "ios"
            object[FMESSAGE_DEVICETOKEN] = getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken)
            object[FMESSAGE_CURRENTDEVICETYPE] = self.deviceType
            object[FMESSAGE_CURRENTDEVICETOKEN] = receiverFCMToken
            object[FMESSAGE_CURRENTUSERID] = userDetail["user_id"] ?? ""
            object[FMESSAGE_USERID] = userID
            object.saveInBackground()
            let dictBody:[String:String] = ["email":userEmail,
                                            "text":customMessage,
                                            "username":userDetail["name"] as! String,
                                            "photo":"",
                                            "fcm_token": getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String,
                                            "currentEmail":userDetail["email"] as! String,
                                            "current_userid":userDetail["user_id"] as! String,
                                            "sound":"default"]
            print(dictBody)
            appDelegate.sendPushMessage(strToken: receiverFCMToken, deviceType: self.deviceType, dictBody: dictBody)
            
            // 4
            JSQSystemSoundPlayer.jsq_playMessageSentSound()
            
            // 5
            finishSendingMessage()
            isTyping = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        observeTyping()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isOpenPage = false
        setMyUserDefaults(value: "", key: MyUserDefaults.OpentChatID)
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        if let refHandle = updatedMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Collection view data source (and related) methods
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item] // 1
        if message.senderId == senderId { // 2
            return outgoingBubbleImageView
        } else { // 3
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        
        if message.senderId == senderId { // 1
            cell.textView?.textColor = UIColor.white // 2
        } else {
            cell.textView?.textColor = UIColor.black // 3
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString? {
        let message = messages[indexPath.item]
        switch message.senderId {
        case senderId:
            return nil
        default:
            guard let senderDisplayName = message.senderDisplayName else {
                assertionFailure()
                return nil
            }
            return NSAttributedString(string: senderDisplayName)
        }
    }
    
    // MARK: - Firebase related methods
    
    private func observeMessages() {
        messageRef = channelRef!.child(FMESSAGE_PATH).child(messageObjectID)
        let messageQuery = messageRef.queryLimited(toLast:25)
        
        // We can use the observe method to listen for new
        // messages being written to the Firebase DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData = snapshot.value as! [String:Any]
            //print(messageData)
            if let id = messageData["senderId"] as! String!, let name = messageData["senderName"] as! String!, let text = messageData["text"] as! String!, text.characters.count > 0 {
                self.addMessage(withId: id, name: name, text: text)
                if id != self.senderId {
                    self.receiverFCMToken = messageData["deviceToken"] as! String
                    self.deviceType = messageData["deviceType"] as! String
                    if self.isOpenPage {
                        setMyUserDefaults(value: messageData["deviceToken"] as! String, key: MyUserDefaults.OpentChatID)
                    }
                }else{
                    self.receiverFCMToken = messageData[FMESSAGE_CURRENTDEVICETOKEN] as! String
                    self.deviceType = messageData[FMESSAGE_CURRENTDEVICETYPE] as! String
                }
                self.finishReceivingMessage()
            }
            
        })
        
    }
    
    private func observeTyping() {
        let typingIndicatorRef = channelRef!.child("typingIndicator")
        userIsTypingRef = typingIndicatorRef.child(senderId)
        userIsTypingRef.onDisconnectRemoveValue()
        usersTypingQuery = typingIndicatorRef.queryOrderedByValue().queryEqual(toValue: true)
        
        usersTypingQuery.observe(.value) { (data: FIRDataSnapshot) in
            
            // You're the only typing, don't show the indicator
            if data.childrenCount == 1 && self.isTyping {
                return
            }
            
            // Are there others typing?
            self.showTypingIndicator = data.childrenCount > 0
            self.scrollToBottom(animated: true)
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let object = FObject(path: FMESSAGE_PATH, subpath:messageObjectID)
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        object[MESSAGE_TEXT] = text
        object[FMESSAGE_SENDERID] = self.senderId
        object[FMESSAGE_SENDERNAME] = senderDisplayName
        object[FMESSAGE_DEVICETYPE] = "ios"
        object[FMESSAGE_DEVICETOKEN] = getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken)
        object[FMESSAGE_CURRENTDEVICETYPE] = self.deviceType
        object[FMESSAGE_CURRENTDEVICETOKEN] = receiverFCMToken
        object[FMESSAGE_CURRENTUSERID] = userDetail["user_id"] ?? ""
        object[FMESSAGE_USERID] = userID
        object.saveInBackground()
        let dictBody:[String:String] = ["email":userEmail,
                                        "text":text,
                                        "username":userDetail["name"] as! String,
                                        "photo":"",
                                        "fcm_token": getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String,
                                        "currentEmail":userDetail["email"] as! String,
                                        "current_userid":userDetail["user_id"] as! String,
                                        "sound":"default"]
        print(dictBody)
        appDelegate.sendPushMessage(strToken: receiverFCMToken, deviceType: self.deviceType, dictBody: dictBody)
        
        // 4
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        // 5
        finishSendingMessage()
        isTyping = false
    }
    
    override func didPressTemplateButtonMessageText(_ text: String!) {
        
        let trimmedString = text.trimmingCharacters(in: .whitespacesAndNewlines)
        print(trimmedString)
        
        let object = FObject(path: FMESSAGE_PATH, subpath:messageObjectID)
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        object[MESSAGE_TEXT] = trimmedString
        object[FMESSAGE_SENDERID] = self.senderId
        object[FMESSAGE_SENDERNAME] = senderDisplayName
        object[FMESSAGE_DEVICETYPE] = "ios"
        object[FMESSAGE_DEVICETOKEN] = getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken)
        object[FMESSAGE_CURRENTDEVICETYPE] = self.deviceType
        object[FMESSAGE_CURRENTDEVICETOKEN] = receiverFCMToken
        object[FMESSAGE_CURRENTUSERID] = userDetail["user_id"] ?? ""
        object[FMESSAGE_USERID] = userID
        object.saveInBackground()
        let dictBody:[String:String] = ["email":userEmail,
                                        "text":trimmedString,
                                        "username":userDetail["name"] as! String,
                                        "photo":"",
                                        "fcm_token": getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String,
                                        "currentEmail":userDetail["email"] as! String,
                                        "current_userid":userDetail["user_id"] as! String,
                                        "sound":"default"]
        print(dictBody)
        appDelegate.sendPushMessage(strToken: receiverFCMToken, deviceType: self.deviceType, dictBody: dictBody)
        
        // 4
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        // 5
        finishSendingMessage()
        isTyping = false
    }
    
    func sendPhotoMessage() -> String? {
        let itemRef = messageRef.childByAutoId()
        
        let messageItem = [
            "photoURL": imageURLNotSetKey,
            "senderId": senderId!,
            ]
        
        itemRef.setValue(messageItem)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        finishSendingMessage()
        return itemRef.key
    }
    
    func setImageURL(_ url: String, forPhotoMessageWithKey key: String) {
        let itemRef = messageRef.child(key)
        itemRef.updateChildValues(["photoURL": url])
    }
    
    // MARK: - UI and User Interaction
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        /*let picker = UIImagePickerController()
         picker.delegate = self
         if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
         picker.sourceType = UIImagePickerControllerSourceType.camera
         } else {
         picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
         }
         
         present(picker, animated: true, completion:nil)*/
    }
    
    private func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
            messages.append(message)
        }
    }
    
    private func addPhotoMessage(withId id: String, key: String, mediaItem: JSQPhotoMediaItem) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            messages.append(message)
            
            if (mediaItem.image == nil) {
                photoMessageMap[key] = mediaItem
            }
            
            collectionView.reloadData()
        }
    }
    
    // MARK: - UITextViewDelegate methods
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        // If the text is not empty, the user is typing
        isTyping = textView.text != ""
    }
    
}

// MARK: Image Picker Delegate
extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion:nil)
        
       /* // 1
        if let photoReferenceUrl = info[UIImagePickerControllerReferenceURL] as? URL {
            // Handle picking a Photo from the Photo Library
            // 2
            let assets = PHAsset.fetchAssets(withALAssetURLs: [photoReferenceUrl], options: nil)
            let asset = assets.firstObject
            
            // 3
            if let key = sendPhotoMessage() {
                // 4
                asset?.requestContentEditingInput(with: nil, completionHandler: { (contentEditingInput, info) in
                    let imageFileURL = contentEditingInput?.fullSizeImageURL
                    
                    // 5
                    let path = "\(String(describing: FIRAuth.auth()?.currentUser?.uid))/\(Int(Date.timeIntervalSinceReferenceDate * 1000))/\(photoReferenceUrl.lastPathComponent)"
                    
                    // 6
                    self.storageRef.child(path).putFile(imageFileURL!, metadata: nil) { (metadata, error) in
                        if let error = error {
                            print("Error uploading photo: \(error.localizedDescription)")
                            return
                        }
                        // 7
                        self.setImageURL(self.storageRef.child((metadata?.path)!).description, forPhotoMessageWithKey: key)
                    }
                })
            }
        } else {
            // Handle picking a Photo from the Camera - TODO
        }*/
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion:nil)
    }
}
