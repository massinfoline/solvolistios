//
//  PaymentViewController.swift
//  SolvoList
//
//  Created by ronak patel on 22/11/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Stripe
import Alamofire
class PaymentViewController: UIViewController,PayPalPaymentDelegate, STPAddCardViewControllerDelegate {
    
    
    var productDetail = [String:Any]()
    var addressDetail = [String:String]()
    var productName = ""
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    var redirectContext:STPRedirectContext!
    var isJobPromote:Bool = false
    var isSellProd:Bool = false
    var addCardViewController:STPAddCardViewController!
    
    @IBOutlet var lblAmount:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if isJobPromote {
            productName = productDetail["job_title"] as! String
        }else{
            productName = productDetail["product_name"] as! String
        }
        
        payPalConfig.merchantName = "solvolist"
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.example.com/privacy")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.example.com/legal")
        payPalConfig.acceptCreditCards = true
        
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        payPalConfig.payPalShippingAddressOption = .payPal;
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Payment"
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //PayPalMobile.preconnect(withEnvironment: "live")
        PayPalMobile.preconnect(withEnvironment: "sandbox")
        if isJobPromote {
            lblAmount.text = "$ \(getMyUserDefaults(key: MyUserDefaults.JobPromoteAmount) as! String)"
        }else{
            lblAmount.text = "$ \(getMyUserDefaults(key: MyUserDefaults.ProductPromoteAmount) as! String)"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        // Dismiss add card view controller
        dismiss(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        print("Stripe Token: \(token)")
        self.callStripChargeAPI(strToken: token.tokenId)
        
        /*submitTokenToBackend(token, completion: { (error: Error?) in
         if let error = error {
         // Show error in add card view controller
         completion(error)
         }
         else {
         // Notify add card view controller that token creation was handled successfully
         completion(nil)
         
         // Dismiss add card view controller
         dismiss(animated: true)
         }
         })*/
    }
    
    
    //MARK: - Calling API Methods
    func callStripChargeAPI(strToken:String) {
        var amount = ""
        if isJobPromote {
            amount = getMyUserDefaults(key: MyUserDefaults.JobPromoteAmount) as! String
        }else{
            amount = getMyUserDefaults(key: MyUserDefaults.ProductPromoteAmount) as! String
        }
        
        if isSellProd {
            amount = productDetail["product_price"] as! String
        }
        
        let parameters = ["i_key":WebURL.appKey,
                          "method":"charge",
                          "amount":Float(amount)!*100,
                          "currency":"usd",
                          "source":strToken,
                          "description":"Promote \(productName)"] as [String : Any]
        appDelegate.showHud()
        request(WebURL.stripeCharge, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("stripeCharge:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as? [String : Any] {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.addCardViewController.dismiss(animated: true, completion: {
                            if self.isSellProd {
                                self.callProductPurchase(promoteStatus: "1")
                            }else{
                                if self.isJobPromote {
                                    self.callPromoteJob(promoteStatus: "1")
                                }else{
                                    self.callPromoteProduct(promoteStatus: "1")
                                }
                            }
                        })
                        
                        
                    }else{
                        self.addCardViewController.dismiss(animated: true, completion: {
                            if self.isSellProd {
                                self.callProductPurchase(promoteStatus: "0")
                            }else{
                                if self.isJobPromote {
                                    self.callPromoteJob(promoteStatus: "0")
                                }else{
                                    self.callPromoteProduct(promoteStatus: "0")
                                }
                            }
                        })
                    }
                    
                }
            }else{
                self.addCardViewController.dismiss(animated: true, completion:nil)
            }
        })
    }
    
    func callPromoteProduct(promoteStatus:String) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        let parameters = ["i_key":WebURL.appKey,
                          "product_id":productDetail["product_id"] ?? "",
                          "user_id":userDetail["user_id"] ?? "",
                          "promote_status":promoteStatus] as [String : Any]
        appDelegate.showHud()
        request(WebURL.promoteProduct, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("promoteProduct:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as? [String : Any] {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.navigationController?.popViewController(animated: true)
                        //self.dismiss(animated: true)
                    }else{
                        //self.dismiss(animated: true)
                    }
                    
                }
            }
        })
    }
    
    func callProductPurchase(promoteStatus:String) {
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        var parameters = [String:Any]()
        parameters["i_key"] = WebURL.appKey
        parameters["product_id"] = productDetail["product_id"] ?? ""
        parameters["user_id"] = userDetail["user_id"] ?? ""
        parameters["address"] = addressDetail["address"] ?? ""
        parameters["city"] = addressDetail["city"] ?? ""
        parameters["state"] = addressDetail["state"] ?? ""
        parameters["email"] = addressDetail["email"] ?? ""
        parameters["phone"] = addressDetail["phone"] ?? ""
        parameters["amount"] = productDetail["product_price"] ?? ""
        parameters["pincode"] = addressDetail["pincode"] ?? ""
        parameters["payment_status"] = promoteStatus
        
        appDelegate.showHud()
        request(WebURL.addProductPurchase, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("addProductPurchase:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as? [String : Any] {
                    if (dictResult["o_returnCode"] as! Bool){
                        let successMsg = dictResult["o_message"] as! String
                        showAlert(msg: successMsg, completion: { (true) in
                            self.navigationController?.popToRootViewController(animated: true)
                            //self.dismiss(animated: true, completion: nil)
                        })
                        //self.dismiss(animated: true)
                    }else{
                        let successMsg = dictResult["o_message"] as! String
                        showAlert(msg: successMsg, completion: { (true) in
                            //self.presentingViewController?.dismiss(animated: true, completion: nil)
                        })
                    }
                    
                }
            }
        })
    }
    
    func callPromoteJob(promoteStatus:String) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        let parameters = ["i_key":WebURL.appKey,
                          "job_id":productDetail["hire_id"] ?? "",
                          "user_id":userDetail["user_id"] ?? "",
                          "promote_status":promoteStatus] as [String : Any]
        appDelegate.showHud()
        request(WebURL.promoteJob, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("promoteJob:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as? [String : Any] {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.navigationController?.popViewController(animated: true)
                        //self.dismiss(animated: true)
                    }else{
                        //self.dismiss(animated: true)
                    }
                    
                }
            }
        })
    }
    
    //MARK: - Tapped Event
    @IBAction func tappedOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnStrip(_ sender: Any){
        addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        
        // Present add card view controller
        let navigationController = UINavigationController(rootViewController: addCardViewController)
        present(navigationController, animated: true)
    }
    
    // MARK: - Single Payment
    @IBAction func buyClothingAction(_ sender: AnyObject) {
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        var amount = ""
        if isJobPromote {
            amount = getMyUserDefaults(key: MyUserDefaults.JobPromoteAmount) as! String
        }else {
            amount = getMyUserDefaults(key: MyUserDefaults.ProductPromoteAmount) as! String
        }
        
        if isSellProd {
            amount = productDetail["product_price"] as! String
        }
        // Optional: include multiple items
        let item1 = PayPalItem(name: productName, withQuantity: 1, withPrice: NSDecimalNumber(string: amount), withCurrency: "USD", withSku: "")
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.0")
        let tax = NSDecimalNumber(string: "0.0")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: productName, intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
        
    }
    
    // PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        //successView.isHidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            if self.isSellProd {
                self.callProductPurchase(promoteStatus: "1")
            }else {
                if self.isJobPromote {
                    self.callPromoteJob(promoteStatus: "1")
                }else{
                    self.callPromoteProduct(promoteStatus: "1")
                }
            }
            self.resultText = completedPayment.description
            //self.showSuccess()
        })
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
