//
//  MyPostingViewController.swift
//  SolvoList
//
//  Created by ronak patel on 10/11/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire
class BuyingCell: UITableViewCell {
    @IBOutlet var imgProduct:UIImageView!
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var lblPrice:UILabel!
    @IBOutlet var lblTime:UILabel!
    @IBOutlet var btnMessage:UIButton!
}

class PostingCell: UITableViewCell {
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var lblPrice:UILabel!
    @IBOutlet var lblDescription:UILabel!
    @IBOutlet var lblPostedBy:UILabel!
    @IBOutlet var btnEdit:UIButton!
    @IBOutlet var btnAnnouncement:UIButton!
    @IBOutlet var btnDelete:UIButton!
    @IBOutlet var btnSold:UIButton!
}

class ApplyJobCell: UITableViewCell {
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var lblSkill:UILabel!
    @IBOutlet var lblDescription:UILabel!
    @IBOutlet var btnMessage:UIButton!
    @IBOutlet var btnAnnouncement:UIButton!
    @IBOutlet var btnShare:UIButton!
}

class SellingCell: UICollectionViewCell {
    @IBOutlet var imgProduct:UIImageView!
    @IBOutlet var lblViews:UILabel!
    @IBOutlet var lblPrice:UILabel!
    @IBOutlet var btnSold:UIButton!
    @IBOutlet var btnPromote:UIButton!
    @IBOutlet var btnEdit:UIButton!
}

class MyPostingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    


    @IBOutlet var tblPosting:UITableView!
    
    @IBOutlet var collectionSelling:UICollectionView!
    
    @IBOutlet var btnBuying:UIButton!
    @IBOutlet var btnSelling:UIButton!
    @IBOutlet var btnPostJob:UIButton!
    @IBOutlet var btnApplyJob:UIButton!
    
    @IBOutlet var constraintWidth:NSLayoutConstraint!
    @IBOutlet var constraintXpos:NSLayoutConstraint!
    var searchBar:UISearchBar!
    var arrBuyProduct = [Any]()
    var arrApplyJob = [Any]()
    var arrPostedJob = [Any]()
    var arrSellingProduct = [Any]()
    let dateFormatter = DateFormatter()
    var strFlag = ""
    
    
    //PromoteView
    
    @IBOutlet var viewPromoteContainer: UIView!
    
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnPromote: UIButton!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var imgPromote: UIImageView!
    @IBOutlet var btnTagSold: UIButton!
    @IBOutlet var btnDeleteJob: UIButton!
    @IBOutlet var lblPromotePrice: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionSelling.isHidden = true
        dateFormatter.dateFormat = DateFormate.YYYYMMDD24
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgAccount"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "My Postings"
        
        self.tblPosting.estimatedRowHeight = 100.0
        
        
        
        
        strFlag = "Buying"
        DispatchQueue.main.async {
            self.constraintWidth.constant = self.btnBuying.frame.size.width
            self.constraintXpos.constant = self.btnBuying.frame.origin.x
        }
        
        self.viewPromoteContainer.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //-------API Calling---------
        self.getMyPostingListing()
    }
    
    //MARK: - Methods
    
    func getMyPostingListing() {
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if userDetail == nil {
            appDelegate.showAlertMessage(strMessage: "Please login first.")
            return
        }
        
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        request(WebURL.myPostingAPI, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("Get List:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        
                        let arrTemp = dictResult["o_data"] as! [Any]
                        let dictAll = arrTemp[0] as! [String : Any]
                        self.arrBuyProduct = dictAll["buying"] as! [Any]
                        self.arrApplyJob = dictAll["applied_job"] as! [Any]
                        self.arrSellingProduct = dictAll["selling"] as! [Any]
                        self.arrPostedJob = dictAll["posted_job"] as! [Any]
                        self.tblPosting.reloadData()
                        self.collectionSelling.reloadData()
                    }else{
                        
                    }
                    
                }
            }else{
                showAlert(msg: "This application requires internet connection", completion: nil)
            }
        })
    }
    
    func callDeleteJobAPI(strJobID:String) {
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if userDetail == nil {
            appDelegate.showAlertMessage(strMessage: "Please login first.")
            return
        }
        
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? "",
                          "job_id":strJobID] as [String : Any]
        appDelegate.showHud()
        request(WebURL.deleteJob, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("Get List:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let _:[String:Any] = response.result.value as! [String : Any]? {
                    
                }
            }
        })
    }
    
    func callAPIMarkAsSold(productId:String, soldStatus:String) {
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if userDetail == nil {
            appDelegate.showAlertMessage(strMessage: "Please login first.")
            return
        }
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? "",
                          "product_id":productId,
                          "sold_status":soldStatus] as [String : Any]
        appDelegate.showHud()
        request(WebURL.markSold, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("markSold:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as? [String : Any] {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.getMyPostingListing()
                        //self.tblPosting.reloadData()
                    }else{
                        
                    }
                    
                }
            }
        })
    }
    
    func callAPIMarkAsJobSold(jobId:String, soldStatus:String) {
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if userDetail == nil {
            appDelegate.showAlertMessage(strMessage: "Please login first.")
            return
        }
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? "",
                          "job_id":jobId,
                          "mark_status":soldStatus] as [String : Any]
        appDelegate.showHud()
        request(WebURL.markJob, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("markJob:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as? [String : Any] {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.getMyPostingListing()
                        //self.tblPosting.reloadData()
                    }else{
                        
                    }
                    
                }
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Tapped Event
    @IBAction func tappedOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnBarButton(_ sender: UIButton) {
        if sender == btnBuying {
            constraintWidth.constant = btnBuying.frame.size.width
            constraintXpos.constant = btnBuying.frame.origin.x
            tblPosting.isHidden = false
            collectionSelling.isHidden = true
            strFlag = "Buying"
        }else if sender == btnSelling {
            constraintWidth.constant = btnSelling.frame.size.width
            constraintXpos.constant = btnSelling.frame.origin.x
            collectionSelling.isHidden = false
            tblPosting.isHidden = true
            strFlag = "Selling"
        }else if sender == btnPostJob {
            constraintWidth.constant = btnPostJob.frame.size.width
            constraintXpos.constant = btnPostJob.frame.origin.x
            strFlag = "PostingJob"
            tblPosting.isHidden = false
            collectionSelling.isHidden = true
        }else if sender == btnApplyJob {
            constraintWidth.constant = btnApplyJob.frame.size.width
            constraintXpos.constant = btnApplyJob.frame.origin.x
            strFlag = "ApplyJob"
            tblPosting.isHidden = false
            collectionSelling.isHidden = true
        }
        tblPosting.reloadData()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func tappedOnEditProd(_ sender: UIButton) {
        let sellStep1 = storyBoards.Sell.instantiateViewController(withIdentifier: "SellProductStep1") as! SellProductStep1
        let dictDetail = self.arrSellingProduct[sender.tag] as! [String:Any]
        setMyUserDefaults(value: dictDetail, key: MyUserDefaults.prodUpdateDetail)
        sellStep1.isUpdate = true
        let navController1 = NavigationController(rootViewController: sellStep1)
        present(navController1, animated: true, completion: nil)
    }
    
    @IBAction func tappedOnProdSold(_ sender: UIButton) {
        let dictDetail = self.arrSellingProduct[sender.tag] as! [String:Any]
        let tagSold = dictDetail["product_sold_status"] as! String
        let cell = collectionSelling.cellForItem(at: IndexPath(row: sender.tag, section: 0)) as! SellingCell
        
        if tagSold == "1" {
            /*cell.btnSold.isSelected = false
            cell.btnSold.setTitle("Tag as Sold?", for: .normal)
            self.callAPIMarkAsSold(productId: dictDetail["product_id"] as! String, soldStatus: "0")*/
            appDelegate.showAlertMessage(strMessage: "You can not promote product after it is solved.")
        }else{
            cell.btnSold.isSelected = true
            cell.btnSold.setTitle("Repost", for: .normal)
            self.callAPIMarkAsSold(productId: dictDetail["product_id"] as! String, soldStatus: "1")
        }
    }
    
    @IBAction func tappedOnEditJob(_ sender: UIButton) {
        let hireStep1 = storyBoards.Hire.instantiateViewController(withIdentifier: "HireStep1") as! HireStep1
        let dictDetail = self.arrPostedJob[sender.tag] as! [String:Any]
        setMyUserDefaults(value: dictDetail, key: MyUserDefaults.jobUpdateDetail)
        
        hireStep1.isUpdate = true
        let navController1 = NavigationController(rootViewController: hireStep1)
        present(navController1, animated: true, completion: nil)
    }
    
    @IBAction func tappedOnPromoteProd(_ sender: UIButton) {
        
        let dictDetail = self.arrSellingProduct[sender.tag] as! [String:Any]
        lblPromotePrice.text = "$\(dictDetail["product_price"] as! String)"
        let strURL = dictDetail["product_cover_image"] as! String
        btnTagSold.isHidden = true
        btnDeleteJob.isHidden = true
        btnShare.tag = sender.tag
        if dictDetail["product_sold_status"] as! String == "1" {
            btnTagSold.isSelected = true
        }else{
            btnTagSold.isSelected = false
        }
        imgPromote.sd_setImage(with: URL(string: strURL), completed: { (image, error, nil, url) in
            
        })
        self.viewPromoteContainer.tag = sender.tag
        self.viewPromoteContainer.isHidden = false
        self.viewPromoteContainer.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.viewPromoteContainer.transform = .identity
        }, completion: {(finished: Bool) -> Void in
            // do something once the animation finishes, put it here
        })
    }
    
    @IBAction func tappedOnCancel(_ sender: UIButton) {
        self.viewPromoteContainer.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.viewPromoteContainer.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            self.viewPromoteContainer.isHidden = true
        })
    }
    
    @IBAction func tappedOnMarkAsSold(_ sender: UIButton) {
        let dictDetail = self.arrSellingProduct[self.viewPromoteContainer.tag] as! [String:Any]
        
        if sender.isSelected {
            sender.isSelected = false
            self.callAPIMarkAsSold(productId: dictDetail["product_id"] as! String, soldStatus: "0")
        }else{
            sender.isSelected = true
            self.callAPIMarkAsSold(productId: dictDetail["product_id"] as! String, soldStatus: "1")
        }
        
    }
    
    @IBAction func tappedOnPromote(_ sender: UIButton) {
        self.viewPromoteContainer.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.viewPromoteContainer.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            self.viewPromoteContainer.isHidden = true
            let dictDetail = self.arrSellingProduct[self.viewPromoteContainer.tag] as! [String:Any]
            let paymentView = storyBoards.Main.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            paymentView.productDetail = dictDetail
            self.navigationController?.pushViewController(paymentView, animated: true)
        })
    }
    
    @IBAction func tappedOnDeleteJob(_ sender: UIButton) {
        let alertController = UIAlertController(title: "SolvoList", message: "Are you sure you want to delete job?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: {
            alert -> Void in
            let dictJob = self.arrPostedJob[sender.tag] as! [String:Any]
            self.arrPostedJob.remove(at: sender.tag)
            self.tblPosting.reloadData()
            self.callDeleteJobAPI(strJobID: dictJob["hire_id"] as! String)
        }))
        alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func tappedOnPromoteJob(_ sender: UIButton) {
        let dictDetail = self.arrPostedJob[sender.tag] as! [String:Any]
        let paymentView = storyBoards.Main.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        paymentView.productDetail = dictDetail
        paymentView.isJobPromote = true
        self.navigationController?.pushViewController(paymentView, animated: true)
    }
    
    @IBAction func tappedOnShare(_ sender: UIButton) {
        let dictDetail = self.arrApplyJob[sender.tag] as! [String:Any]
        print("\(dictDetail)")
        
        let text = "Job Title: \(dictDetail["job_title"] as! String)"
        let description = "Job Description: \(dictDetail["job_description"] as! String)"
        let skill = "Skill: \(dictDetail["skill_1"] as! String), \(dictDetail["skill_2"] as! String), \(dictDetail["skill_3"] as! String)"
        
        let shareAll = [text, description, skill] as [Any]
        
        let activityViewController = UIActivityViewController(activityItems: shareAll , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func tappedOnShareProd(_ sender: UIButton) {
        let dictDetail = self.arrSellingProduct[sender.tag] as! [String:Any]
        print("\(dictDetail)")
        
        let text = "Product Name: \(dictDetail["product_name"] as! String)"
        let description = "Price: $\(dictDetail["product_price"] as! String)"
        let skill = "Image: \(dictDetail["product_cover_image"] as! String)"
        
        let shareAll = [text, description, skill] as [Any]
        
        let activityViewController = UIActivityViewController(activityItems: shareAll , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func tappedOnBuyingMessage(_ sender: UIButton) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if let userEmail = userDetail["email"] as? String {
            FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
                if let err:Error = error {
                    print(err.localizedDescription)
                    return
                }
                //let strUID = FIRAuth.auth()?.currentUser?.uid
                let dictProduct = self.arrBuyProduct[sender.tag] as! [String:Any]
                let dictUser = dictProduct["product_user_detail"] as! [String : Any]
                //let dictUser = dictProduct["userprofile"] as! [String:Any]
                print("Email:\(userEmail)")
                let strProdEmail = dictUser["apply_email"] as! String
                print("MD5: \(chatId([userEmail,strProdEmail]))")
                let groupID = chatId([userEmail,strProdEmail])
                
                _ = chatId([groupID,userEmail])
                
                let object = FObject(path: FRECENT_PATH)
                object[FRECENT_OBJECTID] = groupID
                object[FRECENT_USERID] = dictProduct["user_id"] as! String
                object[FRECENT_CURRENTUSERID] = userDetail["user_id"] as! String
                
                object[FRECENT_NAME] = dictUser["apply_name"] as! String
                object[FRECENT_CURRENTNAME] = userDetail["name"] as! String
                
                object[FRECENT_EMAIL] = dictUser["apply_email"] as! String
                object[FRECENT_CURRENTEMAIL] = userDetail["email"] as! String
                
                object[FRECENT_PICTURE] = dictProduct["profile_picture"] as! String
                object[FRECENT_CURRENTPICTURE] = userDetail["profile_picture"] as! String
                object[FRECENT_MSGID] = groupID
                object.saveInBackground()
                
                
                let chatView = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatView.hidesBottomBarWhenPushed = true
                chatView.messageObjectID = groupID
                chatView.senderDisplayName = dictUser["apply_name"] as! String
                chatView.userID = dictProduct["user_id"] as! String
                self.navigationController?.pushViewController(chatView, animated: true)
            })
        }
    }
    
    @IBAction func tappedOnApplyMessage(_ sender: UIButton) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if let userEmail = userDetail["email"] as? String {
            FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
                if let err:Error = error {
                    print(err.localizedDescription)
                    return
                }
                //let strUID = FIRAuth.auth()?.currentUser?.uid
                let dictProduct = self.arrApplyJob[sender.tag] as! [String:Any]
                let dictUser = dictProduct["posted_user_detail"] as! [String : Any]
                //let dictUser = dictProduct["userprofile"] as! [String:Any]
                print("Email:\(userEmail)")
                let strProdEmail = dictUser["apply_email"] as! String
                print("MD5: \(chatId([userEmail,strProdEmail]))")
                let groupID = chatId([userEmail,strProdEmail])
                
                _ = chatId([groupID,userEmail])
                
                let object = FObject(path: FRECENT_PATH)
                object[FRECENT_OBJECTID] = groupID
                object[FRECENT_USERID] = dictProduct["user_id"] as! String
                object[FRECENT_CURRENTUSERID] = userDetail["user_id"] as! String
                
                object[FRECENT_NAME] = dictUser["apply_name"] as! String
                object[FRECENT_CURRENTNAME] = userDetail["name"] as! String
                
                object[FRECENT_EMAIL] = dictUser["apply_email"] as! String
                object[FRECENT_CURRENTEMAIL] = userDetail["email"] as! String
                
                object[FRECENT_PICTURE] = dictUser["profile_picture"] ?? ""
                object[FRECENT_CURRENTPICTURE] = userDetail["profile_picture"] as! String
                object[FRECENT_MSGID] = groupID
                object.saveInBackground()
                
                
                let chatView = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatView.hidesBottomBarWhenPushed = true
                chatView.messageObjectID = groupID
                chatView.senderDisplayName = dictUser["apply_name"] as! String
                chatView.userID = dictProduct["user_id"] as! String
                self.navigationController?.pushViewController(chatView, animated: true)
            })
        }
    }
    
    @IBAction func tappedOnJobDelete(_ sender: UIButton) {
        
    }
    
    @IBAction func tappedOnJobSold(_ sender: UIButton) {
        let dictDetail = self.arrPostedJob[sender.tag] as! [String:Any]
        let tagSold = dictDetail["Mark_Status"] as! String
        let cell = tblPosting.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! PostingCell
        
        if tagSold == "1" {
            /*cell.btnSold.isSelected = false
            cell.btnSold.setTitle("Tag as Sold?", for: .normal)
            self.callAPIMarkAsJobSold(jobId: dictDetail["hire_id"] as! String, soldStatus: "0")*/
            appDelegate.showAlertMessage(strMessage: "You can not promote product after it is solved.")
        }else{
            cell.btnSold.isSelected = true
            cell.btnSold.setTitle("Repost", for: .normal)
            self.callAPIMarkAsJobSold(jobId: dictDetail["hire_id"] as! String, soldStatus: "1")
        }
    }
    
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if strFlag == "Buying" {
            return arrBuyProduct.count
        }else if strFlag == "PostingJob" {
            return self.arrPostedJob.count
        }else if strFlag == "ApplyJob" {
            return self.arrApplyJob.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if strFlag == "Buying" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuyingCell") as! BuyingCell
            let dictDetail = self.arrBuyProduct[indexPath.row] as! [String:Any]
            cell.lblTitle.text = dictDetail["product_name"] as? String
            cell.lblPrice.text = "$\(dictDetail["product_price"] as! String)"
            let uploadDate = dateFormatter.date(from: dictDetail["buy_created_datetime"] as! String)
            let strDateSince:String = (uploadDate?.timeAgoSinceDate(numericDates: true))!
            
            cell.lblTime.text = "\(String(describing: strDateSince))"
            let strURL = dictDetail["product_cover_image"] as! String
            cell.imgProduct.sd_setImage(with: URL(string: strURL), completed: { (image, error, nil, url) in
                
            })
            
            cell.btnMessage.tag = indexPath.row
            cell.btnMessage.addTarget(self, action: #selector(tappedOnBuyingMessage(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else if strFlag == "PostingJob" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostingCell") as! PostingCell
            let dictDetail = self.arrPostedJob[indexPath.row] as! [String:Any]
            cell.lblTitle.text = dictDetail["job_title"] as? String
            cell.lblDescription.text = dictDetail["job_description"] as? String
            cell.lblPrice.text = "$\(dictDetail["pay_per_hour"] as! String)"
            cell.lblPostedBy.text = dictDetail["company_name"] as? String
            
            cell.btnAnnouncement.tag = indexPath.row
            cell.btnAnnouncement.addTarget(self, action: #selector(tappedOnPromoteJob(_:)), for: .touchUpInside)
            
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(tappedOnDeleteJob(_:)), for: .touchUpInside)
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(tappedOnEditJob(_:)), for: .touchUpInside)
            
            
            let tagSold = dictDetail["Mark_Status"] as! String
            if tagSold == "1" {
                cell.btnSold.setTitle("Repost", for: .normal)
                cell.btnSold.isSelected = true
            }else{
                cell.btnSold.setTitle("Mark as filled?", for: .normal)
                cell.btnSold.isSelected = false
            }
            
            cell.btnSold.tag = indexPath.row
            cell.btnSold.addTarget(self, action: #selector(tappedOnJobSold(_:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            return cell
        }else if strFlag == "ApplyJob" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ApplyJobCell") as! ApplyJobCell
            let dictDetail = self.arrApplyJob[indexPath.row] as! [String:Any]
            cell.lblTitle.text = dictDetail["job_title"] as? String
            cell.lblDescription.text = dictDetail["job_description"] as? String
            cell.lblSkill.text = "\(dictDetail["skill_1"] as! String), \(dictDetail["skill_2"] as! String), \(dictDetail["skill_3"] as! String)"
            //cell.btnAnnouncement.isHidden = true
            cell.btnMessage.tag = indexPath.row
            cell.btnMessage.addTarget(self, action: #selector(tappedOnApplyMessage(_:)), for: .touchUpInside)
            
            cell.btnMessage.tag = indexPath.row
            cell.btnMessage.addTarget(self, action: #selector(tappedOnApplyMessage(_:)), for: .touchUpInside)
            
            cell.btnShare.tag = indexPath.row
            cell.btnShare.addTarget(self, action: #selector(tappedOnShare(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuyingCell") as! BuyingCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSellingProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SellingCell", for: indexPath) as! SellingCell
        let dictDetail = self.arrSellingProduct[indexPath.row] as! [String:Any]
        cell.lblPrice.text = "$\(dictDetail["product_price"] as! String)"
        //cell.lblViews.text = ""
        let strURL = dictDetail["product_cover_image"] as! String
        cell.imgProduct.sd_setImage(with: URL(string: strURL), completed: { (image, error, nil, url) in
            
        })
        
        cell.btnPromote.setTitle("Promote (\(dictDetail["promote_count"] as! Int))", for: .normal)
        cell.btnPromote.tag = indexPath.row
        cell.btnPromote.addTarget(self, action: #selector(tappedOnPromoteProd(_:)), for: .touchUpInside)
        
        cell.btnEdit.isHidden = false
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(tappedOnEditProd(_:)), for: .touchUpInside)
        let tagSold = dictDetail["product_sold_status"] as! String
        if tagSold == "1" {
            cell.btnSold.setTitle("Repost", for: .normal)
            cell.btnSold.isSelected = true
        }else{
            cell.btnSold.setTitle("Tag as Sold?", for: .normal)
            cell.btnSold.isSelected = false
        }
        cell.btnSold.isHidden = false
        cell.btnSold.tag = indexPath.row
        cell.btnSold.addTarget(self, action: #selector(tappedOnProdSold(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (ScreenSize.SCREEN_WIDTH - 30) / 2
        let height = (width * 150) / 172
        return CGSize(width: width, height: height)
    }
    
    //MARK: - UIScrollViewDelegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //self.searchBar.resignFirstResponder()
        self.view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
