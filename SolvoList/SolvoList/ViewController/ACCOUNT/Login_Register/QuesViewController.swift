//
//  QuesViewController.swift
//  SolvoList
//
//  Created by Jaydeep on 9/21/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire

class QuesViewController: UIViewController,UITextFieldDelegate {
   
    @IBOutlet weak var txtMainSkill: UITextField!
    @IBOutlet weak var txtSecondarySkill: UITextField!
    @IBOutlet weak var txtTertiarySkill: UITextField!
    @IBOutlet weak var txtPayPerHour: UITextField!
    @IBOutlet weak var txtWhenStart: UITextField!
    @IBOutlet var scrollView:UIScrollView!
    var strTextFieldSelected = ""
    let dropDownMainSkill = DropDown()
    var isRedictToAccount:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Question"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white];
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        //------Navigation Button------
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
    
        
        //self.hideKeyboardWhenTappedAround()
        showData()

    }
    
    func showData() {
        if let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as? [String:Any] {
            txtMainSkill.text = userDetail["que1_query"] as? String
            txtSecondarySkill.text = userDetail["que2_query"] as? String
            txtTertiarySkill.text = userDetail["que3_query"] as? String
            txtPayPerHour.text = userDetail["que4_query"] as? String
            txtWhenStart.text = userDetail["que5_query"] as? String
        }
        
    }
    
    //MARK: - Custom Methods.
    func intilize(textField:UITextField) {
        
        
        // The view to which the drop down will appear on
        dropDownMainSkill.anchorView = textField // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        if textField == txtMainSkill {
            var arrMainSkill = [String]()
            for dictHire in appDelegate.arrHireSkill {
                let dict = dictHire as! [String:Any]
                arrMainSkill.append(dict["skill_60"] as! String)
            }
            dropDownMainSkill.dataSource = arrMainSkill
        }else if textField == txtSecondarySkill {
            var arrSecondarySkill = [String]()
            for dictHire in appDelegate.arrHireSkill {
                let dict = dictHire as! [String:Any]
                arrSecondarySkill.append(dict["skill_30"] as! String)
            }
            dropDownMainSkill.dataSource = arrSecondarySkill
        }else if textField == txtTertiarySkill {
            dropDownMainSkill.dataSource = ["Trainning"]
        }else if textField == txtWhenStart {
            dropDownMainSkill.dataSource = ["1 Week", "2 Week", "3 Week"]
        }
        
        
        dropDownMainSkill.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            textField.text = item
        }
    }

    
    func callQuesAPI() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        let parameters = ["i_key":WebURL.appKey,
                          "que1_query":txtMainSkill.text ?? "",
                          "que2_query":txtSecondarySkill.text ?? "",
                          "que3_query":txtTertiarySkill.text ?? "",
                          "que4_query":txtPayPerHour.text ?? "",
                          "que5_query":txtWhenStart.text ?? "",
                          "id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        request(WebURL.questionQuery, method: .post, parameters: parameters).responseJSON { (response) in
            print("QuestionQuery Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        if self.isRedictToAccount {
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            self.presentingViewController?.dismiss(animated: true, completion: nil)
                        }
                    }else{
                        let errorMsg = dictResult["o_message"] as! String
                        appDelegate.showAlertMessage(strMessage: errorMsg)
                    }
                    
                }
            }
        }
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnSubmit(_ sender:UIBarButtonItem) {
        //self.presentingViewController?.dismiss(animated: true, completion: nil)
        if txtMainSkill.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please select main skill.")
        }else if txtSecondarySkill.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please select secondart skill.")
        }else if txtTertiarySkill.text?.length == 0  {
            appDelegate.showAlertMessage(strMessage: "Please select tertiary skill.")
        }else if txtPayPerHour.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter pay per hour.")
        }else if txtWhenStart.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please select time.")
        }else{
            self.callQuesAPI()
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    
    //MARK: - Keyboard Show Hide 
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        textField.inputAccessoryView = self.toolbarInit()
        if textField == txtMainSkill{
            strTextFieldSelected = "txtMainSkill"
        }else if textField == txtSecondarySkill{
            strTextFieldSelected = "txtSecondarySkill"
        }else if textField == txtTertiarySkill{
            strTextFieldSelected = "txtTertiarySkill"
        }else if textField == txtPayPerHour{
            strTextFieldSelected = "txtPayPerHour"
        }else if textField == txtWhenStart{
            strTextFieldSelected = "txtWhenStart"
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.minimumDate = Date()
            txtWhenStart.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }
        textField.inputAccessoryView = self.toolbarInit()
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtWhenStart.text = dateFormatter.string(from: sender.date)
    }

    // MARK: - Keyboard Navigation
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtMainSkill" {
                txtSecondarySkill.becomeFirstResponder()
            }else if strTextFieldSelected == "txtSecondarySkill" {
                txtTertiarySkill.becomeFirstResponder()
            }else if strTextFieldSelected == "txtTertiarySkill"{
                txtPayPerHour.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPayPerHour"{
                txtWhenStart.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtMainSkill"{
                txtMainSkill.becomeFirstResponder()
            }else if strTextFieldSelected == "txtSecondarySkill" {
                txtMainSkill.becomeFirstResponder()
            }else if strTextFieldSelected == "txtTertiarySkill" {
                txtSecondarySkill.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPayPerHour"{
                txtTertiarySkill.resignFirstResponder()
            }else if strTextFieldSelected == "txtWhenStart"{
                txtPayPerHour.becomeFirstResponder()
            }
        }
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
