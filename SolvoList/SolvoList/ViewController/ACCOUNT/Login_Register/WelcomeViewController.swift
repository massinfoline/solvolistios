//
//  WelcomeViewController.swift
//  SolvoList
//
//  Created by ronak patel on 15/09/17.
//  Copyright © 2017 depixed.com. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import Alamofire

class WelcomeViewController: UIViewController, GIDSignInUIDelegate {

    
    var signInWith:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        
        
        //-------- NotificationCenter Register ------
        NotificationCenter.default.addObserver(self, selector: #selector(getGoogleUserData(notification:)), name: .signInWithGoogle, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnClose(_ sender:UIButton) {
        //dismiss(animated: true, completion: nil)
        exit(0)
    }
    
    @IBAction func tappedOnFacebook(_ sender:UIButton) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email","public_profile"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if let fbPermissions = fbloginresult.grantedPermissions {
                    if(fbPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }
                }
                
            }
        }
    }
    
    @IBAction func tappedOnGoogle(_ sender:UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func tappedOnLogin(_ sender:UIButton) {
        let loginView = self.storyboard?.instantiateViewController(withIdentifier: "SignInUpViewController") as! SignInUpViewController
        self.navigationController?.pushViewController(loginView, animated: true)
    }
    
    @IBAction func tappedOnRegister(_ sender:UIButton) {
        let registerView = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(registerView, animated: true)
    }
    
    
    //MARK: - Get Facebook User Details
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result ?? "")
                    let fbData = result as! [String:Any]
                    self.signInWith = "Facebook"
                    setMyUserDefaults(value: fbData, key: MyUserDefaults.FBdata)
                    self.isUserExist(strEmail: fbData["email"] as! String)
                }
            })
        }
    }
    
    
    //MARK: - Get Facebook User Details
    func getGoogleUserData(notification:NSNotification){
        if let googleData = getMyUserDefaults(key: MyUserDefaults.googleData) as? [String:Any]  {
            self.signInWith = "Google"
            self.isUserExist(strEmail: googleData["email"] as! String)
        }
    }
    
    
    func isUserExist(strEmail:String) {
        let parameters = ["i_key":WebURL.appKey,
                          "email":strEmail] as [String : Any]
        appDelegate.showHud()
        
        request(WebURL.emailVerification, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("emailVerification Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        setMyUserDefaults(value: true, key: MyUserDefaults.isLogin)
                        setMyUserDefaults(value: dictResult["o_data"] as! [String:Any], key: MyUserDefaults.UserData)
                        appDelegate.updateDeviceToken(strToken: getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String)
                        self.presentingViewController?.dismiss(animated: true, completion: nil)
                    }else{
                        setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
                        let registerView = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                        registerView.strSignInWith = self.signInWith
                        self.navigationController?.pushViewController(registerView, animated: true)
                        //let errorMsg = dictResult["o_message"] as! String
                        //appDelegate.showAlertMessage(strMessage: errorMsg)
                    }
                    
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }
    
    
    
    //MARK: - GIDSignInUIDelegate
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
