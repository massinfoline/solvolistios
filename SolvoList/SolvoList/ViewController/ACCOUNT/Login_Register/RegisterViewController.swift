//
//  RegisterViewController.swift
//  SolvoList
//
//  Created by ronak patel on 19/09/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import SDWebImage
class RegisterViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var imgJobAlert: UIImageView!
    @IBOutlet var txtZipCode: UITextField!
    @IBOutlet var txtFullName: UITextField!
    @IBOutlet var txtEmail:UITextField!
    @IBOutlet var txtCEmail: UITextField!
    @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var txtPassword:UITextField!
    @IBOutlet var viewPassword:UIView!
    var userProfile:UIImage!
    var strSignInWith:String = ""
    var strSocialUserID = ""
    
    var strTextFieldSelected:String = ""
    var strAddress = ""
    
    @IBOutlet var scrollView:UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Sign up with email"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white];
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        //------Navigation Button------
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Help", style: .plain, target: self, action: #selector(tappedOnHelp(_:)))
        
        //self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imgJobAlert.isHighlighted = true
        registerNotifications()
        self.fillSocialMediaData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    //MARK: - Private Methods
    func fillSocialMediaData() {
        if strSignInWith == "Facebook" {
            self.title = "Sign up with facebook"
            let fbData = getMyUserDefaults(key: MyUserDefaults.FBdata) as! [String:Any]
            if let email = fbData["email"] {
                self.txtEmail.text = email as? String
                self.txtCEmail.text = email as? String
                self.txtEmail.isUserInteractionEnabled = false
                self.txtCEmail.isUserInteractionEnabled = false
                self.txtPassword.isUserInteractionEnabled = false
                self.viewPassword.isHidden = true
                self.strSocialUserID = fbData["id"] as! String
            }
            
            if let name = fbData["name"] {
                self.txtFullName.text = name as? String
                self.txtFullName.isUserInteractionEnabled = false
            }
            
            if let picture = fbData["picture"] as? [String:Any] {
                let picData = picture["data"] as! [String:Any]
                let strURL = picData["url"]
                self.imgView.sd_setImage(with: URL(string: strURL as! String), completed: { (image, error, nil, url) in
                    if image != nil {
                        self.userProfile = image
                    }
                })
            }
            
        }else if strSignInWith == "Google" {
            self.title = "Sign up with google"
            let googleData = getMyUserDefaults(key: MyUserDefaults.googleData) as! [String:Any]
            if let email = googleData["email"] {
                self.txtEmail.text = email as? String
                self.txtCEmail.text = email as? String
                self.txtEmail.isUserInteractionEnabled = false
                self.txtCEmail.isUserInteractionEnabled = false
                self.txtPassword.isUserInteractionEnabled = false
                self.txtPassword.isHidden = true
                self.strSocialUserID = googleData["userID"] as! String
            }
            
            if let name = googleData["Profile"] {
                self.txtFullName.text = name as? String
                self.txtFullName.isUserInteractionEnabled = false
            }
            
            
            if let picture = googleData["profilePic"] as? String {
                self.imgView.sd_setImage(with: URL(string: picture), completed: { (image, error, nil, url) in
                    if image != nil {
                        self.userProfile = image
                    }
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Button Tapped Event
    @IBAction func tappedOnHelp(_ sender:UIBarButtonItem) {
        let helpView = self.storyboard?.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        self.navigationController?.pushViewController(helpView, animated: true)
    }
    
    @IBAction func tappedOnZipcode(_ sender:UIBarButtonItem) {
        
        
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            //let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let location = CLLocation(latitude: latitude, longitude: longitude)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    
                    print(pm.postalCode ?? "") //prints zip code
                    print(pm.locality ?? "") //prints zip code
                    print(pm.administrativeArea ?? "") //prints zip code
                    print(pm.country ?? "") //prints zip code
                    self.txtZipCode.text = pm.postalCode
                    let locality = (pm.locality! != "") ? pm.locality! : ""
                    //let administrativeArea = (pm.administrativeArea! != "") ? ", \(pm.administrativeArea!)" : ""
                    //let country = (pm.country! != "") ? ", \(pm.country!)" : ""
                    self.strAddress =  locality // + administrativeArea + country
                }
                else {
                    print("Problem with the data received from geocoder")
                }
            })
        }
        
        
    }
    
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnRegister(_ sender:UIButton) {
        var strLatutude = ""
        var strLongitude = ""
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            strLatutude = "\(latitude)"
            strLongitude = "\(longitude)"
        }
        
        if txtFullName.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter name")
        }else if txtZipCode.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter zip code")
        }else if !((txtZipCode.text?.isNumber())!) {
            appDelegate.showAlertMessage(strMessage: "Please enter valid zip code")
        }else if !((txtEmail.text?.isValidEmail())!)  {
            appDelegate.showAlertMessage(strMessage: "Please enter valid email id")
        }else if txtCEmail.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter confirm email id")
        }else if txtEmail.text != txtCEmail.text {
            appDelegate.showAlertMessage(strMessage: "Email id do not match")
        }else if txtPhoneNumber.text?.length == 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter phone number")
        }else if (txtPassword.text?.length)! < 8 && strSignInWith == ""{
            appDelegate.showAlertMessage(strMessage: "Password must be > 8 characters")
        }else{
            var deviceToken = ""
            if let token = getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as? String {
                deviceToken = token
            }
            var parameters = ["i_key":WebURL.appKey,
                              "name":txtFullName.text ?? "",
                              "email":txtEmail.text ?? "",
                              "password":txtPassword.text ?? "",
                              "zipcode":txtZipCode.text ?? "",
                              "phonenumber":txtPhoneNumber.text ?? "",
                              "device_type":"ios",
                              "device_id":deviceToken,
                              "lat":strLatutude,
                              "long":strLongitude,
                              "address":self.strAddress] as [String : Any]
            
            
            if self.strSignInWith == "Facebook"{
                parameters["fb_token"] = self.strSocialUserID
            }else if self.strSignInWith == "Google"{
                parameters["google_token"] = self.strSocialUserID
            }
            appDelegate.showHud()
            
            
            upload(multipartFormData: { (multipartFormData) in
                if self.userProfile != nil {
                    let data1 = UIImageJPEGRepresentation(self.userProfile!, 7.0)
                    multipartFormData.append(data1!, withName: "profile_picture", fileName: "userProfile.jpg", mimeType: "image/jpeg")
                }
                for (key, value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                
            }, to: WebURL.register, method: .post, encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    
                    upload.validate()
                    upload.responseJSON { response in
                        
                        appDelegate.hideHud()
                        if response.result.error != nil {
                            print("failure")
                            UIAlertView(title: "Fail", message: "Please retry again.", delegate: nil, cancelButtonTitle: "Ok").show()
                        } else {
                            print("success")
                            print("Register Result:\(response)")
                            if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                                if (dictResult["o_returnCode"] as! Bool){
                                    let successMsg = dictResult["o_message"] as! String
                                    //appDelegate.showAlertMessage(strMessage: errorMsg)
                                    showAlert(msg: successMsg, completion: { (true) in
                                        setMyUserDefaults(value: true, key: MyUserDefaults.isLogin)
                                        setMyUserDefaults(value: dictResult["o_data"] as! [String:Any], key: MyUserDefaults.UserData)
                                        if self.imgJobAlert.isHighlighted {
                                            let loginView = self.storyboard?.instantiateViewController(withIdentifier: "QuesViewController") as! QuesViewController
                                            self.navigationController?.pushViewController(loginView, animated: true)
                                        }else{
                                            //self.navigationController?.popViewController(animated: true)
                                            self.presentingViewController?.dismiss(animated: true, completion: nil)
                                        }
                                    })
                                }else{
                                    setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
                                    let errorMsg = dictResult["o_message"] as! String
                                    //appDelegate.showAlertMessage(strMessage: errorMsg)
                                    showAlert(msg: errorMsg, completion: { (true) in
                                        if errorMsg == "Your email address is already registered.Please use it to login." {
                                            let loginView = self.storyboard?.instantiateViewController(withIdentifier: "SignInUpViewController") as! SignInUpViewController
                                            self.navigationController?.pushViewController(loginView, animated: true)
                                        }
                                    })
                                }
                            }
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    //failure
                }
            })
            
    
            
           /* request(WebURL.register, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                print("Register Result:\(response)")
                appDelegate.hideHud()
                if response.result.isSuccess {
                    if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                        if (dictResult["o_returnCode"] as! Bool){
                            let successMsg = dictResult["o_message"] as! String
                            //appDelegate.showAlertMessage(strMessage: errorMsg)
                            showAlert(msg: successMsg, completion: { (true) in
                                //self.navigationController?.popViewController(animated: true)
                            if imgJobAlert.isHighlighted {
                            let loginView = self.storyboard?.instantiateViewController(withIdentifier: "QuesViewController") as! QuesViewController
                                self.navigationController?.pushViewController(loginView, animated: true)
                            }else{
                                self.navigationController?.popViewController(animated: true)
                            }
                            })
                        }else{
                            setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
                            let errorMsg = dictResult["o_message"] as! String
                            appDelegate.showAlertMessage(strMessage: errorMsg)
                        }
                        
                    }
                }
            })
            */
//            if imgJobAlert.isHighlighted {
//                let loginView = self.storyboard?.instantiateViewController(withIdentifier: "QuesViewController") as! QuesViewController
//                self.navigationController?.pushViewController(loginView, animated: true)
//            }else{
//                self.presentingViewController?.dismiss(animated: true, completion: nil)
//            }
            
            
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func tappedOnJobAlert(_ sender:UIControl) {
        if imgJobAlert.isHighlighted {
            imgJobAlert.isHighlighted = false
        }else{
            imgJobAlert.isHighlighted = true
        }
    }
    
    @IBAction func tappedOnProfile(_ sender:UIControl){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Open Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoCamera(target: self, canEdit: true)
        })
        let action2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoLibrary(target: self, canEdit: true)
        })
        let action3 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        present(alert, animated: true) { _ in }
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtZipCode {
            strTextFieldSelected = "txtZipCode"
        }else if textField == txtFullName  {
            strTextFieldSelected = "txtFullName"
        }else if textField == txtEmail  {
            strTextFieldSelected = "txtEmail"
        }else if textField == txtCEmail  {
            strTextFieldSelected = "txtCEmail"
        }else if textField == txtPhoneNumber  {
            strTextFieldSelected = "txtPhoneNumber"
        }else if textField == txtPassword  {
            strTextFieldSelected = "txtPassword"
        }
        
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtZipCode {
            txtFullName.becomeFirstResponder()
        }else if textField == txtFullName  {
            txtEmail.becomeFirstResponder()
        }else if textField == txtEmail  {
            txtCEmail.becomeFirstResponder()
        }else if textField == txtCEmail  {
            txtPhoneNumber.becomeFirstResponder()
        }else if textField == txtPhoneNumber  {
            txtPassword.becomeFirstResponder()
        }else if textField == txtPassword  {
            txtPassword.resignFirstResponder()
        }
        return true
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtFullName" {
                txtZipCode.becomeFirstResponder()
            }else if strTextFieldSelected == "txtZipCode" {
                txtEmail.becomeFirstResponder()
            }else if strTextFieldSelected == "txtEmail" {
                txtCEmail.becomeFirstResponder()
            }else if strTextFieldSelected == "txtCEmail" {
                txtPhoneNumber.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPhoneNumber" {
                txtPassword.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPassword" {
                txtPassword.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtFullName" {
                txtFullName.resignFirstResponder()
            }else if strTextFieldSelected == "txtZipCode" {
                txtFullName.becomeFirstResponder()
            }else if strTextFieldSelected == "txtEmail" {
                txtZipCode.becomeFirstResponder()
            }else if strTextFieldSelected == "txtCEmail" {
                txtEmail.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPhoneNumber" {
                txtCEmail.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPassword" {
                txtPhoneNumber.becomeFirstResponder()
            }
        }
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    //MARK: - 
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        // use the image
        userProfile = chosenImage
        imgView.image = chosenImage
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
