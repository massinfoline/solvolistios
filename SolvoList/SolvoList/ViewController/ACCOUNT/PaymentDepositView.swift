//
//  PaymentDepositView.swift
//  SolvoList
//
//  Created by ronak patel on 19/12/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit

class PaymentDepositView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Payment & Deposit"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnDeposite(_ sender:UIButton) {
        let depositView = self.storyboard?.instantiateViewController(withIdentifier: "DepositViewController") as! DepositViewController
        self.navigationController?.pushViewController(depositView, animated: true)
    }
    
    @IBAction func tappedOnHistory(_ sender:UIButton) {
        let historyView = self.storyboard?.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
        let strTitle = sender.title(for: .normal)
        historyView.strHistoryType = (strTitle?.trimmingCharacters(in: .whitespaces))!
        self.navigationController?.pushViewController(historyView, animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
