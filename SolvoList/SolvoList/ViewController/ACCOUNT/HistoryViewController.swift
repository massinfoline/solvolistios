//
//  HistoryViewController.swift
//  SolvoList
//
//  Created by ronak patel on 20/12/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire
class HistoryCell: UITableViewCell {
    @IBOutlet var lblName:UILabel!
    @IBOutlet var lblTime:UILabel!
    @IBOutlet var lblPaid:UILabel!
}

class HistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tblHistory:UITableView!
    var arrHistory = [Any]()
    var strHistoryType:String = ""
    let dateFormatter = DateFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblHistory.estimatedRowHeight = 50.0
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = strHistoryType
        
        self.getHistoryAPI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getHistoryAPI() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        var strURL = ""
        if self.strHistoryType == "Deposite History" {
            strURL = WebURL.displayDepositeHistory
        }else if self.strHistoryType == "Sell Promoted History" {
            strURL = WebURL.promoteProductList
        }else if self.strHistoryType == "Jobs Promoted History" {
            strURL = WebURL.promoteJobList
        }else if self.strHistoryType == "Order History" {
            strURL = WebURL.getProductPurchase
        }
        appDelegate.showHud()
        request(strURL, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("addDepositeAccount:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.arrHistory = dictResult["o_data"] as! [Any]
                        self.tblHistory.reloadData()
                    }else{
                        showAlert(msg: dictResult["o_message"] as! String, completion: { (true) in
                        })
                    }
                    
                }
            }
        })
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        let dictDetail = self.arrHistory[indexPath.row] as! [String:Any]
        if self.strHistoryType == "Deposite History" {
            cell.lblName.text = dictDetail["deposite_by"] as? String
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let depositeDate = dateFormatter.date(from: dictDetail["deposite_date"] as! String)
            cell.lblTime.text = depositeDate?.timeAgoSinceDate(numericDates: true)
            cell.lblPaid.text = "Paid : $ \(dictDetail["deposite_amount"] as! String)"
        }else if self.strHistoryType == "Sell Promoted History" {
            cell.lblName.text = dictDetail["product_name"] as? String
            cell.lblPaid.text = "Paid : $ \(dictDetail["amount"] as! String)"
            dateFormatter.dateFormat = DateFormate.YYYYMMDD24
            let depositeDate = dateFormatter.date(from: dictDetail["product_promote_created_datetime"] as! String)
            cell.lblTime.text = depositeDate?.timeAgoSinceDate(numericDates: true)
        }else if self.strHistoryType == "Jobs Promoted History" {
            cell.lblName.text = dictDetail["job_title"] as? String
            cell.lblPaid.text = "Paid : $ \(dictDetail["amount"] as! String)"
            dateFormatter.dateFormat = DateFormate.YYYYMMDD24
            let depositeDate = dateFormatter.date(from: dictDetail["job_promote_created_datetime"] as! String)
            cell.lblTime.text = depositeDate?.timeAgoSinceDate(numericDates: true)
        }else if self.strHistoryType == "Order History" {
            cell.lblName.text = dictDetail["product_name"] as? String
            cell.lblPaid.text = "Paid : $ \(dictDetail["amount"] as! String)"
            dateFormatter.dateFormat = DateFormate.YYYYMMDD24
            let depositeDate = dateFormatter.date(from: dictDetail["solvolist_purchase_created_datetime"] as! String)
            cell.lblTime.text = depositeDate?.timeAgoSinceDate(numericDates: true)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
