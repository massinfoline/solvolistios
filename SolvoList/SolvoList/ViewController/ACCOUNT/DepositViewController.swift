//
//  DepositViewController.swift
//  SolvoList
//
//  Created by ronak patel on 19/12/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire


class DepositViewController: UIViewController {

    @IBOutlet var txtNameAccount:UITextField!
    @IBOutlet var txtRoutingNumber:UITextField!
    @IBOutlet var txtAccountNumber:UITextField!
    
    @IBOutlet var imgViewChecking:UIImageView!
    @IBOutlet var imgViewSaving:UIImageView!
    @IBOutlet var imgViewBusiness:UIImageView!
    
    var strTextFieldSelected = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Deposit Account"
        
        getDepositeAccount()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    func getDepositeAccount() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        request(WebURL.getDepositeAccount, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("getDepositeAccount Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.txtNameAccount.text = dictResult["ACCOUNT_NAME"] as? String
                        self.txtRoutingNumber.text = dictResult["ROUTING_NO"] as? String
                        self.txtAccountNumber.text = dictResult["ACCOUNT_NO"] as? String
                        let strAccType = dictResult["ACCOUNT_TYPE"] as? String
                        if strAccType == "Checking" {
                            self.imgViewChecking.isHighlighted = true
                            self.imgViewSaving.isHighlighted = false
                            self.imgViewBusiness.isHighlighted = false
                        }else if strAccType == "Saving" {
                            self.imgViewChecking.isHighlighted = false
                            self.imgViewSaving.isHighlighted = true
                            self.imgViewBusiness.isHighlighted = false
                        }else if strAccType == "BusinessChecking" {
                            self.imgViewChecking.isHighlighted = false
                            self.imgViewSaving.isHighlighted = false
                            self.imgViewBusiness.isHighlighted = true
                        }
                    }else{
                        //let errorMsg = dictResult["o_message"] as! String
                        //appDelegate.showAlertMessage(strMessage: errorMsg)
                    }
                    
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
        
    }
    
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnDeposit(_ sender:UIButton) {
        if (txtNameAccount.text?.length)! == 0 {
            showAlert(msg: "Please enter name", completion: nil)
        }else if imgViewBusiness.isHighlighted == false && imgViewSaving.isHighlighted == false && imgViewChecking.isHighlighted == false {
            showAlert(msg: "Please select type", completion: nil)
        }else if (txtRoutingNumber.text?.length)! == 0 {
            showAlert(msg: "Please enter routing number", completion: nil)
        }else if (txtAccountNumber.text?.length)! == 0 {
            showAlert(msg: "Please enter account number", completion: nil)
        }else{
            var strType = ""
            if imgViewChecking.isHighlighted {
                strType = "Checking"
            }else if imgViewSaving.isHighlighted {
                strType = "Saving"
            }else if imgViewBusiness.isHighlighted {
                strType = "BusinessChecking"
            }
            let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
            let parameters = ["i_key":WebURL.appKey,
                              "user_id":userDetail["user_id"] ?? "",
                              "account_name":txtNameAccount.text ?? "",
                              "account_type":strType,
                              "routing_no":txtRoutingNumber.text ?? "",
                              "account_no":txtAccountNumber.text ?? ""] as [String : Any]
            appDelegate.showHud()
            request(WebURL.addDepositeAccount, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                print("addDepositeAccount:\(response)")
                appDelegate.hideHud()
                if response.result.isSuccess {
                    if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                        if (dictResult["o_returnCode"] as! Bool){
                            showAlert(msg: dictResult["o_message"] as! String, completion: { (true) in
                                self.navigationController?.popViewController(animated: true)
                            })
                            
                        }else{
                            showAlert(msg: dictResult["o_message"] as! String, completion: { (true) in
                            })
                        }
                        
                    }
                }
            })
        }
    }
    
    @IBAction func tappedOnType(_ sender:UIControl) {
        if sender.tag == 111 {
            imgViewChecking.isHighlighted = true
            imgViewSaving.isHighlighted = false
            imgViewBusiness.isHighlighted = false
        }else if sender.tag == 222 {
            imgViewChecking.isHighlighted = false
            imgViewSaving.isHighlighted = true
            imgViewBusiness.isHighlighted = false
        }else if sender.tag == 333 {
            imgViewChecking.isHighlighted = false
            imgViewSaving.isHighlighted = false
            imgViewBusiness.isHighlighted = true
        }
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtNameAccount {
            strTextFieldSelected = "txtNameAccount"
        }else if textField == txtRoutingNumber {
            strTextFieldSelected = "txtRoutingNumber"
        }else if textField == txtAccountNumber {
            strTextFieldSelected = "txtAccountNumber"
        }
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtNameAccount" {
                txtRoutingNumber.becomeFirstResponder()
            }else if strTextFieldSelected == "txtRoutingNumber" {
                txtAccountNumber.becomeFirstResponder()
            }else if strTextFieldSelected == "txtAccountNumber" {
                txtAccountNumber.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtNameAccount" {
                txtNameAccount.resignFirstResponder()
            }else if strTextFieldSelected == "txtRoutingNumber" {
                txtNameAccount.becomeFirstResponder()
            }else if strTextFieldSelected == "txtAccountNumber" {
                txtRoutingNumber.becomeFirstResponder()
            }
        }
    }
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
