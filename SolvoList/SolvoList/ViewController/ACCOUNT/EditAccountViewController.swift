//
//  EditAccountViewController.swift
//  SolvoList
//
//  Created by Jaydeep on 10/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire
class EditAccountViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var txtName: TextField!
    @IBOutlet weak var txtEmail: TextField!
    @IBOutlet weak var txtLocation: TextField!
    @IBOutlet weak var txtPassword: TextField!
    @IBOutlet weak var txtPhone: TextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    var strTextFieldSelected:String = ""
    var userProfile:UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        self.title = "Profile"
        
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        txtName.text = userDetail["name"] as? String
        txtEmail.text = userDetail["email"] as? String
        txtPhone.text = userDetail["phonenumber"] as? String
        txtLocation.text = userDetail["address"] as? String
        imgView.sd_setImage(with: URL(string: userDetail["profile_picture"] as! String), completed: { (image, error, nil, url) in
            if image == nil {
            }
        })
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    
    
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtName {
            strTextFieldSelected = "txtName"
        }else if textField == txtEmail {
            strTextFieldSelected = "txtEmail"
        }else if textField == txtLocation {
            strTextFieldSelected = "txtLocation"
        }else if textField == txtPassword {
            strTextFieldSelected = "txtPassword"
        }else if textField == txtPhone {
            strTextFieldSelected = "txtPhone"
        }
        textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnProfile(_ sender:UIControl){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Open Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoCamera(target: self, canEdit: false)
        })
        let action2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoLibrary(target: self, canEdit: false)
        })
        let action3 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        present(alert, animated: true) { _ in }
    }
    
    @IBAction func tappedOnUpdateProfile(_ sender:UIButton){
        updateProfileDetail()
    }
    
    func updateProfileDetail() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        let parameters = ["i_key":WebURL.appKey,
                          "name":txtName.text ?? "",
                          "email":txtEmail.text ?? "",
                          "password":txtPassword.text ?? "",
                          "phonenumber":txtPhone.text ?? "",
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        
        
        upload(multipartFormData: { (multipartFormData) in
            if self.userProfile != nil {
                let data1 = UIImageJPEGRepresentation(self.userProfile!, 7.0)
                multipartFormData.append(data1!, withName: "profile_picture", fileName: "userProfile.jpg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            
        }, to: WebURL.updateProfile, method: .post, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                
                
                upload.validate()
                upload.responseJSON { response in
                    
                    appDelegate.hideHud()
                    if response.result.error != nil {
                        print("failure")
                        UIAlertView(title: "Fail", message: "Please retry again.", delegate: nil, cancelButtonTitle: "Ok").show()
                    } else {
                        print("success")
                        print("updateProfile Result:\(response)")
                        if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                            if (dictResult["o_returnCode"] as! Bool){
                                let successMsg = dictResult["o_message"] as! String
                                showAlert(msg: successMsg, completion: { (true) in
                                    setMyUserDefaults(value: dictResult["o_data"] as! [String:Any], key: MyUserDefaults.UserData)
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }else{
                                let errorMsg = dictResult["o_message"] as! String
                                appDelegate.showAlertMessage(strMessage: errorMsg)
                            }
                        }
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                //failure
            }
        })
    }
    
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtName" {
                txtPassword.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPassword" {
                txtPhone.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPhone" {
                txtPhone.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtName" {
                txtName.resignFirstResponder()
            }else if strTextFieldSelected == "txtPassword" {
                txtName.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPhone" {
                txtPassword.becomeFirstResponder()
            }
        }
    }
    
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        // use the image
        userProfile = chosenImage
        imgView.image = chosenImage
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
