//
//  AccountViewController.swift
//  SolvoList
//
//  Created by ronak patel on 15/09/17.
//  Copyright © 2017 depixed.com. All rights reserved.
//

import UIKit
import Alamofire
import GoogleSignIn
import CoreLocation
import Stripe
class AccountViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnAccessory: UIButton!
}

class AccountViewCell2: UITableViewCell {
    
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var lblVersion: UILabel!
    
}

class AccountViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    
    var ProfileName = ["Photo","Your Name","xxxys@hah.com","Ahemdabad","Password","+91 111 222 3334","Connect Facebook","My Postings","Change job Skills","Setup Payments and Deposits"]
    
    var ProfileImage = [UIImage]()
    var strPostalCode = ""
    
    @IBOutlet var tblAccount:UITableView!
    
    override func awakeFromNib() {
        self.tabBarItem.title = "Account"
        self.title = "Account"
        self.tabBarItem.image = UIImage(named: "imgAccount")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:Color.COLOR_GREEN];
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgClose"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        ProfileImage = [#imageLiteral(resourceName: "imgUser"),#imageLiteral(resourceName: "user-name"),#imageLiteral(resourceName: "email"),#imageLiteral(resourceName: "location"),#imageLiteral(resourceName: "password"),#imageLiteral(resourceName: "mobile"),#imageLiteral(resourceName: "facebok"),#imageLiteral(resourceName: "my-posting"),#imageLiteral(resourceName: "change-job-skill"),#imageLiteral(resourceName: "setup-payment")]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:Color.COLOR_GREEN];
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.getUserProfile()
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        ProfileName = ["Photo",userDetail["name"] as! String,userDetail["email"] as! String,(userDetail["address"]) != nil ? userDetail["address"] as! String : "","Password",userDetail["phonenumber"] as! String,"Connect Facebook","My Postings","Change job Skills","Setup Payments and Deposits"]
        tblAccount.reloadData()
    }
    
    //MARK: - Button Action
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedOnZipcode(_ sender:UIBarButtonItem) {
        
        
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            //let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let location = CLLocation(latitude: latitude, longitude: longitude)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    
                    print(pm.postalCode ?? "") //prints zip code
                    print(pm.locality ?? "") //prints zip code
                    print(pm.administrativeArea ?? "") //prints zip code
                    print(pm.country ?? "") //prints zip code
                    self.strPostalCode = pm.postalCode!
                    let locality = (pm.locality! != "") ? pm.locality! : ""
                    let administrativeArea = (pm.administrativeArea! != "") ? ", \(pm.administrativeArea!)" : ""
                    let country = (pm.country! != "") ? ", \(pm.country!)" : ""
                    self.ProfileName[3] =  locality + administrativeArea + country
                    self.tblAccount.reloadData()
                    self.updateUserLocation()
                }
                else {
                    print("Problem with the data received from geocoder")
                }
            })
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableViewDataSource & Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "PROMOTE"
        }else if section == 1{
            return "PROFILE"
        }else if section == 2{
            return "SUPPORT"
        }
        return ""
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            return 10
        }else if section == 2{
            return 2
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 2 && indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! AccountViewCell2
            cell.btnLogout.addTarget(self, action: #selector(logOutMethod), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AccountViewCell
            if indexPath.section == 0 && indexPath.row == 0{
                cell.btnAccessory.setTitle("", for: .normal)
                cell.lblName.text = "Invite Facebook Friend"
                cell.imgView.image = #imageLiteral(resourceName: "invite-facebok.png")
                cell.accessoryType = .disclosureIndicator
            }else if indexPath.section == 1{
                cell.lblName.text = ProfileName[indexPath.row]
                if indexPath.row == 0 {
                    let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
                    cell.imgView.sd_setImage(with: URL(string: userDetail["profile_picture"] as! String), completed: { (image, error, nil, url) in
                        if image == nil {
                            cell.imgView.image = UIImage(named: "imgUser")
                        }
                    })
                }else{
                    cell.imgView.image = ProfileImage[indexPath.row]
                }
                cell.btnAccessory.setTitle("", for: .normal)
                if indexPath.row == 6 {
                    cell.accessoryType = .checkmark
                }else if indexPath.row == 0 || indexPath.row == 4 || indexPath.row == 5 {
                    cell.btnAccessory.setTitle("Edit", for: .normal)
                }else if indexPath.row == 3 {
                    cell.btnAccessory.setImage(#imageLiteral(resourceName: "gps.png"), for: .normal)
                    cell.btnAccessory.addTarget(self, action: #selector(tappedOnZipcode(_:)), for: .touchUpInside)
                }
            }else if indexPath.section == 2 && indexPath.row == 0{
                cell.lblName.text = "Help"
                cell.btnAccessory.setTitle("", for: .normal)
                cell.imgView.image = #imageLiteral(resourceName: "help.png")
                cell.accessoryType = .disclosureIndicator
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.row == 0 || indexPath.row == 4 || indexPath.row == 5) && indexPath.section == 1 {
            let sellStep1 = storyBoards.Main.instantiateViewController(withIdentifier: "EditAccountViewController") as! EditAccountViewController
            self.navigationController?.pushViewController(sellStep1, animated: true)
        }else if indexPath.row == 7 && indexPath.section == 1 {
            let sellStep1 = storyBoards.Main.instantiateViewController(withIdentifier: "MyPostingViewController") as! MyPostingViewController
            self.navigationController?.pushViewController(sellStep1, animated: true)
        }else if indexPath.row == 8 && indexPath.section == 1 {
            let quesViewController = self.storyboard?.instantiateViewController(withIdentifier: "QuesViewController") as! QuesViewController
            quesViewController.isRedictToAccount = true
            self.navigationController?.pushViewController(quesViewController, animated: true)
        }else if indexPath.row == 9 && indexPath.section == 1 {
            let paymentDepositView = self.storyboard?.instantiateViewController(withIdentifier: "PaymentDepositView") as! PaymentDepositView
            self.navigationController?.pushViewController(paymentDepositView, animated: true)
        }else if indexPath.row == 0 && indexPath.section == 2 {
            let helpView = self.storyboard?.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
            self.navigationController?.pushViewController(helpView, animated: true)
        }else if indexPath.section == 0 && indexPath.row == 0{
            
            
            
            if (UIApplication.shared.canOpenURL(URL(string: "fb://")!)) {
                UIApplication.shared.openURL(URL(string: "fb://")!)
            }
            else {
                UIApplication.shared.openURL(URL(string: "https://www.facebook.com")!)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 && indexPath.row == 1{
            
            return 100
        }else {
            
            return 50
        }
    }
    
    //MARK: - Methods
    func getUserProfile() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        request(WebURL.showProfile, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("showProfile Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        setMyUserDefaults(value: dictResult["o_data"] as! [String:Any], key: MyUserDefaults.UserData)
                    }else{
                        let errorMsg = dictResult["o_message"] as! String
                        appDelegate.showAlertMessage(strMessage: errorMsg)
                    }
                    
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
        
    }
    
    func updateUserLocation() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        var latitude = ""
        var longitude = ""
        if let lat = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            latitude = "\(lat)"
            longitude = "\(getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees)"
        }
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? "",
                          "zipcode":self.strPostalCode,
                          "address":self.ProfileName[3],
                          "lat":latitude,
                          "long":longitude] as [String : Any]
        appDelegate.showHud()
        request(WebURL.updateProfile, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("updateProfile Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        setMyUserDefaults(value: dictResult["o_data"] as! [String:Any], key: MyUserDefaults.UserData)
                    }else{
                        let errorMsg = dictResult["o_message"] as! String
                        appDelegate.showAlertMessage(strMessage: errorMsg)
                    }
                    
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })

    }
    
    func logOutMethod() {
        let alertController = UIAlertController(title: "LOGOUT", message: "Are you sure you want to logout?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Logout", style: .default, handler: {
            alert -> Void in
            setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
            self.dismiss(animated: true, completion: { 
                GIDSignIn.sharedInstance().signOut()
            })
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
