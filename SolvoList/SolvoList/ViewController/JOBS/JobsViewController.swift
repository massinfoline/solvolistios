//
//  JobsViewController.swift
//  SolvoList
//
//  Created by ronak patel on 15/09/17.
//  Copyright © 2017 depixed.com. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class JobCell: UITableViewCell {
    @IBOutlet var imgHire:UIImageView!
    @IBOutlet var lblJobTitle:UILabel!
    @IBOutlet var lblCompanyName:UILabel!
    @IBOutlet var lblDayBefore:UILabel!
    @IBOutlet var lblPayHr:UILabel!
    @IBOutlet var btnJobApply:UIButton!
    @IBOutlet var btnPromote:UIButton!
}

class JobsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {

    @IBOutlet var tblJobs:UITableView!
    var arrJobList = [Any]()
    var arrJobSearchList = [Any]()
    let dateFormatter = DateFormatter()
    
    var searchBar:UISearchBar!
    var strUserID = ""
    var dictJobDetail:[String:Any] = [:]
    
    override func awakeFromNib() {
        self.tabBarItem.title = "Jobs"
        self.title = "Jobs"
        self.tabBarItem.image = UIImage(named: "imgJobs")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat = DateFormate.YYYYMMDD24
        
        
        self.tblJobs.estimatedRowHeight = 100.0
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Jobs", style: .plain, target: self, action: nil)
        
        let titleView = UIView(frame: CGRect(x:0, y:0, width:(ScreenSize.SCREEN_WIDTH * 0.9), height:44))
        searchBar =  UISearchBar(frame: CGRect(x:0, y:0, width:(ScreenSize.SCREEN_WIDTH * 0.7), height:44))
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        searchBar.placeholder = "SolvoList"
        titleView.addSubview(searchBar)
        self.navigationItem.titleView = titleView
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getJobListing()
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        strUserID = userDetail["user_id"] as! String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    
    func getJobListing() {
        
        if appDelegate.strCityName == "" {
            let when = DispatchTime.now() + 5 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.getJobListing()
            }
            
            return;
        }
        
        var strLat = ""
        var strLong = ""
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            strLat = "\(latitude)"
            strLong = "\(longitude)"
        }
        
        let parameters = ["i_key":WebURL.appKey,
                          "address":appDelegate.strCityName,
                          "lat":strLat,
                          "long":strLong] as [String : Any]
        appDelegate.showHud()
        request(WebURL.getJobList, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("JobList List:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.arrJobList = dictResult["o_data"] as! [Any]
                        self.arrJobSearchList = self.arrJobList
                        self.tblJobs.reloadData()
                    }else{
                        
                    }
                    
                }
            }else{
                showAlert(msg: "This application requires internet connection", completion: nil)
            }
        })
    }
    
    func sendApplyJobMessage() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if let userEmail = userDetail["email"] as? String {
            FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
                if let err:Error = error {
                    print(err.localizedDescription)
                    return
                }
                //let strUID = FIRAuth.auth()?.currentUser?.uid
                let dictUser = self.dictJobDetail["userprofile"] as! [String:Any]
                print("User: \(dictUser)")
                let strProdEmail = dictUser["user_email"] as! String
                let groupID = chatId([userEmail,strProdEmail])
                
                //let strRecentID1 = self.chatId([groupID,userEmail])
                
                let object = FObject(path: FRECENT_PATH)
                object[FRECENT_OBJECTID] = groupID
                object[FRECENT_USERID] = dictUser["user_id"] as! String
                object[FRECENT_CURRENTUSERID] = userDetail["user_id"] as! String
                
                object[FRECENT_NAME] = dictUser["user_name"] as! String
                object[FRECENT_CURRENTNAME] = userDetail["name"] as! String
                
                object[FRECENT_EMAIL] = dictUser["user_email"] as! String
                object[FRECENT_CURRENTEMAIL] = userDetail["email"] as! String
                
                object[FRECENT_PICTURE] = dictUser["user_profile_picture"] as! String
                object[FRECENT_CURRENTPICTURE] = userDetail["profile_picture"] as! String
                object[FRECENT_MSGID] = groupID
                object.saveInBackground()
                
                let msgObject = FObject(path: FMESSAGE_PATH, subpath:groupID)
                let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
                
                msgObject[MESSAGE_TEXT] = "Hi I would like to apply to your job."
                msgObject[FMESSAGE_SENDERID] = FIRAuth.auth()?.currentUser?.uid ?? ""
                msgObject[FMESSAGE_SENDERNAME] = dictUser["user_name"] ?? ""
                msgObject[FMESSAGE_DEVICETYPE] = "ios"
                msgObject[FMESSAGE_DEVICETOKEN] = getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken)
                msgObject[FMESSAGE_CURRENTDEVICETYPE] = dictUser["device_type"] as! String
                msgObject[FMESSAGE_CURRENTDEVICETOKEN] = dictUser["device_id"] as! String
                msgObject[FMESSAGE_CURRENTUSERID] = userDetail["user_id"] ?? ""
                msgObject[FMESSAGE_USERID] = dictUser["user_id"] ?? ""
                msgObject.saveInBackground()
                
                let dictBody:[String:String] = ["email":userEmail,
                                                "text":"Hi I would like to apply to your job.",
                                                "username":userDetail["name"] as! String,
                                                "photo":"",
                                                "fcm_token": getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String,
                                                "currentEmail":userDetail["email"] as! String,
                                                "current_userid":userDetail["user_id"] as! String,
                                                "sound":"default"]
                print(dictBody)
                appDelegate.sendPushMessage(strToken: dictUser["device_id"] as! String, deviceType: dictUser["device_type"] as! String, dictBody: dictBody)
            })
        }
    }
    
    //MARK: - UIScrollViewDelegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    //MARK: - UITableViewDataSource & UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrJobSearchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell") as! JobCell
        let dictJob = self.arrJobSearchList[indexPath.row] as! [String:Any]
        cell.lblJobTitle.text = dictJob["job_title"] as? String
        cell.lblCompanyName.text = "Company Name: \(dictJob["company_name"] as! String)"
        cell.lblPayHr.text = "Pay/hr : $\(dictJob["pay_per_hour"] as! String)"
        let uploadDate = dateFormatter.date(from: dictJob["hire_created_datetime"] as! String)
        let strDateSince:String = (uploadDate?.timeAgoSinceDate(numericDates: true))!
        cell.lblDayBefore.text = "\(String(describing: strDateSince))"
        //cell.imgHire.sd_setImage(with: URL(string: dictJob["image"] as! String), completed: nil)
        cell.imgHire.sd_setImage(with: URL(string: dictJob["image"] as! String)) { (image, error, nil, url) in
            if image == nil {
                cell.imgHire.image = #imageLiteral(resourceName: "jobs")
            }
        }
        cell.btnJobApply.tag = indexPath.row
        cell.btnJobApply.addTarget(self, action: #selector(tappedOnApply(_:)), for: .touchUpInside)
        if strUserID == dictJob["user_id"] as! String {
            cell.btnJobApply.isHidden = true
            cell.btnPromote.isHidden = false
        }else{
            cell.btnJobApply.isHidden = false
            cell.btnPromote.isHidden = true
        }
        
        cell.btnPromote.tag = indexPath.row
        cell.btnPromote.addTarget(self, action: #selector(tappedOnPromote(_:)), for: .touchUpInside)
            
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let jobDetail = self.storyboard?.instantiateViewController(withIdentifier: "JobDetailViewController") as! JobDetailViewController
        jobDetail.jobDetail = self.arrJobSearchList[indexPath.row] as! [String : Any]
        jobDetail.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(jobDetail, animated: true)
    }
    
    //MARK: - Button Action Event
    @IBAction func tappedOnApply(_ sender:UIButton) {
        dictJobDetail = self.arrJobSearchList[sender.tag] as! [String : Any]
        applyJob(jobID: dictJobDetail["hire_id"] as! String)
    }
    
    @IBAction func tappedOnPromote(_ sender:UIButton) {
        let dictDetail = self.arrJobSearchList[sender.tag] as! [String : Any]
        let paymentView = storyBoards.Main.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        paymentView.productDetail = dictDetail
        paymentView.isJobPromote = true
        self.navigationController?.pushViewController(paymentView, animated: true)
    }
    
    func applyJob(jobID:String) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? "",
                          "job_id":jobID] as [String : Any]
        appDelegate.showHud()
        request(WebURL.applyJob, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("ApplyJob Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool) {
                        let successMsg = dictResult["o_message"] as! String
                        //appDelegate.showAlertMessage(strMessage: errorMsg)
                        showAlert(msg: successMsg, completion: { (true) in
                            self.sendApplyJobMessage()
                        })
                    }else {
                        let errorMsg = dictResult["o_message"] as! String
                        appDelegate.showAlertMessage(strMessage: errorMsg)
                    }
                }
            }
        })
    }
    
    //MARK: - UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        searchBar.frame =  CGRect(x:0, y:0, width:(ScreenSize.SCREEN_WIDTH * 0.8), height:44)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.frame =  CGRect(x:0, y:0, width:(ScreenSize.SCREEN_WIDTH * 0.7), height:44)
        if searchBar.text == "" {
            arrJobSearchList = arrJobList;
            self.tblJobs.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
            arrJobSearchList = arrJobList;
            self.tblJobs.reloadData()
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("search text: \(searchText)")
        let namePredicate = NSPredicate(format: "job_title CONTAINS[C] '\(searchText)'")
        arrJobSearchList = self.arrJobList.filter { namePredicate.evaluate(with: $0) }
        if searchText == "" {
            arrJobSearchList = arrJobList;
        }
        self.tblJobs.reloadData()
        //print("search result: \(arrTemp)")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
