//
//  JobDetailViewController.swift
//  SolvoList
//
//  Created by ronak patel on 17/10/17.
//  Copyright © 2017 MassInfo. All rights reserved.
//

import UIKit
import Alamofire
class JobDetailViewController: UIViewController {

    var jobDetail = [String:Any]()
    
    @IBOutlet var imgHire:UIImageView!
    @IBOutlet var lblJobTitle:UILabel!
    @IBOutlet var lblCompanyName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblDayBefore: UILabel!
    @IBOutlet var lblPayHr: UILabel!
    @IBOutlet var lblExtraSkill: UILabel!
    
    @IBOutlet var lblSkill1: UILabel!
    @IBOutlet var lblSkill2: UILabel!
    @IBOutlet var lblSkill3: UILabel!
    
    @IBOutlet var btnJobApply: UIButton!
    @IBOutlet var btnJobCancel: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Jobs"
        self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack"), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("JobDetail:\(jobDetail)")
        
        lblJobTitle.text = jobDetail["job_title"] as? String
        lblCompanyName.text = "Company Name: \(jobDetail["company_name"] as! String)"
        lblPayHr.text = "Pay/hr : $\(jobDetail["pay_per_hour"] as! String)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormate.YYYYMMDD24
        let uploadDate = dateFormatter.date(from: jobDetail["hire_created_datetime"] as! String)
        let strDateSince:String = (uploadDate?.timeAgoSinceDate(numericDates: true))!
        lblDayBefore.text = "\(String(describing: strDateSince))"
        lblExtraSkill.text = jobDetail["extra_skill"] as? String
        lblSkill1.text = jobDetail["skill_1"] as? String
        lblSkill2.text = jobDetail["skill_2"] as? String
        lblSkill3.text = jobDetail["skill_3"] as? String
        self.imgHire.sd_setImage(with: URL(string: jobDetail["image"] as! String)) { (image, error, nil, url) in
            if image == nil {
                self.imgHire.image = #imageLiteral(resourceName: "jobs")
            }
        }
        
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        let strUserID = userDetail["user_id"] as! String
        if strUserID == jobDetail["user_id"] as! String {
            btnJobApply.isHidden = true
            btnJobCancel.isHidden = true
        }else{
            btnJobApply.isHidden = false
            btnJobCancel.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Button Action Event
    @IBAction func tappedOnApply(_ sender:UIButton) {
        applyJob(jobID: jobDetail["hire_id"] as! String)
    }
    
    func applyJob(jobID:String) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? "",
                          "job_id":jobID] as [String : Any]
        appDelegate.showHud()
        request(WebURL.applyJob, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("ApplyJob Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        let successMsg = dictResult["o_message"] as! String
                        //appDelegate.showAlertMessage(strMessage: errorMsg)
                        showAlert(msg: successMsg, completion: { (true) in
                            self.sendApplyJobMessage()
                        })
                    }else{
                        let errorMsg = dictResult["o_message"] as! String
                        appDelegate.showAlertMessage(strMessage: errorMsg)
                    }
                    
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }
    
    
    func sendApplyJobMessage() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if let userEmail = userDetail["email"] as? String {
            FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
                if let err:Error = error {
                    print(err.localizedDescription)
                    return
                }
                //let strUID = FIRAuth.auth()?.currentUser?.uid
                let dictUser = self.jobDetail["userprofile"] as! [String:Any]
                print("User: \(dictUser)")
                let strProdEmail = dictUser["user_email"] as! String
                let groupID = chatId([userEmail,strProdEmail])
                
                //let strRecentID1 = self.chatId([groupID,userEmail])
                
                let object = FObject(path: FRECENT_PATH)
                object[FRECENT_OBJECTID] = groupID
                object[FRECENT_USERID] = dictUser["user_id"] as! String
                object[FRECENT_CURRENTUSERID] = userDetail["user_id"] as! String
                
                object[FRECENT_NAME] = dictUser["user_name"] as! String
                object[FRECENT_CURRENTNAME] = userDetail["name"] as! String
                
                object[FRECENT_EMAIL] = dictUser["user_email"] as! String
                object[FRECENT_CURRENTEMAIL] = userDetail["email"] as! String
                
                object[FRECENT_PICTURE] = dictUser["user_profile_picture"] as! String
                object[FRECENT_CURRENTPICTURE] = userDetail["profile_picture"] as! String
                object[FRECENT_MSGID] = groupID
                object.saveInBackground()
                
                
                
                let msgObject = FObject(path: FMESSAGE_PATH, subpath:groupID)
                let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
                
                msgObject[MESSAGE_TEXT] = "Hi I would like to apply to your job."
                msgObject[FMESSAGE_SENDERID] = FIRAuth.auth()?.currentUser?.uid ?? ""
                msgObject[FMESSAGE_SENDERNAME] = dictUser["user_name"] ?? ""
                msgObject[FMESSAGE_DEVICETYPE] = "ios"
                msgObject[FMESSAGE_DEVICETOKEN] = getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken)
                msgObject[FMESSAGE_CURRENTDEVICETYPE] = dictUser["device_type"] as! String
                msgObject[FMESSAGE_CURRENTDEVICETOKEN] = dictUser["device_id"] as! String
                msgObject[FMESSAGE_CURRENTUSERID] = userDetail["user_id"] ?? ""
                msgObject[FMESSAGE_USERID] = dictUser["user_id"] ?? ""
                msgObject.saveInBackground()
                
                let dictBody:[String:String] = ["email":userEmail,
                                                "text":"Hi I would like to apply to your job.",
                                                "username":userDetail["name"] as! String,
                                                "photo":"",
                                                "fcm_token": getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String,
                                                "currentEmail":userDetail["email"] as! String,
                                                "current_userid":userDetail["user_id"] as! String,
                                                "sound":"default"]
                print(dictBody)
                appDelegate.sendPushMessage(strToken: dictUser["device_id"] as! String, deviceType: dictUser["device_type"] as! String, dictBody: dictBody)
                
                
                /*let chatView = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatView.hidesBottomBarWhenPushed = true
                chatView.messageObjectID = groupID
                chatView.senderDisplayName = dictUser["user_name"] as! String
                chatView.userID = dictUser["user_id"] as! String
                chatView.isSendCustomMessage = true
                chatView.receiverFCMToken = dictUser["device_id"] as! String
                chatView.customMessage = "Hi I would like to apply to your job."
                self.navigationController?.pushViewController(chatView, animated: true)*/
            })
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
