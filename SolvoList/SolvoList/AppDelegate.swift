//
//  AppDelegate.swift
//  SolvoList
//
//  Created by ronak patel on 15/09/17.
//  Copyright © 2017 depixed.com. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import Google
import UserNotifications
import CoreLocation
import Alamofire
import Fabric
import Crashlytics
import Firebase
import FirebaseMessaging
import Stripe
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let publishableKey = "pk_test_P3Pd4IO4RVo8kPl5qsUaEqbb"
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate, UNUserNotificationCenterDelegate,CLLocationManagerDelegate {
    
    
    
    var window: UIWindow?
    var tabBarController:UITabBarController!
    
    var mbProgressHud:MBProgressHUD!
    
    var homeController:HomeViewController!
    var sellController:SellViewController!
    var jobsController:JobsViewController!
    var messageController:MessageViewController!
    var hireController:HireViewController!
    var locationManager:CLLocationManager!
    
    var arrCategory = [Any]()
    var arrHireCategory = [Any]()
    var arrHireSkill = [Any]()
    var strCityName = ""
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        setMyUserDefaults(value: "", key: MyUserDefaults.OpentChatID)
        
        DropDown.startListeningToKeyboard()
        
        Fabric.with([Crashlytics.self])
        
        if let strCity = getMyUserDefaults(key: MyUserDefaults.currentCityName) as? String  {
            self.strCityName = strCity
        }else{
            setMyUserDefaults(value: "", key: MyUserDefaults.currentCityName)
        }
        
        //-----------------------------------Farebase Configure-------------------------------------
        FIRApp.configure()
        
        //-----------------------------------Intialize Window-------------------------------------
        window = UIWindow(frame: UIScreen.main.bounds)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil);
        self.homeController = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.sellController = storyBoard.instantiateViewController(withIdentifier: "SellViewController") as! SellViewController
        self.jobsController = storyBoard.instantiateViewController(withIdentifier: "JobsViewController") as! JobsViewController
        self.hireController = storyBoard.instantiateViewController(withIdentifier: "HireViewController") as! HireViewController
        self.messageController = storyBoard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        
        let navController1 = NavigationController(rootViewController: self.homeController)
        let navController2 = NavigationController(rootViewController: self.sellController)
        let navController3 = NavigationController(rootViewController: self.jobsController)
        let navController4 = NavigationController(rootViewController: self.hireController)
        let navController5 = NavigationController(rootViewController: self.messageController)
        
        
        self.tabBarController = UITabBarController()
        self.tabBarController.viewControllers = [navController1, navController2, navController3, navController4, navController5]
        self.tabBarController.tabBar.isTranslucent = false
        self.tabBarController.selectedIndex = 0
        
        self.window?.rootViewController = self.tabBarController
        
        UITabBar.appearance().tintColor = Color.COLOR_GREEN
        if #available(iOS 10.0, *) {
            UITabBar.appearance().unselectedItemTintColor = UIColor.black
        } else {
            // Fallback on earlier versions
        }
        
        //----------PayPal------------
        PayPalMobile .initializeWithClientIds(forEnvironments:
            [PayPalEnvironmentProduction: "AdKsIxy0Nzi5hr4dB9ON9w-Ts_c1Wd3D75sz89vSK12mpvDT0JI2_4qFQOmckYkJjlvfBY22Ry9n7HST",
             PayPalEnvironmentSandbox: "AT-ySRBJvqmh74DMJgzbzovc8SUEK8HU2beYR2dqoKUWersn9294sNMJLM9V"])
        
        
        //----------Strip Intialize------------
        STPPaymentConfiguration.shared().publishableKey = publishableKey
        STPPaymentConfiguration.shared().companyName = "SolvoList"
        MainAPIClient.shared.baseURLString = WebURL.baseURL
        
        //----------Facebook Intialize------------
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //----------Google Intialize------------
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        GIDSignIn.sharedInstance().delegate = self
        
        //----------Register Push Notification------------
        self.registerForRemoteNotification()
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                                         name: NSNotification.Name.firInstanceIDTokenRefresh,
                                                         object: nil)
        
        
        //----------Current Location------------
        self.getCurrentLocation()
        
        //----------Sell Category------------
        self.getSellCategory()
        self.getHireCategory()
        self.getHireSkill()
        self.getPromoteAmount()
        
        setBadgeCount()
        // Override point for customization after application launch.
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let stripeHandled = Stripe.handleURLCallback(with: url)
        if (stripeHandled) {
            return true
        }
        
        var handled:Bool = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        if #available(iOS 9.0, *) {
            handled = handled ? handled : GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            return handled
        } else {
            // Fallback on earlier versions
        }
        return handled
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                let stripeHandled = Stripe.handleURLCallback(with: url)
                if (stripeHandled) {
                    return true
                } else {
                    // This was not a stripe url – do whatever url handling your app
                    // normally does, if any.
                }
            }
        }
        return false
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Device Token:- \(deviceTokenString)")
        //UserDefaults.standard.setValue(deviceTokenString, forKey: "DeviceToken")
        setMyUserDefaults(value: deviceTokenString, key: MyUserDefaults.DeviceToken)
        
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            setMyUserDefaults(value: refreshedToken, key: MyUserDefaults.FCMDeviceToken)
            /*let alertController = UIAlertController(title: "InstanceID", message: "", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
                textField.keyboardType = UIKeyboardType.emailAddress;
                textField.text = refreshedToken
            })
            
            window?.rootViewController?.present(alertController, animated: true, completion: nil)*/
        }
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        setMyUserDefaults(value: "", key: MyUserDefaults.DeviceToken)
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        
        let dictNotificationInfo = notification.request.content.userInfo as! [String:Any]
        print("Notification willPresent:\(dictNotificationInfo)")
        let openChatID = getMyUserDefaults(key: MyUserDefaults.OpentChatID) as! String
        if openChatID != dictNotificationInfo["gcm.notification.fcm_token"] as! String {
            completionHandler([.alert, .badge, .sound])
        }
        if let count = getMyUserDefaults(key: MyUserDefaults.NotificationCount) as? String {
            setMyUserDefaults(value: "\(Int(count)! + 1)", key: MyUserDefaults.NotificationCount)
        }else{
            setMyUserDefaults(value: "1", key: MyUserDefaults.NotificationCount)
        }
        setBadgeCount()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Notification didReceiveRemoteNotification:\(userInfo)")
        if let count = getMyUserDefaults(key: MyUserDefaults.NotificationCount) as? String {
            setMyUserDefaults(value: "\(Int(count)! + 1)", key: MyUserDefaults.NotificationCount)
        }else{
            setMyUserDefaults(value: "1", key: MyUserDefaults.NotificationCount)
        }
        setBadgeCount()
    }
    
    func setBadgeCount() {
        let item:UITabBarItem = self.tabBarController.tabBar.items![4]
        let badge = getMyUserDefaults(key: MyUserDefaults.NotificationCount) as? String
        if badge == "0" {
            item.badgeValue = nil
        }else{
            item.badgeValue = badge
        }
        
    }
    
    //MARK: - Register RemoteNotification
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
            //FIRMessaging.messaging().remoteMessageDelegate = self as! FIRMessagingDelegate
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func tokenRefreshNotification(notification: NSNotification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            setMyUserDefaults(value: refreshedToken, key: MyUserDefaults.FCMDeviceToken)
            /*let alertController = UIAlertController(title: "InstanceID", message: "", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
                textField.keyboardType = UIKeyboardType.emailAddress;
                textField.text = refreshedToken
            })
            
            window?.rootViewController?.present(alertController, animated: true, completion: nil)*/
        }
    }
    
    //MARK: - GIDSignInDelegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            /*print("UserID:\(user.userID)")                  // For client-side use only!
             print("Token:\(user.authentication.idToken)") // Safe to send to the server
             print("Profile:\(user.profile.name)")
             print("GivenName:\(user.profile.givenName)")
             print("familyName:\(user.profile.familyName)")
             print("Email:\(user.profile.email)")
             print("ProfilePic:\(user.profile.imageURL(withDimension: 200))")*/
            var googleData = [String:Any]()
            googleData["userID"] = user.userID
            //googleData["Token"] = user.authentication.idToken
            googleData["Profile"] = user.profile.name
            googleData["GivenName"] = user.profile.givenName
            googleData["familyName"] = user.profile.familyName
            googleData["email"] = user.profile.email
            googleData["profilePic"] = user.profile.imageURL(withDimension: 200).absoluteString
            setMyUserDefaults(value: googleData, key: MyUserDefaults.googleData)
            NotificationCenter.default.post(name: .signInWithGoogle, object: nil)
        } else {
            print("\(error.localizedDescription)")
            NotificationCenter.default.post(name: .signInWithGoogle, object: nil)
        }
    }
    
    //MARK: - HUD method
    func showHud()  {
        
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
            
            self.mbProgressHud = MBProgressHUD.showAdded(to: window, animated: true)
            self.mbProgressHud.label.text = "Please wait..."
            
            
            //self.mbProgressHud.hideAnimated(true, afterDelay: 30.0)
        }
    }
    
    func hideHud() {
        if self.mbProgressHud != nil {
            self.mbProgressHud.hide(animated: true)
        }
    }
    
    
    func showToastMessage(strMessage:String) {
        window?.makeToast(strMessage, duration: 3.0, position: CSToastPositionBottom)
    }
    
    func showAlertMessage(strMessage:String) {
        UIAlertView(title: "SolvoList", message: strMessage, delegate: nil, cancelButtonTitle: "Okay").show()
    }
    
    //MARK: - Get Current location
    func getCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        //locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func getCurrentCity() {
        if self.strCityName != "" {
            return
        }
        
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            //let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let location = CLLocation(latitude: latitude, longitude: longitude)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    
                    print(pm.postalCode ?? "") //print zip code
                    print(pm.locality ?? "") //print City
                    self.strCityName = pm.locality!
                    setMyUserDefaults(value: self.strCityName, key: MyUserDefaults.currentCityName)
                }
                else {
                    print("Problem with the data received from geocoder")
                }
            })
        }
    }
    
    func getSellCategory()
    {
        let parameters = ["i_key":WebURL.appKey] as [String : Any]
        
        request(WebURL.getCategoryList, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("SellCategory:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.arrCategory = dictResult["o_data"] as! [Any]
                        NotificationCenter.default.post(name: .refreshProductCategory, object: nil)
                    }else{
                        
                    }
                    
                }
            }
        })
    }
    
    func getHireCategory()
    {
        let parameters = ["i_key":WebURL.appKey] as [String : Any]
        
        request(WebURL.hireCategory, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("Hire Category:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.arrHireCategory = dictResult["o_data"] as! [Any]
                    }else{
                        
                    }
                    
                }
            }
        })
    }
    
    func getHireSkill()
    {
        let parameters = ["i_key":WebURL.appKey] as [String : Any]
        
        request(WebURL.getHireSkill, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("Hire Category:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.arrHireSkill = dictResult["o_data"] as! [Any]
                    }else{
                        
                    }
                    
                }
            }
        })
    }
    
    func updateDeviceToken(strToken:String) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        
        let parameters = ["i_key":WebURL.appKey,
                          "device_token":strToken,
                          "device_type":"ios",
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        request(WebURL.updateDevicetoken, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("updateDevicetoken Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    print(dictResult)
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }
    
    func getPromoteAmount() {
        
        let parameters = ["i_key":WebURL.appKey] as [String : Any]
        appDelegate.showHud()
        request(WebURL.getPromoteAmount, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("getPromoteAmount Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    let arrData = dictResult["o_data"] as! [Any]
                    for dict in arrData {
                        let dictDetail = dict as! [String:String]
                        if dictDetail["promote_type"] == "job"{
                            setMyUserDefaults(value: dictDetail["amount"] ?? "0.00", key: MyUserDefaults.JobPromoteAmount)
                        }else if dictDetail["promote_type"] == "product" {
                            setMyUserDefaults(value: dictDetail["amount"] ?? "0.00", key: MyUserDefaults.ProductPromoteAmount)
                        }
                    }
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }
    
    func sendPushMessage(strToken:String,deviceType:String,dictBody:[String:String]) {
        _ = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        var strKey = "notification"
        if deviceType == "android" {
            strKey = "data"
        }
        let parameters = ["to":strToken,
                          "priority":"high",
                          strKey:dictBody] as [String : Any]
        let headerHttp = ["Authorization":"key=AIzaSyCCpAd1-hDrIDEXD63IC1k9ouz-h3an_vs",
                          "Content-Type":"application/json"]
        print("Parameter:\(parameters)")
        //appDelegate.showHud()
        request("https://fcm.googleapis.com/fcm/send", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headerHttp).responseJSON(completionHandler: { (response) in
            print("send message Result:\(response)")
            //appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    print(dictResult)
                }
            }
        })
    }
    
    //MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("didStartMonitoringFor = \(locValue.latitude) \(locValue.longitude)")
        setMyUserDefaults(value: locValue.latitude, key: MyUserDefaults.UserLatitude)
        setMyUserDefaults(value: locValue.longitude, key: MyUserDefaults.UserLongitude)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        //print("didUpdateLocations = \(locValue.latitude) \(locValue.longitude)")
        setMyUserDefaults(value: locValue.latitude, key: MyUserDefaults.UserLatitude)
        setMyUserDefaults(value: locValue.longitude, key: MyUserDefaults.UserLongitude)
        self.getCurrentCity()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch CLLocationManager.authorizationStatus()
        {
            case .authorizedAlways:
                print("Location services authorised by user");
                break;
            
            case .authorizedWhenInUse:
                print("Location services authorised by user");
                break;
            
            case .denied:
                print("Location services denied by user");
                break;
            
            case .restricted:
                print("Parental controls restrict location services");
                break;
            
            case .notDetermined:
                print("Unable to determine, possibly not available");
                break;
            
            default:
                print("Default");
                break;
        }
    }
    
    
    
    
    
    
    
    
}

