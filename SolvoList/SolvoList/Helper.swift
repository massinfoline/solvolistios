//
//  Helper.swift
//  CommonClass
//
//  Created by Devubha Manek on 5/2/16.
//  Copyright © 2016 Devubha Manek. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

//MARK: - Device Type
enum UIUserInterfaceIdiom : Int
{
    case unspecified
    case phone
    case pad
}

struct DateFormate {
    static let YYYYMMDD24 = "yyyy-MM-dd HH:mm:ss"
}

let arrSellTab = ["Photo","Details","Price","Finish"]
let arrHireTab = ["Photo","Details","Pay/hr","Finish"]

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6PLUS         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}
let yellowColor = UIColor.init(red: 242.0/255.0, green: 128.0/255.0, blue: 34.0/255.0, alpha: 1.0)

//MARK: - Screen Size
struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

//MARK: - Font Layout
struct FontName
{
    static let OpenSansBold = "OpenSans-Bold"
    static let OpenSansRegular = "OpenSans"
    static let OpenSansLight = "OpenSans-Light"
    static let OpenSansSemiBold = "OpenSans-Semibold"
}
func setFontLayout(_ strFontName:String,fontSize:CGFloat) -> UIFont
{
    return UIFont(name: strFontName, size: (ScreenSize.SCREEN_WIDTH / 375) * fontSize)!
}

//MARK: - Scaling
struct DeviseScale
{
    static let SCALE_X = ScreenSize.SCREEN_WIDTH / 375
    static let SCALE_Y = ScreenSize.SCREEN_HEIGHT / 667
}

//MARK: - Web Services Constant

struct WebURL {
    static let appKey:String = "solvolist@123"
    
    static let baseURL:String = "http://solvolist.massinfoline.com/"
    
    static let register:String = WebURL.baseURL + "register.php"
    static let login:String = WebURL.baseURL + "login.php"
    static let forgotPassword:String = WebURL.baseURL + "forgotpassword.php"
    static let resetPassword:String = WebURL.baseURL + "resetpassword.php"
    static let emailVerification:String = WebURL.baseURL + "emailverification.php"
    static let getCategoryList:String = WebURL.baseURL + "getcategorylist.php"
    static let questionQuery:String = WebURL.baseURL + "questionquery.php"
    static let showProfile:String = WebURL.baseURL + "showprofile.php"
    static let hireCategory:String = WebURL.baseURL + "get_hire_categorylist.php"
    static let productList:String = WebURL.baseURL + "getproductlist.php"
    static let addProduct:String = WebURL.baseURL + "addproduct.php"
    static let updateProfile:String = WebURL.baseURL + "updateprofile.php"
    static let addHire:String = WebURL.baseURL + "addhire.php"
    static let getJobList:String = WebURL.baseURL + "getjoblist.php"
    static let applyJob:String = WebURL.baseURL + "apply_job.php"
    static let getUserBuyingProduct:String = WebURL.baseURL + "get_user_buying_productlist.php"
    static let getUserApplyJoblist:String = WebURL.baseURL + "get_user_apply_joblist.php"
    static let myPostingAPI:String = WebURL.baseURL + "user_product_job.php"
    static let filter:String = WebURL.baseURL + "filter.php"
    static let getHireSkill:String = WebURL.baseURL + "get_hire_skill.php"
    static let makeOffer:String = WebURL.baseURL + "make_offer.php"
    static let markSold:String = WebURL.baseURL + "mark_sold.php"
    static let stripeCharge:String = WebURL.baseURL + "charge.php"
    static let promoteProduct:String = WebURL.baseURL + "promote_product.php"
    static let promoteJob:String = WebURL.baseURL + "promote_job.php"
    static let updateDevicetoken:String = WebURL.baseURL + "update_devicetoken.php"
    static let deleteJob:String = WebURL.baseURL + "delete_job.php"
    static let addDepositeAccount:String = WebURL.baseURL + "add_deposite_account.php"
    static let displayDepositeHistory:String = WebURL.baseURL + "display_deposite_history.php"
    static let promoteProductList:String = WebURL.baseURL + "promote_product_list.php"
    static let promoteJobList:String = WebURL.baseURL + "promote_job_list.php"
    static let updateProductDetail:String = WebURL.baseURL + "update_product_detail.php"
    static let markJob:String = WebURL.baseURL + "mark_job.php"
    static let getProductPurchase:String = WebURL.baseURL + "get_solvolist_product_purchase.php"
    static let getPromoteAmount:String = WebURL.baseURL + "get_promote_amount.php"
    static let UpdateJobDetail:String = WebURL.baseURL + "update_job_detail.php"
    static let addProductPurchase:String = WebURL.baseURL + "add_solvolist_product_purchase.php"
    static let getDepositeAccount:String = WebURL.baseURL + "get_deposite_account.php"
}

extension Notification.Name {
    static let signInWithGoogle = Notification.Name("signInWithGoogle")
    static let refreshProductCategory = Notification.Name("RefreshProductCategory")
}


struct storyBoards {
    static let Main = UIStoryboard(name: "Main", bundle: Bundle.main)
    static let Sell = UIStoryboard(name: "Sell", bundle: Bundle.main)
    static let Hire = UIStoryboard(name: "Hire", bundle: Bundle.main)
    static let Notification = UIStoryboard(name: "Notification", bundle: Bundle.main)
    static let Chat = UIStoryboard(name: "Chat", bundle: Bundle.main)
}


//MARK: - Color
struct Color
{
    static let textColor = UIColor(red: 0.92, green: 0.92, blue: 0.92, alpha: 1.0)
    static let keyboardHeaderColor = UIColor(red: 0.34, green: 0.43, blue: 0.42, alpha: 1.0)
    static let COLOR_NAVIGATION_TEXT = UIColor.white
    static let COLOR_NAVIGATION_BACKGROUND = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    static let COLOR_GREEN = UIColor(red: 20.0/255.0, green: 168.0/255.0, blue: 48.0/255.0, alpha: 1.0)
}

//MARK: - Get/Set UserDefaults
func setMyUserDefaults(value:Any, key:String){
    UserDefaults.standard.set(value, forKey: key)
    UserDefaults.standard.synchronize()
}
func getMyUserDefaults(key:String)->Any {
    return UserDefaults.standard.value(forKey: key) ?? (Any).self
}
func clearMyUserDefaluts(){
    let appDomain = Bundle.main.bundleIdentifier!
    UserDefaults.standard.removePersistentDomain(forName: appDomain)
    UserDefaults.standard.synchronize()
}
func removeMyUserDefaults(key:String){
    UserDefaults.standard.removeObject(forKey: key)
}

struct MyUserDefaults {
    static let UserId:String = "UserId"
    static let UserData:String = "UserData"
    static let isLogin:String = "isLogin"
    static let Login:String = "Login"
    static let LoginWith:String = "LoginWith"
    static let PredefineData:String = "PredefineData"
    static let UserProfileData:String = "UserProfileData"
    static let UserLatitude:String = "UserLatitude"
    static let UserLongitude:String = "UserLongitude"
    static let DeviceToken:String = "DeviceToken"
    static let FCMDeviceToken:String = "FCMDeviceToken"
    static let FBdata:String = "FBdata"
    static let googleData:String = "googleData"
    static let currentCityName:String = "currentCityName"
    static let prodUpdateDetail:String = "prodUpdateDetail"
    static let jobUpdateDetail:String = "jobUpdateDetail"
    static let JobPromoteAmount:String = "JobPromoteAmount"
    static let ProductPromoteAmount:String = "ProductPromoteAmount"
    static let NotificationCount:String = "NotificationCount"
    static let OpentChatID:String = "OpentChatID"
}



//MARK: - Show Alert
func showAlert(msg:String, completion:((Bool)->())?){
    let alert = UIAlertController(title: "SolvoList", message: msg, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
        if completion != nil {
            completion!(true)
        }
    }))
    appDelegate.window?.topViewController()?.present(alert, animated: true, completion: nil)
    //UIWindow.topViewController()?.present(alert, animated: true, completion: nil)
}

extension UIWindow {
    func topViewController() -> UIViewController? {
        var top = self.rootViewController
        while true {
            if let presented = top?.presentedViewController {
                top = presented
            } else if let nav = top as? UINavigationController {
                top = nav.visibleViewController
            } else if let tab = top as? UITabBarController {
                top = tab.selectedViewController
            } else {
                break
            }
        }
        return top
    }
}

//MARK: - Shared Instance
class Helper
{
    class var sharedInstance: Helper
    {
        struct Static
        {
            static var onceToken: Int = 0
            static var instance: Helper? = nil
        }
        //_ = Helper.__once
        return Static.instance!
    }
}
func setAttribText(_ strText:String) -> NSAttributedString {
    
    let textRange = NSMakeRange(0, strText.characters.count)
    let attributedText = NSMutableAttributedString(string: strText)
    attributedText.addAttribute(NSUnderlineStyleAttributeName , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
    // Add other attributes if needed
    return attributedText
}

func chatId(_ members: [String]) -> String {
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    let sorted = members.sorted { $0.compare($1 , options: .numeric) == .orderedAscending }
    let strMDSHASH = Checksum.md5Hash(of: sorted.joined(separator: ""))
    return strMDSHASH!
    //return Checksum.md5Hash(sorted.joined(separator: ""))
}


//MARK: - Set Line spacing
extension UILabel
{
    func setLineHeight(_ lineHeight: CGFloat) {
        let text = self.text
        if let text = text {
            let attributeString = NSMutableAttributedString(string: text)
            let style = NSMutableParagraphStyle()
            
            style.lineSpacing = lineHeight
            attributeString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSMakeRange(0, text.characters.count))
            self.attributedText = attributeString
        }
    }
    
    
    
    //Set notification counter with dynamic width calculate
    func setNotificationCounter(counter: String) {
        if counter.length == 0 {
            self.isHidden = true
        }
        else {
            self.isHidden = false
            let strCounter:String = ":\(counter)|"
            
            self.clipsToBounds = true
            self.layer.cornerRadius = self.frame.size.height / 2.0
            
            let string_to_color1 = ":"
            let range1 = (strCounter as NSString).range(of: string_to_color1)
            let attributedString1 = NSMutableAttributedString(string:strCounter)
            attributedString1.addAttribute(NSForegroundColorAttributeName, value: UIColor.clear , range: range1)
            
            let string_to_color2 = "|"
            let range2 = (strCounter as NSString).range(of: string_to_color2)
            attributedString1.addAttribute(NSForegroundColorAttributeName, value: UIColor.clear , range: range2)
            self.attributedText = attributedString1
        }
    }
    //Get notification counter
    func getNotificationCounter() -> String {
        if self.text?.length == 0 {
            return ""
        }
        else {
            var strCounter = self.text
            strCounter = strCounter?.replacingOccurrences(of: ":", with: "")
            strCounter = strCounter?.replacingOccurrences(of: "|", with: "")
            return strCounter!
        }
    }
    
}


//MARK: - String Get length
extension String
{
    var length: Int { return characters.count    }  // Swift 2.0
    
    var html2AttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    
    func isValidEmail() -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    
    func isNumber() -> Bool {
        let numberCharacters = CharacterSet.decimalDigits.inverted
        return self.rangeOfCharacter(from:numberCharacters) == nil
    }
}
//MARK: - Dynamic Width / Height set
extension UILabel{
    
    func requiredHeight() -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        
        return label.frame.height
    }
    func requiredWidth() -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        
        return label.frame.width
    }
}

func ValidEmail(testStr:String) -> Bool {
    print("validate emilId: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let result = emailTest.evaluate(with: testStr)
    return result
}

func validateMobile(value: String) -> Bool {
    if value.length >= 10 && value.length <= 15 {
        return true
    }else{
        return false
    }
    //    let PHONE_REGEX = "^\\d{10}$"
    //    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    //    let result =  phoneTest.evaluate(with: value)
    //    return result
    
}

func widthForLabel(_ label:UILabel,text:String) ->CGFloat
{
    let fontName = label.font.fontName;
    let fontSize = label.font.pointSize;
    
    let attributedText = NSMutableAttributedString(string: text,attributes: [NSFontAttributeName:UIFont(name: fontName,size: fontSize)!])
    let rect: CGRect = attributedText.boundingRect(with: CGSize(width: label.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
    
    return ceil(rect.size.width)
}

//MARK: - UIView Extension
extension UIView {
    
    //MARK: Set Corner Radious
    @IBInspectable var corner_Radius:CGFloat {
        set {
            self.layer.cornerRadius = newValue
        }
        get {
            return self.layer.cornerRadius
        }
    }
    //Set Round
    @IBInspectable var Round:Bool {
        set {
            self.layer.cornerRadius = self.frame.size.height / 2.0
        }
        get {
            return self.layer.cornerRadius == self.frame.size.height / 2.0
        }
    }
    
    //Set Border Color
    @IBInspectable var borderColor:UIColor {
        set {
            self.layer.borderColor = newValue.cgColor
        }
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
    }
    //Set Border Width
    @IBInspectable var borderWidth:CGFloat {
        set {
            self.layer.borderWidth = newValue
        }
        get {
            return self.layer.borderWidth
        }
    }
}

//MARK: -  extension UIViewController
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

//MARK: - UILabel
class MTLabel : UILabel {
    override func awakeFromNib() {
        //Font size auto resizing in different devices
        self.font = self.font.withSize(self.font.pointSize * DeviseScale.SCALE_X)
    }
}

//MARK: - UIButton
class kButton : UIButton {
    override func awakeFromNib() {
        //Font size auto resizing in different devices
        self.titleLabel?.font = self.titleLabel?.font.withSize((self.titleLabel?.font.pointSize)! * DeviseScale.SCALE_X)
    }
}

//MARK: - Class NSLayoutConstraint
class kLayoutConstraint: NSLayoutConstraint {
    override func awakeFromNib() {
        //Font size auto resizing in different devices
        self.constant = self.constant * DeviseScale.SCALE_X
    }
}


//MARK: - Class UITextField
class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
    
    override func awakeFromNib() {
        //Font size auto resizing in different devices
        self.font = self.font?.withSize((self.font?.pointSize)! * DeviseScale.SCALE_X)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}

import MobileCoreServices

//MARK: - Open Camera & Photo Library
func PresentPhotoCamera(target: Any, canEdit: Bool) -> Bool {
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
        return false
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    let type = kUTTypeImage
    let imagePicker = UIImagePickerController()
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if UIImagePickerController.isSourceTypeAvailable(.camera) && (UIImagePickerController.availableMediaTypes(for: .camera)?.contains(type as String))! {
        imagePicker.mediaTypes = [type as String]
        imagePicker.sourceType = .camera
        if UIImagePickerController.isCameraDeviceAvailable(.rear) {
            imagePicker.cameraDevice = .rear
        }
        else if UIImagePickerController.isCameraDeviceAvailable(.front) {
            imagePicker.cameraDevice = .front
        }
    }
    else {
        return false
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    imagePicker.allowsEditing = canEdit
    imagePicker.showsCameraControls = true
    imagePicker.delegate = target as? (UIImagePickerControllerDelegate & UINavigationControllerDelegate)
    (target as! UIViewController).present(imagePicker, animated: true) { _ in }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    return true
}


func PresentPhotoLibrary(target: UIViewController, canEdit: Bool) -> Bool {
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    if (UIImagePickerController.isSourceTypeAvailable(.photoLibrary) == false && UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) == false) {
        return false
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    let type = kUTTypeImage
    let imagePicker = UIImagePickerController()
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) && (UIImagePickerController.availableMediaTypes(for: .photoLibrary)?.contains(type as String))! {
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [type as String]
    }
    else if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) && (UIImagePickerController.availableMediaTypes(for: .savedPhotosAlbum)?.contains(type as String))! {
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.mediaTypes = [type as String]
    }
    else {
        return false
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    imagePicker.allowsEditing = canEdit
    imagePicker.delegate = target as? (UIImagePickerControllerDelegate & UINavigationControllerDelegate)
    target.present(imagePicker, animated: true) { _ in }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    return true
}


func PresentVideoLibrary(target: UIViewController, canEdit: Bool, length:Float) -> Bool {
    if (UIImagePickerController.isSourceTypeAvailable(.photoLibrary) == false && UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) == false) {
        return false
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    let type = kUTTypeMovie as? String
    let imagePicker = UIImagePickerController()
    //---------------------------------------------------------------------------------------------------------------------------------------------
    imagePicker.videoMaximumDuration = TimeInterval(length);
    if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) && (UIImagePickerController.availableMediaTypes(for: .photoLibrary)?.contains(type!))! {
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [type!]
    }
    else if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) && (UIImagePickerController.availableMediaTypes(for: .savedPhotosAlbum)?.contains(type!))! {
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.mediaTypes = [type!]
    }
    else {
        return false
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    imagePicker.allowsEditing = canEdit
    imagePicker.delegate = target as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
    target.present(imagePicker, animated: true) { _ in }
    return true;
}

extension Date {
    func timeAgoSinceDate(numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(self)
        let latest = (earliest == now as Date) ? self : now as Date
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
    }
}


