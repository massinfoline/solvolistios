
//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		HEXCOLOR(c) [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]
//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		SYSTEM_VERSION								[[UIDevice currentDevice] systemVersion]
#define		SYSTEM_VERSION_EQUAL_TO(v)					([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedSame)
#define		SYSTEM_VERSION_GREATER_THAN(v)				([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedDescending)
#define		SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)	([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedAscending)
#define		SYSTEM_VERSION_LESS_THAN(v)					([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedAscending)
#define		SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)		([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedDescending)
//-------------------------------------------------------------------------------------------------------------------------------------------------

#define isIPad  UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define userDefaults [NSUserDefaults standardUserDefaults]


//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		VERSION_CUSTOM
#define		VERSION_PREMIUM
//---------------------------------------------------------------------------------
#define		DEFAULT_TAB							0
#define		DEFAULT_COUNTRY						84
//---------------------------------------------------------------------------------
#define		VIDEO_LENGTH						100
#define		AUDIO_LENGTH						5000
#define		INSERT_MESSAGES						10
#define		DOWNLOAD_TIMEOUT					300
//---------------------------------------------------------------------------------
#define		STATUS_LOADING						1
#define		STATUS_SUCCEED						2
#define		STATUS_MANUAL						3
//---------------------------------------------------------------------------------
#define		MEDIA_IMAGE							1
#define		MEDIA_VIDEO							2
#define		MEDIA_AUDIO							3
//---------------------------------------------------------------------------------
#define		NETWORK_MANUAL						1
#define		NETWORK_WIFI						2
#define		NETWORK_ALL							3
//---------------------------------------------------------------------------------
#define		KEEPMEDIA_WEEK						1
#define		KEEPMEDIA_MONTH						2
#define		KEEPMEDIA_FOREVER					3
//---------------------------------------------------------------------------------
#define		DEL_ACCOUNT_NONE					1
#define		DEL_ACCOUNT_ONE						2
#define		DEL_ACCOUNT_ALL						3
//---------------------------------------------------------------------------------
#define		CHAT_PRIVATE						@"private"
#define		CHAT_MULTIPLE						@"multiple"
#define		CHAT_GROUP							@"group"
//---------------------------------------------------------------------------------
#define		CALLHISTORY_AUDIO					@"audio"
#define		CALLHISTORY_VIDEO					@"video"
//---------------------------------------------------------------------------------
#define		MESSAGE_TEXT						@"text"
#define		MESSAGE_OTHER						@"other"
#define		MESSAGE_EMOJI						@"emoji"
#define		MESSAGE_PICTURE						@"picture"
#define		MESSAGE_VIDEO						@"video"
#define		MESSAGE_AUDIO						@"audio"
#define		MESSAGE_LOCATION					@"location"
#define		MESSAGE_STICKER                     @"sticker"
#define		MESSAGE_CONTACT                     @"contact"
#define		MESSAGE_DOCUMENT                    @"document"
//---------------------------------------------------------------------------------
#define		LOGIN_EMAIL							@"Email"
#define		LOGIN_FACEBOOK						@"Facebook"
#define		LOGIN_GOOGLE						@"Google"
#define		LOGIN_PHONE							@"Phone"
//---------------------------------------------------------------------------------
#define		COLOR_OUTGOING			[UIColor colorWithRed:194.0/255.0 green:39.0/255.0 blue:45.0/255.0 alpha:1.0]//HEXCOLOR(0x00C3372D)
#define		COLOR_INCOMING			[UIColor colorWithRed:12.0/255.0 green:104.0/255.0 blue:53.0/255.0 alpha:1.0]//HEXCOLOR(0xE6E5EAFF)
//---------------------------------------------------------------------------------
#define		COLOR_NAVIGATION_TEXT				[UIColor whiteColor]
#define		COLOR_NAVIGATION_BACKGROUND			[UIColor colorWithRed:194.0/255.0 green:39.0/255.0 blue:45.0/255.0 alpha:1.0]//HEXCOLOR(0x7FBB00FF)
//---------------------------------------------------------------------------------
#define		TEXT_QUEUED							@"Queued"
#define		TEXT_SENT							@"Sent"
#define		TEXT_READ							@"Read"
//---------------------------------------------------------------------------------
#define		PHOTOS_ALBUM_TITLE					@"Chat"
//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------
#define		SCREEN_WIDTH						[UIScreen mainScreen].bounds.size.width
#define		SCREEN_HEIGHT						[UIScreen mainScreen].bounds.size.height
//-------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------USER------------------------------------------
#define		FUSER_PATH							@"User"					//	Path name
#define		FUSER_OBJECTID						@"objectId"				//	String

#define		FUSER_EMAIL							@"email"				//	String
#define		FUSER_PHONE							@"phone"				//	String
#define		FUSER_WCPHONE							@"wcPhone"				//	String

#define		FUSER_FIRSTNAME						@"firstname"			//	String
#define		FUSER_LASTNAME						@"lastname"				//	String
#define		FUSER_FULLNAME						@"fullname"				//	String
#define		FUSER_COUNTRY						@"country"				//	String
#define		FUSER_LOCATION						@"location"				//	String
#define		FUSER_STATUS						@"status"				//	String
#define		FUSER_MUTEDID						@"mutedId"				//	String

#define		FUSER_PICTURE						@"picture"
#define		FUSER_PICTUREANDROID				@"pictureAndroid"
#define		FUSER_SingleNotiTone				@"SingleNotiTone"       //	String
#define		FUSER_GroupNotiTone                 @"GroupNotiTone"        //	String
#define		FUSER_SHOWUSERNOTIFICATION			@"showUserNotification" //	String
#define		FUSER_SHOWGROUPNOTIFICATION			@"showGroupNotification"//	String
#define		FUSER_GroupNotiTone                 @"GroupNotiTone"        //	String
#define		FUSER_THUMBNAIL						@"thumbnail"			//	String
#define		FUSER_ABOUTME						@"about_me"             //	String
#define		FUSER_DEVICETYPE					@"deviceType"			//	String

#define		FUSER_KEEPMEDIA						@"keepMedia"			//	Number
#define		FUSER_NETWORKIMAGE					@"networkImage"			//	Number
#define		FUSER_NETWORKVIDEO					@"networkVideo"			//	Number
#define		FUSER_NETWORKAUDIO					@"networkAudio"			//	Number
#define		FUSER_AUTOSAVEMEDIA					@"autoSaveMedia"		//	Boolean
#define		FUSER_WALLPAPER						@"wallpaper"			//	String

#define		FUSER_LOGINMETHOD					@"loginMethod"			//	String
#define		FUSER_ONESIGNALID					@"oneSignalId"			//	String
#define		FUSER_BLOCKMEMBER					@"blockMembers"

#define		FUSER_LASTACTIVE					@"lastActive"			//	Timestamp
#define		FUSER_LASTTERMINATE					@"lastTerminate"		//	Timestamp

#define		FUSER_CREATEDAT						@"createdAt"			//	Timestamp
#define		FUSER_UPDATEDAT						@"updatedAt"			//	Timestamp
//---------------------------------------------------------------------------------
#define		FBLOCKED_PATH						@"Blocked"				//	Path name
#define		FBLOCKED_OBJECTID					@"objectId"				//	String

#define		FBLOCKED_BLOCKEDID					@"blockedId"			//	String
#define		FBLOCKED_USERID						@"userId"				//	String

#define		FBLOCKED_ISDELETED					@"isDeleted"			//	Boolean

#define		FBLOCKED_CREATEDAT					@"createdAt"			//	Timestamp
#define		FBLOCKED_UPDATEDAT					@"updatedAt"			//	Timestamp
//--------------------------------------CALL-------------------------------------------
#define		FCALLHISTORY_PATH					@"CallHistory"			//	Path name
#define		FCALLHISTORY_OBJECTID				@"objectId"				//	String

#define		FCALLHISTORY_INITIATORID			@"initiatorId"			//	String
#define		FCALLHISTORY_RECIPIENTID			@"recipientId"			//	String
#define		FCALLHISTORY_PHONENUMBER			@"phoneNumber"			//	String

#define		FCALLHISTORY_TYPE					@"type"					//	String
#define		FCALLHISTORY_TEXT					@"text"					//	String

#define		FCALLHISTORY_STATUS					@"status"				//	String
#define		FCALLHISTORY_DURATION				@"duration"				//	Number

#define		FCALLHISTORY_STARTEDAT				@"startedAt"			//	Timestamp
#define		FCALLHISTORY_ESTABLISHEDAT			@"establishedAt"		//	Timestamp
#define		FCALLHISTORY_ENDEDAT				@"endedAt"				//	Timestamp

#define		FCALLHISTORY_ISDELETED				@"isDeleted"			//	Boolean

#define		FCALLHISTORY_CREATEDAT				@"createdAt"			//	Timestamp
#define		FCALLHISTORY_UPDATEDAT				@"updatedAt"			//	Timestamp
//---------------------------------------GROUP------------------------------------------
#define		FGROUP_PATH							@"Group"				//	Path name
#define		FGROUP_OBJECTID						@"objectId"				//	String

#define		FGROUP_USERID						@"userId"				//	String
#define		FGROUP_NAME							@"name"					//	String
#define		FGROUP_PICTURE						@"picture"				//	String
#define		FGROUP_PICTUREANDROID				@"pictureAndroid"
#define		FGROUP_MEMBERS						@"members"				//	Array

#define		FGROUP_ISDELETED					@"isDeleted"			//	Boolean

#define		FGROUP_CREATEDAT					@"createdAt"			//	Timestamp
#define		FGROUP_UPDATEDAT					@"updatedAt"			//	Timestamp
//----------------------------------------MESSAGE-----------------------------------------
#define		FMESSAGE_PATH						@"Message"				//	Path name
#define		FMESSAGE_OBJECTID					@"objectId"				//	String

#define		FMESSAGE_GROUPID					@"groupId"				//	String
#define		FMESSAGE_SENDERID					@"senderId"				//	String
#define		FMESSAGE_SENDERNAME					@"senderName"			//	String
#define		FMESSAGE_SENDERINITIALS				@"senderInitials"		//	String

#define		FMESSAGE_CURRENTUSERID				@"currentUserId"		//	String
#define		FMESSAGE_USERID                     @"userId"				//	String

#define		FMESSAGE_DEVICETOKEN				@"deviceToken"		//	String
#define		FMESSAGE_DEVICETYPE                 @"deviceType"		//	String
#define		FMESSAGE_CURRENTDEVICETOKEN			@"currentDeviceToken"   //	String
#define		FMESSAGE_CURRENTDEVICETYPE          @"currentDeviceType"    //	String

#define		FMESSAGE_TYPE						@"type"					//	String
#define		FMESSAGE_TEXT						@"text"					//	String

#define		FMESSAGE_PICTURENAME				@"file_name"			//	String
#define		FMESSAGE_PICTURE					@"picture"				//	String
#define		FMESSAGE_PICTURE_WIDTH				@"picture_width"		//	Number
#define		FMESSAGE_PICTURE_HEIGHT				@"picture_height"		//	Number
#define		FMESSAGE_PICTURE_MD5				@"picture_md5"			//	String

#define		FMESSAGE_VIDEO						@"video"				//	String
#define		FMESSAGE_VIDEO_DURATION				@"video_duration"		//	Number
#define		FMESSAGE_VIDEO_MD5					@"video_md5"			//	String

#define		FMESSAGE_AUDIO						@"audio"				//	String
#define		FMESSAGE_AUDIO_DURATION				@"audio_duration"		//	Number
#define		FMESSAGE_AUDIO_MD5					@"audio_md5"			//	String

#define		FMESSAGE_CONTACTNAME				@"contactName"			//	String
#define		FMESSAGE_CONTACTNUMBER				@"contactNumber"		//	String

#define		FMESSAGE_LATITUDE					@"latitude"				//	Number
#define		FMESSAGE_LONGITUDE					@"longitude"			//	Number

#define		FMESSAGE_STATUS						@"status"				//	String
#define		FMESSAGE_ISDELETED					@"isDeleted"			//	Boolean

#define		FMESSAGE_CREATEDAT					@"createdAt"			//	Timestamp
#define		FMESSAGE_UPDATEDAT					@"updatedAt"			//	Timestamp
//--------------------------------------RECENT-------------------------------------------
#define		FRECENT_PATH						@"Recent"				//	Path name
#define		FRECENT_OBJECTID					@"objectId"				//	String

#define		FRECENT_USERID						@"userId"				//	String
#define		FRECENT_CURRENTUSERID				@"currentUserId"		//	String
#define		FRECENT_GROUPID						@"groupId"				//	String

#define		FRECENT_NAME                        @"name"                 //	String
#define		FRECENT_CURRENTNAME					@"currentName"				//	String

#define		FRECENT_EMAIL                       @"email"                 //	String
#define		FRECENT_CURRENTEMAIL				@"currentEmail"				//	String

#define		FRECENT_MSGID                       @"msgObjectId"          //	String

#define		FRECENT_PICTURE						@"photo"				//	String
#define		FRECENT_CURRENTPICTURE				@"currentPhoto"				//	String

#define		FRECENT_DESCRIPTION					@"description"			//	String
#define		FRECENT_MEMBERS						@"members"				//	Array
#define		FRECENT_TYPE						@"type"					//	String

#define		FRECENT_COUNTER						@"counter"				//	Number
#define		FRECENT_LASTMESSAGE					@"lastMessage"			//	String
#define		FRECENT_LASTMESSAGEDATE				@"lastMessageDate"		//	Timestamp

#define		FRECENT_MUTEDUNTIL					@"mutedUntil"			//	Timestamp

#define		FRECENT_ISMUTE                      @"isMute"               //	Boolean
#define		FRECENT_ISARCHIVED					@"isArchived"			//	Boolean
#define		FRECENT_ISDELETED					@"isDeleted"			//	Boolean

#define		FRECENT_CREATEDAT					@"createdAt"			//	Timestamp
#define		FRECENT_UPDATEDAT					@"updatedAt"			//	Timestamp
//---------------------------------TYPEING------------------------------------------------
#define		FTYPING_PATH						@"Typing"				//	Path name
//-----------------------------------USERSTATUS----------------------------------------------
#define		FUSERSTATUS_PATH					@"UserStatus"			//	Path name
#define		FUSERSTATUS_OBJECTID				@"objectId"				//	String

#define		FUSERSTATUS_NAME					@"name"					//	String

#define		FUSERSTATUS_CREATEDAT				@"createdAt"			//	Timestamp
#define		FUSERSTATUS_UPDATEDAT				@"updatedAt"			//	Timestamp
//-------------------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		ONESIGNALID							@"OneSignalId"
#define		USER_ACCOUNTS						@"UserAccounts"
#define		REACHABILITY_CHANGED				@"ReachabilityChanged"
#define		NOTIFICATION_OPEN_CHAT              @"NotificationOpenChat"
//---------------------------------------------------------------------------------
#define		NOTIFICATION_APP_STARTED			@"NotificationAppStarted"
#define		NOTIFICATION_USER_LOGGED_IN			@"NotificationUserLoggedIn"
#define		NOTIFICATION_USER_LOGGED_OUT		@"NotificationUserLoggedOut"
//---------------------------------------------------------------------------------
#define		NOTIFICATION_REFRESH_BLOCKEDS		@"NotificationRefreshBlockeds"
#define		NOTIFICATION_REFRESH_CALLHISTORIES	@"NotificationRefreshCallHistories"
#define		NOTIFICATION_REFRESH_CONTACTS		@"NotificationRefreshContacts"
#define		NOTIFICATION_REFRESH_GROUPS			@"NotificationRefreshGroups"
#define		NOTIFICATION_REFRESH_MESSAGES1		@"NotificationRefreshMessages1"
#define		NOTIFICATION_REFRESH_MESSAGES2		@"NotificationRefreshMessages2"
#define		NOTIFICATION_REFRESH_RECENTS		@"NotificationRefreshRecents"
#define		NOTIFICATION_REFRESH_STATS			@"NotificationRefreshStats"
#define		NOTIFICATION_REFRESH_USERS			@"NotificationRefreshUsers"
#define		NOTIFICATION_VIEW_CONTACT           @"NotificationViewContact"
#define		NOTIFICATION_ADD_CONTACT            @"NotificationAddContact"
//---------------------------------------------------------------------------------
#define		NOTIFICATION_CLEANUP_CHATVIEW		@"NotificationCleanupChatView"
//-------------------------------------------------------------------------------------------------------------------------------------------------


